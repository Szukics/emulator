# glengine.rb


=begin

***--MANAGED--***

==========================================================================
Copyright (C) 2020 Carlo Prelz, University of Bern

carlo.prelz@humdek.unibe.ch

This file is part of Platform Commander.

Platform Commander is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Platform Commander is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
==========================================================================

A few ruby constants & utilities to help the C code that manages the GL engine

=end

require('cx')
require('glengine.so')

BASE='/moog_base/'
SHDBASE=BASE+'shaders/'
COLLADABASE=BASE+'collada/'
DEG2RAD=Math::PI/180.0
RAD2DEG=180.0/Math::PI
CAM_DISPLAY=ENV['CAM_DISPLAY'] ? ENV['CAM_DISPLAY'] : ':0.0'
HMD_DISPLAY=ENV['HMD_DISPLAY'] ? ENV['HMD_DISPLAY'] : ':1.0'

class Glengine
  def self::adapt_shader_source(path,n_lights,n_nodes,n_points,n_trias,n_textures)
    stuff=File::read(path)
    if(n_textures>0)
      texture_decl=n_textures.times.to_a.map do |v|
        sprintf("uniform sampler2D texture%3.3d;",v+1)
      end.join("\n")
      texture_blk=n_textures.times.to_a.map do |v|
        "#{v>0 ? 'else' : ''} if(nt==#{v})\ntexv=texture(texture#{sprintf("%3.3d",v+1)},txtr_v).rgb*1.5;\n"
      end .join("\n")
    else
      texture_decl=''
      texture_blk=''
    end         
    
    [['N_LIGHTS',n_lights],['N_NODES',n_nodes],['N_POINTS',n_points],['N_TRIAS',n_trias],
     ['TEXTURE_DECL',texture_decl],['TEXTURE_BLK',texture_blk]].each do |lbl,val|
      stuff.gsub!(/\<\<\<#{lbl}\>\>\>/,val.to_s)
    end
    #    lg("!!! #{path}:\n#{stuff}")
    stuff
  end
  
  def self::convert_col_arr_to_num(a)
    (a[0]<< 16)|(a[1]<< 8)|a[2]
  end
end

module Moo
  def self::find_input_device(name)
    base='/dev/input/by-id/'
    regex=/#{name}/
    found=nil
    Dir::foreach(base) do |n|
      next if(n[0,1]=='.')
      if(regex.match(n))
        found=File::readlink(base+n)
        break
      end
    end
    return false unless(found)
    '/dev/input/'+found.split('/')[-1]
  end
end
