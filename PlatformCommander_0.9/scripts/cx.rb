require 'pp'
require 'fileutils'

def mysystem(cmd,fail=true)
  lg("Executing <#{cmd}>")
  raise "ERROR!" unless(system(cmd) | !fail)
end

def lg(msg)
  STDERR.puts('['+Time::now().stamp+'] '+msg)
  STDERR.flush()
end

class Time
  def stamp
    #    sprintf("%.2d%.2d%.2d.%.2d%.2d%.2d",year%100,month,mday,hour,min,sec)
    strftime('%y%m%d.%H%M%S')
  end
end

class Array
  PER_LINE=32
  
  def hexprint(outf=STDERR)
    l=self.length()

    0.step(l,PER_LINE) do |i|
      outf.printf("%4.4x ",i)
      PER_LINE.times do |j|
        outf.print((i+j<l) ? sprintf("%2.2x ",self[i+j]) : '   ')
      end
      outf.print('| ')
      PER_LINE.times do |j|
        break if((i+j)>=l)
        v=self[i+j]
        outf.print((v>=32 && v<127) ? v.chr : '.')
      end
      outf.print("\n")
    end
  end
end
