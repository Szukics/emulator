# gtk.rb

=begin

***--MANAGED--***

==========================================================================
Copyright (C) 2020 Carlo Prelz, University of Bern

carlo.prelz@humdek.unibe.ch

This file is part of Platform Commander.

Platform Commander is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Platform Commander is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
==========================================================================

Some Gtk3 code that can be shared by several applications

=end

require('gtk3')

class Leddino < Gtk::Fixed
  BORDER=5
  
  def initialize(size,cond_col)
    super()
    
    @size,@cond_col=size,cond_col
    @cur_col=@cond_col[0]

    @fsize=@size+BORDER*2
    
    set_size_request(@fsize+BORDER*2,@fsize+BORDER*2)
    
    @da=Gtk::DrawingArea::new

    @da.set_size_request(@fsize,@fsize)
    @da.signal_connect('draw') do |w,cr|
      cr.set_source_rgb(@cur_col)
      cr.rectangle(0,0,@fsize,@fsize)
      cr.fill

      true
    end
    put(@da,BORDER,BORDER)
  end

  def set_col(v)
    raise "BAD LED VALUE (#{v})" if(v<0 || v>=@cond_col.length)
    if(@cur_col!=@cond_col[v])
      @cur_col=@cond_col[v]
      @da.queue_draw()
    end
  end  
end

class Actu_box < Gtk::Frame
  LED_COLS=[[0.2,0.2,0.2],[1,0,0]]
  BAR_COLS=[[0.0,0.0,0.0],[0.3,0.3,0.8]]
  LED_SIZE=10
  BAR_MIN_SIZE=400

  def initialize(prog,min,max)
    @prog,@min,@max=prog,min,max

    super("Actu ##{@prog+1}")

    set_size_request(LED_SIZE*2+BAR_MIN_SIZE,-1)

    vbox=Gtk::Box::new(:vertical)
    add(vbox)
    hbox=Gtk::Box::new(:horizontal)
    vbox.pack_start(hbox)

    @led1=Leddino::new(LED_SIZE,LED_COLS)
    hbox.pack_start(@led1,:expand=>false,:fill=>false)

    @l=Gtk::DrawingArea::new
    @l.signal_connect('draw') do |w,cr|
      act_v=(@v-min)/(max-min)
      
      wi=w.allocation.width
      he=w.allocation.height
      cr.set_source_rgb(BAR_COLS[0])
      cr.rectangle(0,0,wi,he)
      cr.fill
      v=wi.to_f*act_v
      cr.set_source_rgb(BAR_COLS[1])
      cr.rectangle(0,0,v,he)
      cr.fill

      true
    end

    # @l=Gtk::LevelBar::new()
    # @l.set_size_request(0,0)
    # @l.set_min_value(@min)
    # @l.set_max_value(@max)
    hbox.pack_start(@l,:expand=>true,:fill=>true)

    @led2=Leddino::new(LED_SIZE,LED_COLS)
    hbox.pack_start(@led2,:expand=>false,:fill=>false)

    @lbl=Gtk::Label::new('***')
    vbox.pack_start(@lbl)

    @v=nil
  end

  def set_value(v,l1,l2)
    @v=v
    @l.queue_draw()
    @led1.set_col(l1)
    @led2.set_col(l2)
  end

  def set_string(v1,v2)
    s=sprintf("%5.1f|%5.1f|%5.1f",@v*1000.0,v1,v2)
    @lbl.set_text(s)
  end
end

class Val
  attr_reader(:name,:min,:max,:startv,:cur)
  attr_accessor(:adj)
  
  def initialize(name,min,max,startv)
    @name,@min,@max,@startv=name,min,max,startv
    @adj=false
    @cur=@startv
  end

  def set(v)
    @cur=v
    @adj.value=@cur
  end
  
  def update(mainc,prog,i,val)
    @cur=val
    mainc.changed(prog,i)
  end

  def self::widg(mainc,prog,name,arr)    
    f=Gtk::Frame::new(name)
    b=Gtk::Box::new(:horizontal)
    f.add(b)
    arr.each_with_index do |val,i|
      b2=Gtk::Box::new(:vertical)
      b.pack_start(b2,expand:true)
      b2.pack_start(Gtk::Label::new(val.name))
      val.adj=Gtk::Adjustment.new(val.startv,val.min,val.max,1.0,3.0,0.0)
      val.adj.signal_connect('value-changed') do |widget|
        val.update(mainc,prog,i,widget.value)
      end      
      scale=Gtk::Scale::new(:vertical,val.adj)
      scale.set_size_request(-1,200)
      b2.pack_start(scale,expand:true)
    end
    f
  end
end

class Val_spin < Gtk::Frame
  attr_reader(:mainc,:lbl,:min,:max,:defv,:cur)
  
  def initialize(mainc,lbl,min,max,defv)
    @mainc,@lbl,@min,@max,@defv=mainc,lbl,min,max,defv
    super(@lbl)

    @cur=@defv
    
    @adj=Gtk::Adjustment.new(@defv,@min,@max,0.1,10.0,0.0)
    @adj.signal_connect "value-changed" do |widget|
      @cur=widget.value
      mainc.spin_changed(self)
    end
    sb=Gtk::SpinButton::new(@adj)
    sb.set_digits(1)
    add(sb)
  end
end

class Led2 < Gtk::Fixed
  BORDER=5
  
  def initialize(name,size,col)
    super()
    
    @name,@size,@col=name,size,col
    @fsize=@size*2+BORDER*2
    
    set_size_request(@fsize,@fsize)

    @cond=true
    
    @da=Gtk::DrawingArea::new

    @da.set_size_request(@fsize,@fsize)
    @da.signal_connect('draw') do |w,cr|
      cr.set_source_rgb([0.0,0.0,0.0])
      cr.rectangle(0,0,@fsize,@fsize)
      cr.fill
      if(@cond)
        cr.set_source_rgb(@col)
        cr.arc(@size+BORDER,@size+BORDER,@size,0,Math::PI*2)
        cr.fill
      end
      true
    end
    put(@da,0,0)
  end

  def set_cond(cond)
    if(@cond!=cond)
      @cond=cond
      @da.queue_draw()
    end
  end  
end

class Moog_console < Gtk::Frame
  LED_SIZE=25
  LED_COL=[0.9,0.3,0.3]
  
  def initialize(app)
    @app=app
    
    super('Moog console')

    vb=Gtk::Box.new(:vertical)
    add(vb)

    hb=Gtk::Box.new(:horizontal)
    vb.pack_start(hb,:expand=>false,:fill=>false)

    hb.pack_start(Gtk::Label::new("Mode: "),:expand=>false,:fill=>false)
    @mode_lbl=Gtk::Label::new("***")
    @mode_lbl.style_context.add_class('hilitlabel')
    hb.pack_start(@mode_lbl,:expand=>true,:fill=>true)

    hb=Gtk::Box.new(:horizontal)
    vb.pack_start(hb,:expand=>false,:fill=>false)

    hb.pack_start(Gtk::Label::new("Server state: "),:expand=>false,:fill=>false)
    @srvr_state_lbl=Gtk::Label::new("***")
    @srvr_state_lbl.style_context.add_class('hilitlabel')
    hb.pack_start(@srvr_state_lbl,:expand=>true,:fill=>true)
    hb.pack_start(Gtk::Label::new("Moog state: "),:expand=>false,:fill=>false)
    @moog_state_lbl=Gtk::Label::new("***")
    @moog_state_lbl.style_context.add_class('hilitlabel')
    hb.pack_start(@moog_state_lbl,:expand=>true,:fill=>true)

    cnt=0
    @leds=[]
    3.times do      
      hb=Gtk::Box.new(:horizontal)
      vb.pack_start(hb,:expand=>false,:fill=>false)
      8.times do
        l=Led2::new(Moo::RESP_LABELS[cnt],LED_SIZE,LED_COL)
        cnt+=1
        @leds.push(l)
        hb.add(l)
      end
    end

    @actuboxes=[]
    @ranges=[[Moo::LX_MIN,Moo::LX_MAX],[Moo::LY_MIN,Moo::LY_MAX],[Moo::LZ_MIN,Moo::LZ_MAX],
            [Moo::YAW_MIN,Moo::YAW_MAX],[Moo::PITCH_MIN,Moo::PITCH_MAX],[Moo::ROLL_MIN,Moo::ROLL_MAX]]
    Moo::N_ACTU.times do |i|
      ab=Actu_box::new(i,*(@ranges[i]))
      vb.add(ab,expand:true)
      @actuboxes.push(ab)
    end

    hb=Gtk::Box.new(:horizontal)
    vb.pack_start(hb,:expand=>false,:fill=>false)

    hb.pack_start(Gtk::Label::new,:expand=>true,:fill=>true)

    @btn_engage=Gtk::Button.new('Engage')
    @btn_engage.set_size_request(140,140)
    @btn_engage.style_context.add_class('redbtn')
    hb.pack_start(@btn_engage,:expand=>false,:fill=>false)

    @btn_engage.signal_connect('clicked') do
      @app.send(:engage)
    end

    @btn_park=Gtk::Button.new('Park')
    @btn_park.set_size_request(140,140)
    @btn_park.style_context.add_class('greenbtn')
    hb.pack_start(@btn_park,:expand=>false,:fill=>false)

    @btn_park.signal_connect('clicked') do
      @app.send(:park)
    end

    hb.pack_start(Gtk::Label::new,:expand=>true,:fill=>true)

  end

  def update_state(mode,sstate,mstate,leds,values)
    @mode_lbl.set_text(mode)
    @srvr_state_lbl.set_text(sstate)
    @moog_state_lbl.set_text(mstate)
    @leds.each_with_index do |l,i|
      l.set_cond(leds.include?(i+1))
    end

    rvalues=[values[0]*1000.0,-values[2]*1000.0,values[1]*1000.0,
             values[3]*Math::PI/180.0,values[4]*Math::PI/180.0,values[5]*Math::PI/180.0]
    @actuboxes.each_with_index do |ab,i|
      v=rvalues[i]
      ab.set_value(v,v<@ranges[i][0] ? 1 : 0,v>@ranges[i][1] ? 1 : 0)
      ab.set_string(0,0)
    end
  end
end
