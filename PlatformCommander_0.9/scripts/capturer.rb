# capturer.rb

=begin

***--MANAGED--***

==========================================================================
Copyright (C) 2020 Carlo Prelz, University of Bern

carlo.prelz@humdek.unibe.ch

This file is part of Platform Commander.

Platform Commander is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Platform Commander is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
==========================================================================

Some code to capture screen on movie using ffmpeg

=end

class Moo::Capturer
  def initialize(path)
    @path=path
    @pid=nil
  end
#  ffmpeg -f x11grab -video_size 1200x1200  -framerate 25 -i :100.1 /tmp/out.mp4
  def capture(display,x,y,w,h)
    if(@pid)
      lg("Capture on #{@path}: sorry! Already capturing.")
      return false
    end
    FileUtils::rm_rf(@path)
    
    # cmd=[['/usr/bin/ffmpeg','capturing to '+@path],'-loglevel','quiet',
    #      '-f','x11grab','-video_size',"#{w}x#{h}",
    #      '-framerate','25','-i',display,@path,]
    # @pid=fork do
    #   STDOUT.reopen('/dev/null','w')
    #   STDERR.reopen('/dev/null','w')
    #   exec(*cmd)
    # end
    #avconv -f x11grab -s xga -r 25 -i :0.0 simple.mpg
    #cmd="/usr/bin/avconv -f x11grab -s #{w}x#{h} -r 25 -i #{display} #{@path}"
    #@pid=fork do
    #  mysystem(cmd)
    #end
         
    cmd=['/usr/bin/avconv','-loglevel','quiet','-f','x11grab','-s',"#{w}x#{h}",
         '-r','25','-i',display,@path]
    @pid=fork do
      STDOUT.reopen('/dev/null','w')
      STDERR.reopen('/dev/null','w')
      exec(*cmd)
    end
    
    lg("Capturing to #{@path}: pid #{@pid}")
    true
  end
  
  def stop
    unless(@pid)
      lg("Stop capture on #{@path}: sorry! Not capturing.")
      return false
    end
    begin
      Process::kill("INT",@pid)
      Process::waitpid(@pid)
    rescue
    end
    lg("Stopped capturing to #{@path}: pid #{@pid} ended.")
    true
  end

  def self::open(path)
    if(Moo::DEVMODE)
      lg("Sorry. Cannot capture in dev mode.")
      return nil
    end

    new(path)
  end
end
    
    
