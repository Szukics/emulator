# master.rb

=begin

***--MANAGED--***

==========================================================================
Copyright (C) 2020 Carlo Prelz, University of Bern

carlo.prelz@humdek.unibe.ch

This file is part of Platform Commander.

Platform Commander is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Platform Commander is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
==========================================================================

The script to be executed to manage the Moog platform.

Is responsible for:

* A Moo::Server instance with two threads:
  * one that sends UDP messages to the Moog at 60Hz
  * one that processes current displacement requests, interpolating along time.
* The two UDP sockets to manage the protocol with clients
* A GTK screen to permit status display and emergency interaction
* Session logging

=end

require_relative('moo')
require('activator.rb')
require('socket')

include(Maths)
include(Moo)

module Moo
  require_relative('capturer')
  require_relative('post_production')

  EOSEQ_CONST=-0xff

  class Seq
    attr_reader(:desc,:max_speed,:rotcenter,:steps,:run_flg)
    
    def initialize(desc,session,max_speed,rotcenter,abort_ack_cmd)
      @desc,@session,@max_speed,@rotcenter,@abort_ack_cmd=desc,session,max_speed,rotcenter,abort_ack_cmd

      @steps=[]
      reset()
    end

    def add_step(dof,time,mode)
      @steps.push([dof,time,mode])
    end

    def basic_fire
      @session.srvr.modify_rotcenter(@rotcenter)
      
      @run_flg=Time::now.to_f
      cnt=@run_flg
      @step_times=@runsteps.map do |s|
        cnt+=(s[1].is_a?(Maths::Accsin) || s[1].is_a?(Maths::Dho)) ? s[1].end_pos_time[2] : s[1]
        cnt
      end
      @last_step=-1
    end

    def loop
      raise "Cannot loop!" unless(@run_flg)
      now=Time::now.to_f
      cs=EOSEQ_CONST # MUST BE DIFFERENT FROM THE CONSTANT @last_step WAS SET AT OBJECT CREATION
      @step_times.each_with_index do |t,i|
        if(now<t)
          cs=i
          break
        end
      end
      if(cs!=@last_step)
        if(cs==EOSEQ_CONST)
          @session.master.msg("#{@desc} completed",20)
          return nil
        end
        @last_step=cs
        @session.master.msg("#{@desc} st.#{@last_step+1}/#{@runsteps.length}",20)
        a=@runsteps[@last_step]
        
#        res=@session.srvr.feed_action(a[0],((a[1].is_a?(Maths::Accsin) ? a[1].end_pos_time[2] : a[1])*1000.0).to_i,a[2],true)
        res=@session.srvr.feed_action(a[0],(a[1].is_a?(Maths::Accsin) || a[1].is_a?(Maths::Dho)) ? a[1] : (a[1]*1000.0).to_i,a[2],true)
      end
      @last_step
    end

    def abort
      if(@run_flg)
        @session.master.msg("#{@desc} ABORTED")        
      else
        lg("Warning: seq abort received, but seq not executing")
      end
      @session.srvr.freeze
      @session.send_cmd(@abort_ack_cmd)
      reset()
    end

    def reset
      @run_flg=nil
    end

    def pos_at_end?(step_nr)
      raise "Bad parameter (#{step_nr})" if(step_nr<0 || step_nr>=@steps.length)

      s=@steps[step_nr]
      if(s[2]==Moo::ALG_SINEACC)
        a=s[1].end_pos_time()
        return a[0]+a[1]
      end
      
      return s[0].pack('f*')
    end                   
  end

  class Short_seq < Seq
    attr_reader(:prog)
    
    @@prog=0
    
    def initialize(session,max_speed,rotcenter)
      @prog=@@prog
      @@prog+=1
      super("Short seq##{@prog}",session,max_speed,rotcenter,MSG_SHORT_SEQ_FREEZE_ACK)
    end

    def fire
      @runsteps=@steps.clone
      basic_fire
    end
  end

  class Long_seq < Seq
    attr_reader(:name)
    
    def initialize(name,session,max_speed,rotcenter)
      @name=name
      super("Long seq #{@name}",session,max_speed,rotcenter,MSG_LONG_SEQ_FREEZE_ACK)
      @must_position=false
    end

    def fire
      @wait_for_position=Time::now+@steps[0][1].to_f
      res=@session.srvr.feed_action(@steps[0][0],(@steps[0][1]*1000.0).to_i,@steps[0][2],true)
    end

    def start
      @runsteps=@steps[1..-1]
      basic_fire
    end

    def loop
      if(@wait_for_position)
        if(Time::now.to_f>=@wait_for_position.to_f)
          @session.master.msg("#{@desc} ready",20)        
          @wait_for_position=nil
          @session.send_cmd(Moo::MSG_LONG_SEQ_READY,@name)
        end
      elsif(@run_flg)
        return super()
      end
      true
    end

    def abort
      @wait_for_position=nil
      super()
    end
  end
  
  class Session
    LOGIN_NO_MOOG_TIMEOUT=5.0#1.5#0.3
#    IDLE_TIMEOUT=140*60
    IDLE_TIMEOUT=10*60

    #    IMMEDIATE_INIT_STEP=0.001
    #    IMMEDIATE_SLEEP=0.02

    HEIGHT_INIT_TOLERANCE=0.005 # in m
    BRING_TO_HEIGHT_SPEED=0.02 # in m/s
    
    JOYSTICK_ID='usb-Microsoft_SideWinder_Force_Feedback_2_Joystick-event-joystick'
    JOYSTICK_MAX_ANG=10.0*Moo::DEG2RAD
    JOYSTICK_MAX_H=0.08

    SURV_LOGFILE='.surv'

    #    XSENSE_DEV='/dev/ttyUSB0'
    #    XSENSE_RATE=100

    DHO_INIT_POS=1.0
    DHO_INIT_VEL=0.0
    DHO_T_STEP=0.01

    MIN_TOL_ANGLE_WHEN_CHG_ROTC=0.001
    MIN_TOL_LIN_WHEN_CHG_ROTC=0.0001

    #
    # @inst is the instance of the session type class, if it exists
    #
    
    attr_reader(:master,:rem_addr,:inst,:tstart,:tstart_ruby,:dir_base,:inport,:outport,:skt,:state,:latest_sts,:latest_states,
                :last_action,:type,:short_seq,:long_seq_being_executed,:srvr,:btn_labels,:sess_info_unit)
    
    def initialize(master,rem_addr,inport,type,mode,outport,btn_labels,inst)
      @master,@rem_addr,@inport,@type,@mode,@outport,@btn_labels,@inst=master,rem_addr,inport,type,mode,outport,btn_labels,inst
      @tstart=Cg::time_now
      @tstart_ruby=Time::at(@tstart[0].unpack('L')[0])
      
      @dir_base=Moo::SESSBASE+sprintf('%4.4d/%2.2d/%2.2d/%2.2d%2.2d%2.2d/',
                                      @tstart_ruby.year,@tstart_ruby.month,@tstart_ruby.day,@tstart_ruby.hour,@tstart_ruby.min,@tstart_ruby.sec)
      FileUtils::mkdir_p(@dir_base)
      
      File::open(@dir_base+INFO_FILE,'w') do |f|
        f.write <<-EOF
time##{@tstart_ruby.stamp}
zerotime##{[@tstart[1]].pack('m').gsub("\n",'')}
address##{@rem_addr}
inport##{@inport}
outport##{@outport}
type##{SESSION_TYPES[@type]}
mode##{COMMAND_MODES[@mode]}
btns##{@btn_labels.compact.join(',')}
EOF
        f.write(@inst.info_data()) if(@inst)
      end
      @sess_info_unit=File::open(@dir_base+SESS_FILE,'wb')

      case(@type)
      when(Moo::SESSION_SHORT_SEQ)        
        @short_seq=nil
      when(Moo::SESSION_LONG_SEQ)
        @long_seq_catalog={}
        @long_seq_being_defined=nil
      when(Moo::SESSION_JOYSTICK)
        @js=Moo::Joystick::new(JOYSTICK_ID)
        @js_cur_values=[0.0,Moo::ACTU_IDLE_H,0.0,0.0,0.0,0.0]
      end

      # begin
      #   @xsense=Moo::Xsense_client::new(XSENSE_DEV,@dir_base,Moo::Xsense_client::RCV_CALIBDATA,XSENSE_RATE)
      # rescue => err
      #   lg("Error opening Xsense: #{err}")
      #   @xsense=nil
      # end
      
      @latest_sts=nil
      tlower,tupper,@tstring=Platf::trias
      @srvr=Moo::Server::new(tlower.joints.flatten.pack('f*'),tupper.joints.flatten.pack('f*'),DEFAULT_ROT_CENTER,
                             @master.local_address,@master.moog_address,@master.moog_locport,@master.moog_remport,@dir_base)
      @surv=nil#Vive::Survive::new(@dir_base+SURV_LOGFILE,'LHR-4DFB6C18')

      set_state(@type!=Moo::SESSION_JOYSTICK ? SSTATE_IDLE : SSTATE_ENGAGING)

      lg("Session \"#{SESSION_TYPES[@type]}\" opened at #{@tstart_ruby.stamp} for #{@rem_addr}/#{@inport}/#{@outport}")

      @skt=UDPSocket::new('AF_INET')
      @skt.bind(@master.server_local_address,@master.server_outport)
      @skt.setsockopt(:SOCKET,:REUSEPORT,true)
      @skt.connect(rem_addr,@outport)

      @master.msg('Session started')

      @latest_states=nil
        
      #
      # set timeout for verifying moog connection
      #

      @alive=false
      @moog_conntest_timeout=Time::now.to_f+LOGIN_NO_MOOG_TIMEOUT
    end

    def to_s
      'S.'+@tstart_ruby.stamp
    end

    def touch
      @last_action=Time::now.to_f
    end
    
    def set_state(state)
      if(state==@state)
        lg("#{self.to_s}: State unchanged (#{SSTATE_DESC[state]})")
        return
      end

      touch()

      @state=state
      lg("#{self.to_s}: State now #{SSTATE_DESC[@state]}")
      @sess_info_unit.write(Cg::time_now[1]+'S'+[@state].pack('S'))

      case(@state)
      when(SSTATE_ENGAGING)
        @master.msg('ENGAGING')
        @srvr.engage(@mode)
        @last_init_time=nil
      when(SSTATE_ENGAGED)
        @master.msg('ENGAGED')
      when(SSTATE_PARKING)
        @master.msg('PARKING')
        @srvr.park
      when(SSTATE_IDLE)
        @master.msg('IDLE')
      when(SSTATE_FROZEN)
        lg("*** Freeze requested")
        @master.msg("MUST FREEZE")
        @srvr.freeze
      end
    end
    
    def send_cmd(msgid,body=nil)
      msg=Moo::prepare_master_pkt(Cg::time_diff_from_now(@tstart[1]),msgid,body)
      #      lg("Send [#{str}]") unless(msgid==Moo::MSG_STATE)
      @skt.send(msg,0)
      unless(msgid==Moo::MSG_STATE)
        @sess_info_unit.write(Cg::time_now[1]+'O'+[msg.length].pack('S')+msg)
        touch()
      end
    end

    def close
      lg("==> Closing")
      @skt.close
      @srvr.close
      File::open(@dir_base+INFO_FILE,'a') do |f|
        f.write <<-EOF
endtime##{Time::now.stamp}
EOF
      end
      @sess_info_unit.close
      @surv.stop_thr if(@surv)
      @js.close_thread if(@js)
#      @xsense.xs.close_thread if(@xsense)
      @inst.close if(@inst)
 
      @master.close_session_threads
      
      lg("==> Closed")
      @master.msg(nil)
    end

    def loop
      @inst.loop if(@inst)
#      @xsense.iter if(@xsense)

      if(@master.audioplayer)
        res=@master.audioplayer.poll        
        res.each do |pkt|
          chn=pkt[0].to_i
          send_cmd(MSG_AV_AUDIO_SAMPLE_ENDED,sprintf("%d,%d",chn,Cg::time_diff(@tstart[1],pkt[1])))
          @master.ui.wnd.stimulinfo.fill_labels(["C#{chn} ended",nil,nil]) if(@master.ui)
        end if(res)
      end
      
      if(@master.transdplayer)
        cur_idleflg=@master.transdplayer.poll_idle
        if(cur_idleflg!=@master.transdplayer_idle)
          cur_idleflg.each_with_index do |flg,i|
            if(@master.transdplayer_idle[i]!=flg && flg)
              @master.ui.wnd.stimulinfo.fill_labels([nil,nil,"C#{i} idle"]) if(@master.ui)
              send_cmd(MSG_AV_TRANSD_CHANNEL_IDLE,i.to_s)
            end
          end if(@master.transdplayer_idle)
          @master.transdplayer_idle=cur_idleflg
        end
      end
        
      @master.hwuser.poll(self,:process_hw_event) if(@master.hwuser)

      #
      # See if connection to moog was made
      #

      if(@moog_conntest_timeout && Time::now.to_f>@moog_conntest_timeout)
        unless(@alive)
          lg("ERROR! Did not receive anything from Moog.")
          send_cmd(Moo::MSG_LOGIN_NAK,'"Timeout trying to talk to Moog"')
          return false
        end
      
        send_cmd(MSG_LOGIN_ACK,@tstart_ruby.stamp)
        @moog_conntest_timeout=nil
      end

      @latest_sts=@srvr.cur_sts

      new_states=[@state,@latest_sts[19],@latest_sts[18][3]]
      if(@latest_states!=new_states)
        @latest_states=new_states
        @sess_info_unit.write(Cg::time_now[1]+'s'+@latest_states.pack('SSS'))
      end

      if(@latest_sts[20])
        @alive=true
        @new_pkt=true

        #
        # First of all check Moog sts for error states.
        #
        
        if(Moo::CRITICAL_MOOG_STATES.include?(@latest_states[2]))
          if(@state!=SSTATE_FAULT)
            if(@short_seq)
              @short_seq.abort
            elsif(@long_seq_being_executed)
              @long_seq_being_executed.abort
            end
            set_state(SSTATE_FAULT)
          end
        elsif(@state==SSTATE_FAULT) # out of fault - move back to idle
          set_state(SSTATE_IDLE)          
        end
        
        begin          
          send_state_pkt
        rescue => err
          lg("Error sending state pkt (#{err})!")
          if(@state==SSTATE_ENGAGED)
            set_state(SSTATE_PARKING)
          else
            return false
          end
        end
      end
      
      case(@state)
      when(SSTATE_ENGAGING)
        if(@latest_states[2]==Moo::MACH_STATE_ENGAGED)
          if(@type==Moo::SESSION_IMMEDIATE || @type==Moo::SESSION_SHORT_SEQ || @type==Moo::SESSION_LONG_SEQ)
            now=Time::now.to_f
            if(!@move_to_height_begun)
              @move_to_height_begun=now
              @move_to_height_diff=@master.height_at_start.abs/BRING_TO_HEIGHT_SPEED
              @move_to_height_end=@move_to_height_begun+@move_to_height_diff
            end

            if(now<=@move_to_height_end)           
              pos=(now-@move_to_height_begun)/@move_to_height_diff
              new_h=@master.height_at_start*pos
              #              STDERR.printf("<%f>",new_h)
              @srvr.feed_immediate_regardless([0.0,0.0,new_h,0.0,0.0,0.0].pack('f*'))
              @inst.loop if(@inst)
              return true
            end
            
            if(@type==Moo::SESSION_IMMEDIATE)
              send_cmd(MSG_IMMEDIATE_AT_HEIGHT)
            elsif(@type==Moo::SESSION_SHORT_SEQ)
              send_cmd(MSG_SHORT_SEQ_AT_HEIGHT)
            elsif(@type==Moo::SESSION_LONG_SEQ)
              send_cmd(MSG_LONG_SEQ_AT_HEIGHT)
            end
          end

          set_state(SSTATE_ENGAGED)
          send_cmd(MSG_ENGAGE_DONE)
        end
      when(SSTATE_PARKING)
        if(@latest_states[2]==Moo::MACH_STATE_IDLE)
          set_state(SSTATE_IDLE)
          send_cmd(MSG_PARK_DONE)
        end
      end

      if(@type==SESSION_JOYSTICK)
        if(@state==Session::SSTATE_FROZEN)
          return true
        end

        res=@js.poll
        if(res)
          chg_flg=false
          res.each do |a|
            next unless(a[0]==Moo::Joystick::EVENT_ABS)
            case(a[1])
            when(Moo::Joystick::VABS_MAIN_X)
              @js_cur_values[5]=-(JOYSTICK_MAX_ANG*a[2])
              chg_flg=true
            when(Moo::Joystick::VABS_MAIN_Y)
              @js_cur_values[4]=-(JOYSTICK_MAX_ANG*a[2])
              chg_flg=true
            when(Moo::Joystick::VABS_MAIN_Z)
              @js_cur_values[3]=JOYSTICK_MAX_ANG*a[2]
              chg_flg=true
            when(Moo::Joystick::VABS_FLAP)
              @js_cur_values[1]=Moo::ACTU_IDLE_H-(JOYSTICK_MAX_H*a[2])
              chg_flg=true
            end      
          end
          if(chg_flg && @latest_states[2]==Moo::MACH_STATE_ENGAGED)
            res=@srvr.feed_action_speed(@js_cur_values,@master.joystick_speed)
            @master.msg("Action feed error #{res}",10) if(res!=true && res[0]!='B')
          end          
        end
      end
      
      true
    end

    def latest_values
      return false unless(@new_pkt)
      @new_pkt=false
      [@latest_sts[6+Moo::N_ACTU,6],@latest_sts[6,Moo::N_ACTU]]
    end
    
    def send_state_pkt
      send_cmd(Moo::MSG_STATE,sprintf('%d,%d,%d,%f,%f,%f,%f,%f,%f',*@latest_states,*(@latest_sts[6+Moo::N_ACTU,6])))
    end
    
    def manage_immediate(pkt_id,msg)
      return false unless(pkt_id==Moo::MSG_IMMEDIATE_REQ)
      
      if(msg.length!=7)
        lg("Bad immediate message (#{msg})")
        return false
      end
      
      step_nr=msg[0].to_i
      dofv=(msg[1,6].map do |s|
          s.to_f
        end).pack('f*')
      
      unless(@state==Session::SSTATE_ENGAGED)
        send_cmd(MSG_IMMEDIATE_NAK,sprintf("%d,\"%s\"",step_nr,'Not engaged'))
        return true
      end
      
      res=@srvr.feed_immediate(dofv,@master.immediate_max_lin,@master.immediate_max_ang)
      if(res==true)
        #        @master.msg("Imm step #{step_nr} VALID",20)
        send_cmd(MSG_IMMEDIATE_ACK,sprintf("%d",step_nr))
      else
        @master.msg("Imm step #{step_nr} invalid (#{res}) (#{dofv.unpack('f*')})",10)
        send_cmd(MSG_IMMEDIATE_NAK,sprintf("%d,%s,%d,%f",step_nr,*res))
      end

      true
    end
    
    def manage_short_seq(pkt_id,msg)
      case(pkt_id)
      when(MSG_BEGIN_SHORT_SEQ)
        if(@latest_states[2]!=Moo::MACH_STATE_ENGAGED)
          send_cmd(MSG_SHORT_SEQ_NAK,'"Must be in engaged state"')
          return false
        end          
        if(@short_seq)
          send_cmd(MSG_SHORT_SEQ_NAK,'"Already defining short seq"')
          return false
        end

        max_speed=Moo::DEFAULT_MAX_SPEED
        rot_center=Moo::DEFAULT_ROT_CENTER

        if(msg.length>0)
          max_speed=msg[0].to_f
          if(msg.length>=4)

            #
            # Rot center order rcvd in meters and in Moog order (heave,surge,lateral - that is, y,z,x)
            # Must change sign of x and y
            #
            
            rot_center=[-msg[3].to_f*1000.0,-msg[1].to_f*1000.0,msg[2].to_f*1000.0].pack('fff')
          end
        end
        
        @short_seq=Short_seq::new(self,max_speed,rot_center)
        @master.msg("Defining #{@short_seq.desc}")
      when(MSG_SHORT_SEQ_STEP)
        if(!@short_seq)
          send_cmd(MSG_SHORT_SEQ_NAK,'"Not defining short seq"')
          return false
        end
        if(@short_seq.run_flg)
          send_cmd(MSG_SHORT_SEQ_NAK,'"Short seq in run mode"')
          return false
        end
        if(msg.length!=8)
          send_cmd(MSG_SHORT_SEQ_NAK,'"Bad step,"'+msg.join(','))
          @short_seq=nil
          return false
        end

        
        @short_seq.add_step(Moo::moog_idea_to_server_idea(msg[0,6].map do |s|
                                                            s.to_f
                                                          end),msg[6].to_f,which_algo(msg[7]))
        @master.msg("Def #{@short_seq.desc} step #{@short_seq.steps.length}",20) if(@short_seq.steps.length%100==0)
      when(MSG_COMPLETE_SHORT_SEQ)
        if(!@short_seq)
          send_cmd(MSG_SHORT_SEQ_NAK,'"Not defining short seq"')
          return false
        end
        if(@short_seq.run_flg)
          send_cmd(MSG_SHORT_SEQ_NAK,'"Short seq in run mode"')
          return false
        end
        if(@short_seq.steps.length<=0)
          send_cmd(MSG_SHORT_SEQ_NAK,'"Empty short seq"')
          @short_seq=nil
          return false
        end

        srv_lv=Moo::moog_idea_to_server_idea(@latest_sts[6+Moo::N_ACTU,6])

        #
        # If a change of rotation center takes place, both the current position and
        # the first step of the sequence must have yaw, pitch & roll =0.0
        #

        cr=@srvr.cur_rotcenter.unpack('fff')
        first_step_ang=nil
        @short_seq.steps.each do |s|
          if(s[0])
            first_step_ang=s[0][3,3]
            break
          end
        end        
        
        a=@short_seq.rotcenter.unpack('fff')
        if(((cr[0]-a[0]).abs>MIN_TOL_LIN_WHEN_CHG_ROTC || (cr[1]-a[1]).abs>MIN_TOL_LIN_WHEN_CHG_ROTC || (cr[2]-a[2]).abs>MIN_TOL_LIN_WHEN_CHG_ROTC) &&
           first_step_ang && (srv_lv[3].abs>MIN_TOL_ANGLE_WHEN_CHG_ROTC ||
                              srv_lv[4].abs>MIN_TOL_ANGLE_WHEN_CHG_ROTC ||
                              srv_lv[5].abs>MIN_TOL_ANGLE_WHEN_CHG_ROTC ||
                              first_step_ang[0]!=0.0 || first_step_ang[1]!=0.0 || first_step_ang[2]!=0.0))
          send_cmd(MSG_SHORT_SEQ_NAK,sprintf("\"Rot. center changes from %.2f,%.2f,%.2f to %.2f,%.2f,%.2f, but either cur angular (%.3f,%.3f,%.3f) or first step angular (%.3f,%.3f,%.3f) are not all zero\"",
                                             *cr,*a,
                                             srv_lv[3],srv_lv[4],srv_lv[5],
                                             first_step_ang[0],first_step_ang[1],first_step_ang[2]))
          @master.msg("Problems with rot.center",20)
          return false
        end
        
        #
        # Test sequence
        #        
        
        res=Moo::quick_verify(@tstring,@short_seq.rotcenter,srv_lv[0,3],srv_lv[3,3],@short_seq.steps,Moo::TSTEP_SEC,@short_seq.max_speed)
        if(res[0]!=true)
          ermsg=sprintf("%d,%d,%d,%d,\"%s\",%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f",res[0],res[1],res[2],res[3],
                        Moo::QUICKTEST_ERROR_LABELS[res[4]],*(Moo::server_idea_to_moog_idea(res[5,6])),*(res[11,Moo::N_ACTU]),res[11+Moo::N_ACTU])
          send_cmd(MSG_SHORT_SEQ_NAK,ermsg)
          @master.msg("Ssq##{@short_seq.prog} BAD at #{res[0]}msec")
          @short_seq=nil
          return false
        end          
        
        @short_seq.fire
        send_cmd(MSG_SHORT_SEQ_ACK,sprintf("%d,%d,%f",@short_seq.prog,@short_seq.steps.length,res[1]))
        @master.msg("Short seq ##{@short_seq.prog} executing")
      when(MSG_SHORT_SEQ_FREEZE)
        @master.msg("Rcvd short seq freeze req")
        unless(@short_seq)
          lg("Warning: short seq abort received, but no seq defined")
          return false
        end
        
        send_cmd(MSG_SHORT_SEQ_FREEZE_ACK)
        @master.freeze_session
      end
      
      true
    end
    
    def abort_short_seq
      return unless(@short_seq)
      @short_seq.abort
      @short_seq=nil
    end

    def terminate_short_seq
      if(!@short_seq)
        lg("Warning: trying to terminate short seq but none available")
      else
        send_cmd(MSG_SHORT_SEQ_DONE,sprintf("%d,%d",@short_seq.prog,@short_seq.steps.length))
        @short_seq=nil
      end
    end
    
    def manage_long_seq(pkt_id,msg)
      case(pkt_id)
      when(MSG_BEGIN_LONG_SEQ)
        if(@latest_states[2]!=Moo::MACH_STATE_IDLE)
          send_cmd(MSG_LONG_SEQ_NAK,'"Must be in idle state"')
          return false
        end          
        if(msg.length<1)
          send_cmd(MSG_LONG_SEQ_NAK,'"Bad begin"')
          return false
        end
        if(@long_seq_being_defined)
          send_cmd(MSG_LONG_SEQ_NAK,'"Already defining [#{@long_seq_being_defined}]. Please close first."')
          return false
        end
        @long_seq_being_defined=msg[0]
        if(@long_seq_catalog[@long_seq_being_defined])
          @master.msg("REdef longseq #{@long_seq_being_defined}",20)
        else
          @master.msg("Def longseq #{@long_seq_being_defined}",20)
        end

        max_speed=Moo::DEFAULT_MAX_SPEED
        rot_center=Moo::DEFAULT_ROT_CENTER
        
        if(msg.length>1)
          max_speed=msg[1].to_f
          if(msg.length>=5)

            #
            # Rot center order rcvd in meters and in Moog order (heave,surge,lateral - that is, y,z,x)
            # Must change sign of x and y
            #
            
            rot_center=[-msg[4].to_f*1000.0,-msg[2].to_f*1000.0,msg[3].to_f*1000.0].pack('fff')
          end
        end
        
        @long_seq_catalog[@long_seq_being_defined]=Long_seq::new(@long_seq_being_defined,self,max_speed,rot_center)
        @accsin_values={}
        @dho_values={}
      when(MSG_LONG_SEQ_STEP)
        if(!@long_seq_being_defined)
          send_cmd(MSG_LONG_SEQ_NAK,'"Not defining long seq"')
          return false
        end
        if(msg.length!=8)
          send_cmd(MSG_LONG_SEQ_NAK,'"Bad step,"'+msg.join(','))
          @long_seq_catalog[@long_seq_being_defined]=nil
          @long_seq_being_defined=nil
          return false
        end
        seq=@long_seq_catalog[@long_seq_being_defined]
        
        #
        # Step definition will use the DOF order specified in the Moog manual:
        #
        # ROLL
        # PITCH
        # heave (my y, in m)
        # surge (my z, in m)
        # YAW
        # lateral (my x, in m)
        #
        # Here I am mapping to x,y,z,YAW,PITCH,ROLL
        #

        stp=Moo::moog_idea_to_server_idea(msg[0,6].map do |s|
                                            s.to_f
                                          end)        
        seq.add_step(stp,msg[6].to_f,which_algo(msg[7]))
        @master.msg("Def #{seq.desc} step #{seq.steps.length}",20)
      when(MSG_LONG_SEQ_STEP_SINEACC)
        if(!@long_seq_being_defined)
          send_cmd(MSG_LONG_SEQ_NAK,'"Not defining long seq"')
          return false
        end
        
        if(msg.length!=10)
          send_cmd(MSG_LONG_SEQ_NAK,'"Bad step,"'+msg.join(','))
          @long_seq_catalog[@long_seq_being_defined]=nil
          @long_seq_being_defined=nil
          return false
        end
        type=ACCSIN_TYPES[msg[0].to_sym]
        unless(type)
          send_cmd(MSG_LONG_SEQ_NAK,"\"Bad accsin type #{msg[0]}\"")
          return false
        end
        
        seq=@long_seq_catalog[@long_seq_being_defined]
        if(seq.steps.length<1)
          send_cmd(MSG_LONG_SEQ_NAK,'"First step cannot be SINEACC"')
          return false
        end
        stp=Moo::moog_idea_to_server_idea(msg[1,6].map do |s|
                                            s.to_f
                                          end)
        begin_pos=seq.pos_at_end?(seq.steps.length-1)
        paras=msg[7,3].map do |s|
          s.to_f
        end

        as=Maths::Accsin::new()
        res=as.load(begin_pos,stp.pack('f*'),type,*paras)
        if(res[0].is_a?(String) || res[1].is_a?(String))
          msg='"'+res.map do |a|
            a ? a : '-'
          end.join(' || ')+'"'
          send_cmd(MSG_LONG_SEQ_NAK,msg)
          return false
        end
        @accsin_values[seq.steps.length]=res
        seq.add_step(stp,as,Moo::ALG_SINEACC)
        @master.msg("Def #{seq.desc} accsin step #{seq.steps.length}. Returned values: #{res}.",10)
      when(MSG_LONG_SEQ_STEP_DAMPEDOSC)

        #
        # Paras:
        #
        # Usual 6 DOF's, plus
        #
        # mass, spring constant, damping, max time, energy threshold
        # (all floats)
        #
        
        if(!@long_seq_being_defined)
          send_cmd(MSG_LONG_SEQ_NAK,'"Not defining long seq"')
          return false
        end
        if(msg.length!=11)
          send_cmd(MSG_LONG_SEQ_NAK,"\"Bad step (len #{msg.length}),\""+msg.join(','))
          @long_seq_catalog[@long_seq_being_defined]=nil
          @long_seq_being_defined=nil
          return false
        end

        seq=@long_seq_catalog[@long_seq_being_defined]
        if(seq.steps.length<1)
          send_cmd(MSG_LONG_SEQ_NAK,'"First step cannot be DAMPEDOSC"')
          return false
        end

        stp=Moo::moog_idea_to_server_idea(msg[0,6].map do |s|
                                            s.to_f
                                          end)
        paras=msg[6,5].map do |s|
          s.to_f
        end

        dho=Maths::Dho::new(paras[0],paras[1],paras[2],DHO_INIT_POS,DHO_INIT_VEL,DHO_T_STEP,paras[3],paras[4])
        begin_pos=seq.pos_at_end?(seq.steps.length-1)
        dho.calc_seq(begin_pos,stp.pack('f*'))
        seq.add_step(stp,dho,Moo::ALG_DAMPEDOSC)
        @dho_values[seq.steps.length]=[dho.damp_success,dho.duration]
        @master.msg("Def #{seq.desc} dampedosc step #{seq.steps.length}. Damped? #{dho.damp_success}.",10)
      when(MSG_COMPLETE_LONG_SEQ)
        if(!@long_seq_being_defined)
          send_cmd(MSG_LONG_SEQ_NAK,'"Not defining long seq"')
          return false
        end
        seq=@long_seq_catalog[@long_seq_being_defined]
        if(seq.steps.length<=1)
          send_cmd(MSG_LONG_SEQ_NAK,'"Empty #{seq.desc}"')
          @long_seq_catalog[@long_seq_being_defined]=nil
          @long_seq_being_defined=nil
          return false
        end
        
        #
        # Test sequence
        #

        res=Moo::quick_verify(@tstring,seq.rotcenter,seq.steps[0][0][0,3],seq.steps[0][0][3,3],seq.steps[1..-1],Moo::TSTEP_SEC,seq.max_speed)
        if(res[0]!=true)
          ermsg=sprintf("%d,%d,%d,%d,\"%s\",%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f",res[0],res[1],res[2],res[3],
                        Moo::QUICKTEST_ERROR_LABELS[res[4]],*(Moo::server_idea_to_moog_idea(res[5,6])),*(res[11,Moo::N_ACTU]),res[11+Moo::N_ACTU])
          send_cmd(MSG_LONG_SEQ_NAK,ermsg)
          @master.msg("#{seq.desc} BAD at #{res[0]}msec",20)
          @long_seq_catalog[@long_seq_being_defined]=nil
          @long_seq_being_defined=nil
          return false
        end
        
        msg=sprintf("%s,%d,%f",seq.name,seq.steps.length,res[1])
        @accsin_values.each do |k,v|
          msg+=sprintf(",A%d|%f|%f",k,*v)
        end        
        @dho_values.each do |k,v|
          msg+=sprintf(",D%d|%s|%f",k,v[0] ? 'Damped' : 'Not damped',v[1])
        end        
        send_cmd(MSG_LONG_SEQ_ACK,msg)
        @master.msg("#{seq.desc} defined",20)
        @long_seq_being_defined=nil
      when(MSG_LONG_SEQ_AT_START)
        if(@long_seq_being_executed)
          send_cmd(MSG_LONG_SEQ_EXECUTE_NAK,'"A long seq is already being executed"')
          return false
        end
        if(msg.length!=1)
          send_cmd(MSG_LONG_SEQ_EXECUTE_NAK,'"Bad start syntax"')
          return false
        end
        seq=@long_seq_catalog[msg[0]]
        if(!seq)
          send_cmd(MSG_LONG_SEQ_EXECUTE_NAK,"\"Unknown seq #{msg[0]}\"")
          return false
        end

        srv_lv=Moo::moog_idea_to_server_idea(@latest_sts[6+Moo::N_ACTU,6])
 
        #
        # If a change of rotation center takes place, both the current position and
        # the first step of the sequence must have yaw, pitch & roll =0.0
        #

        cr=@srvr.cur_rotcenter.unpack('fff')
        a=seq.rotcenter.unpack('fff')

        if(((cr[0]-a[0]).abs>MIN_TOL_LIN_WHEN_CHG_ROTC || (cr[1]-a[1]).abs>MIN_TOL_LIN_WHEN_CHG_ROTC || (cr[2]-a[2]).abs>MIN_TOL_LIN_WHEN_CHG_ROTC) &&
           (srv_lv[3].abs>MIN_TOL_ANGLE_WHEN_CHG_ROTC ||
            srv_lv[4].abs>MIN_TOL_ANGLE_WHEN_CHG_ROTC ||
            srv_lv[5].abs>MIN_TOL_ANGLE_WHEN_CHG_ROTC ||
            seq.steps[0][0][3]!=0.0 || seq.steps[0][0][4]!=0.0 || seq.steps[0][0][5]!=0.0))
          send_cmd(MSG_LONG_SEQ_EXECUTE_NAK,sprintf("\"Rot. center changes from %.2f,%.2f,%.2f to %.2f,%.2f,%.2f, but either cur angular (%.3f,%.3f,%.3f) or first step angular (%.3f,%.3f,%.3f) are not all zero\"",
                                                    *cr,*a,
                                                    srv_lv[3],srv_lv[4],srv_lv[5],
                                                    seq.steps[0][0][3],seq.steps[0][0][4],seq.steps[0][0][5]))
          @master.msg("Problems with rot.center",20)
          return false
        end

        #
        # Here the motion from current position to start of sequence has to be tested
        #

        res=Moo::quick_verify(@tstring,seq.rotcenter,srv_lv[0,3],srv_lv[3,3],seq.steps[0,1],Moo::TSTEP_SEC,seq.max_speed)
        if(res[0]!=true)
          ermsg=sprintf("%d,%d,%d,%d,\"%s\",%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f",res[0],res[1],res[2],res[3],
                        Moo::QUICKTEST_ERROR_LABELS[res[4]],*(Moo::server_idea_to_moog_idea(res[5,6])),*(res[11,Moo::N_ACTU]),res[11+Moo::N_ACTU])
          send_cmd(MSG_LONG_SEQ_EXECUTE_NAK,ermsg)
          @master.msg("#{seq.desc}: pos to start BAD at #{res[0]}msec",15)
          return false
        end          
        @master.msg("#{seq.desc}: pos. at start",20)
        @long_seq_being_executed=seq
        @long_seq_being_executed.fire
      when(MSG_LONG_SEQ_EXECUTE)
        unless(@long_seq_being_executed)
          send_cmd(MSG_LONG_SEQ_EXECUTE_NAK,'"No long seq has been positioned at start"')
          return false
        end
        if(@long_seq_being_executed.run_flg)
          send_cmd(MSG_LONG_SEQ_EXECUTE_NAK,"\"#{@long_seq_being_executed.desc} already executing\"")
          return false
        end
        @long_seq_being_executed.start
      when(MSG_LONG_SEQ_FREEZE)
        @master.msg("Rcvd long seq freeze req")
        unless(@long_seq_being_executed)
          lg("Warning: long seq freeze received, but no seq being run")
          return false
        end

        send_cmd(MSG_LONG_SEQ_FREEZE_ACK)
        @master.freeze_session

      else
        lg("Long seq pkt #{pkt_id} unmanaged")

      end

      true
    end

    def abort_long_seq
      return unless(@long_seq_being_executed)
      @long_seq_being_executed.abort
      @long_seq_being_executed=nil
    end

    def terminate_long_seq
      if(!@long_seq_being_executed)
        lg("Warning: trying to terminate long seq but none available")
      else
        send_cmd(MSG_LONG_SEQ_DONE,@long_seq_being_executed.name)
        @long_seq_being_executed.reset
        @long_seq_being_executed=nil
      end
    end

    def process_kbinp(kbi)
      kbi.each do |bl,cond,t|
        @inst.proc_btn_press(bl,cond,t) if(@inst)
        td=Cg::time_diff(@tstart[1],t)
        send_cmd(MSG_DPIN_CHANGE,sprintf("%s,%d,%d",bl,cond ? 1 : 0,td))
        @sess_info_unit.write(t+'B'+bl.length.chr+bl+(cond ? 1 : 0).chr)
      end
    end

    def process_hw_event(id,time,type,pl)
      dev=Hw::Device::dev_by_id(id)
      td=Cg::time_diff(@tstart[1],time)
      case type 
      when Hw::DEV_GAMEPAD
        pla=pl.unpack('QQSSl')
        if(pla[2]==1)
          dev.feed_btn_act(pla[3],pla[4]!=0) do |b|
            send_cmd(MSG_HW_INPUT_EVENT,sprintf("%d,%s,BTN,%s,%d",td,dev.name,b.tag,b.cond ? 1 : 0))
          end
        end                        
      when Hw::DEV_SPNAV
        pla=pl.unpack('lll')
        if(pla[0]==1)
          dev.feed_btn_act(pla[1],pla[2]!=0) do |b|
            send_cmd(MSG_HW_INPUT_EVENT,sprintf("%d,%s,BTN,%s,%d",td,dev.name,b.tag,b.cond ? 1 : 0))
          end
        end
      end
    end

    def which_algo(letter)
      case(letter)
      when('L')
        Moo::ALG_LIN
      when('S')
        Moo::ALG_SIN
      when('C')
        Moo::ALG_CASTELJAU
      else
        raise "Sorry! Unknown algo #{letter}"
      end
    end
  end

  class Master
    attr_reader(:server_local_address,:server_inport,:server_outport,:local_address,:moog_address,:moog_locport,:moog_remport,:ui,
                :session,:immediate_max_lin,:immediate_max_ang,:height_at_start,:joystick_speed,
                :audioplayer,:stillsplayer,:moviesplayer,:transdplayer,:hwuser)
    attr_accessor(:transdplayer_idle)

    MAX_MILLISEC_INTERVAL=2000

    def initialize(server_local_address,server_inport,server_outport,local_address,moog_address,moog_locport,moog_remport,ui=nil)
      @server_local_address,@server_inport,@server_outport,@local_address,@moog_address,@moog_locport,@moog_remport,@ui=server_local_address,
      server_inport,server_outport,local_address,moog_address,moog_locport,moog_remport,ui
      
      FileUtils::mkdir_p(Moo::LOGBASE)
      Moo::open_logfile(Moo::LOGBASE,'master')
      
      #      @skt_sc=Master_skt_server_client::new(self)
      @skt_sc=Moo::Udplistener::new(@server_local_address,@server_inport)

      @session=nil
      @session_must_abort=false
      @audioplayer=nil
      @stillsplayer=nil
      @moviesplayer=nil
      @transdplayer=nil
      @transdplayer_idle=nil
      @send_next_ping=0.0
    end
    
    def loop
      if(@session)        
        if(@session.state==Session::SSTATE_ENGAGED)
          if(@session.type==Moo::SESSION_SHORT_SEQ && @session.short_seq && @session.short_seq.run_flg)
            res=@session.short_seq.loop
            if(!res)
              lg("Short seq completed")
              @session.terminate_short_seq()
            end
          elsif(@session.type==Moo::SESSION_LONG_SEQ && @session.long_seq_being_executed)
            res=@session.long_seq_being_executed.loop
            if(!res)
              lg("Long seq #{@session.long_seq_being_executed.name} completed")
              @session.terminate_long_seq()
            end
          end
        end
        if(@session_must_abort)
          if(@session.state!=Session::SSTATE_ENGAGED && @session.state!=Session::SSTATE_PARKING)
            lg("#{@session.rem_addr}/#{@session.inport} forced close.")
            @session.close
            @session=nil
            return
          end              
        elsif((Time::now.to_f-@session.last_action)>=Session::IDLE_TIMEOUT)
          lg("SESSION #{@session.rem_addr}/#{@session.inport} TIMED OUT!")
          @session_must_abort=true
          @session.set_state(Session::SSTATE_PARKING) if(@session.state==Session::SSTATE_ENGAGED)
          @session.send_cmd(Moo::MSG_IDLE_TIMEOUT)
        end

        ret=@session.loop
        unless(ret)
          @session.close
          @session=nil
          return
        end

        @moviesplayer.redraw if(@moviesplayer)          
      else
        now=Time::now.to_f     
        if(now>@send_next_ping)
          ping()
          @send_next_ping=now+PING_INTERVAL
        end
      end

      skt_sc_loop()
    end

    def skt_sc_loop
      pkts=@skt_sc.poll
      return unless(pkts)
      pkts.each do |pkt|
        process_incoming_msg(pkt[2],[nil,pkt[0],nil,pkt[1]])
      end
      
      #     begin
      #       @msg,@sender=@skt.recvfrom_nonblock(MAXSIZE)
      #     rescue => err         
      #       return false if(err.is_a?(IO::WaitReadable))
      #       lg("ERROR IN LOOP! #{err}")
      #       return false
      #     end
      #     @master.process_incoming_msg(@msg,@sender)
      #     true
    end

    def process_incoming_msg(msg,sender)
      ret=Moo::parse_client_pkt(msg)
      if(ret.is_a?(String))
        lg("Master: dropping msg coming from #{sender} (#{ret})")
        return
      end

      msg_id=ret[0]
      pkt=ret[1..-1]

#      lg("Got MSG ##{msg_id} (#{msg})")

      return process_message_not_connected(sender,msg_id,pkt,msg) unless(@session)

      @session.sess_info_unit.write(Cg::time_now[1]+'I'+[msg.length].pack('S')+msg)

      if(msg_id==Moo::MSG_LOGIN)
        lg("Login attempt from #{sender} DROPPED!")
        
        if(pkt.length>=2)
          his_port=pkt[1].to_i
          send_unconnected_msg(sender[3],his_port,MSG_LOGIN_NAK,'"session underway"')
        end
        return
      end

      if(sender[3]!=@session.rem_addr || sender[1]!=@session.inport)
        lg("Master: dropping msg #{msg_id} coming from #{sender} (session with #{@session.rem_addr}/#{@session.inport} ongoing)")
        return
      end
      
      if(msg_id==Moo::MSG_LOGOUT)
        if(@session.state!=Session::SSTATE_IDLE)
          lg("Trying to log out, but not idle!")
          @session.send_cmd(Moo::MSG_LOGOUT_NAK,'\"NOT IDLE\"')
          return
        end
        lg("#{@session.rem_addr}/#{@session.inport}/#{@session.outport} logging out.")

        #
        # If more parameters are present, consider them as millisec_interval plus email addresses and
        # try to generate the session report
        #
        
        if(pkt.length>0)
          millisec_interval=pkt.shift.to_i
          if(millisec_interval<=0 || millisec_interval>MAX_MILLISEC_INTERVAL)
            @session.send_cmd(Moo::MSG_LOGOUT_NAK,"\"Bad millisec interval (#{millisec_interval})\"")
            @session.close
            
            @session=nil
            return
          end
          
          @session.sess_info_unit.flush
          sleep(0.5)
          
          ppr=Post_producer::new(@session.dir_base,@session.tstart_ruby.stamp,millisec_interval)
          ppr.generate
          ppr.mail(pkt)
          
          # res=Moo::send_session_report(@session.dir_base,@session.tstart_ruby.stamp,millisec_interval,pkt)
          # if(res!=true)
          #   lg("Warning! error sending mail to #{pkt.join(', ')} (interval #{millisec_interval}): #{res}")
          #   msg("Error post-producing: #{res}",20)
          #   @session.send_cmd(Moo::MSG_LOGOUT_NAK,'\"Error post-producing\"')
          #   return
          # end
        end
        
        @session.send_cmd(Moo::MSG_LOGOUT_ACK)
        @session.close
        
        @session=nil
        return          
      elsif(msg_id==Moo::MSG_ENGAGE)
        if(@session.state!=Session::SSTATE_IDLE)
          lg("Trying to engage, but not idle! (#{@session.state})")
          @session.send_cmd(Moo::MSG_ENGAGE_NAK,'"NOT IDLE"')
          return
        end
        request_engage
        @session.send_cmd(Moo::MSG_ENGAGE_ACK)
        return
      elsif(msg_id==Moo::MSG_PARK)
        if(@session.state!=Session::SSTATE_ENGAGED)
          lg("Trying to park, but not engaged!")
          @session.send_cmd(Moo::MSG_PARK_NAK,'"NOT ENGAGED"')
          return
        end
        @session.send_cmd(Moo::MSG_PARK_ACK)
        @session.set_state(Session::SSTATE_PARKING)
        return
        
      #
      # AUDIO
      #
        
      elsif(msg_id==Moo::MSG_AV_OPEN_AUDIO)
        if(pkt.length!=1)
          @session.send_cmd(Moo::MSG_AV_AUDIO_NAK,'"Must send number of audio channels"')
          return
        end
        n_ch=pkt.shift.to_i
        if(@audioplayer)
          @session.send_cmd(Moo::MSG_AV_AUDIO_NAK,'"Player already open"')
          return
        end
        begin
          @audioplayer=Stimuli::Audio::new(Moo::AUDIO_BOARD,Moo::AUDIO_DSP,Moo::AUDIOBASE,n_ch)
        rescue => err
          @audioplayer=nil
          @session.send_cmd(Moo::MSG_AV_AUDIO_NAK,"\"#{err}\"")
          return
        end
        @session.send_cmd(Moo::MSG_AV_AUDIO_ACK)
      elsif(msg_id==Moo::MSG_AV_AUDIO_PLAY)
        unless(@audioplayer)
          @session.send_cmd(Moo::MSG_AV_AUDIO_NAK,'"Player not open"')
          return
        end
        if(pkt.length!=4)
          @session.send_cmd(Moo::MSG_AV_AUDIO_NAK,'"Must send sample name, channel, audio volume (pct) and repeat count"')
          return
        end
        sample_name=pkt.shift
        chn=pkt.shift.to_i
        vol=pkt.shift.to_f
        n_rpt=pkt.shift.to_i
        res=nil
        begin
          res=@audioplayer.play(sample_name,chn,vol,n_rpt)
        rescue => err
          @session.send_cmd(Moo::MSG_AV_AUDIO_NAK,"\"Trying to play #{sample_name} on ch#{chn}: #{err}\"")
          return
        end
        if(res)
          @session.send_cmd(Moo::MSG_AV_AUDIO_ACK)
          @ui.wnd.stimulinfo.fill_labels(["#{sample_name} (#{chn})",nil,nil]) if(@ui)
        else
          @session.send_cmd(Moo::MSG_AV_AUDIO_NAK,"\"Error playing #{sample_name}\"")
        end
      elsif(msg_id==Moo::MSG_AV_AUDIO_STOP)
        unless(@audioplayer)
          @session.send_cmd(Moo::MSG_AV_AUDIO_NAK,'"Player not open"')
          return
        end
        if(pkt.length!=1)
          @session.send_cmd(Moo::MSG_AV_AUDIO_NAK,'"Must send channel"')
          return
        end
        chn=pkt.shift.to_i
        begin
          res=@audioplayer.stop_channel(chn)
        rescue => err
          @session.send_cmd(Moo::MSG_AV_AUDIO_NAK,"\"Trying to stop chn #{chn}: #{err}\"")
          return
        end
        if(res)
          @session.send_cmd(Moo::MSG_AV_AUDIO_ACK)
        else
          @session.send_cmd(Moo::MSG_AV_AUDIO_NAK,"\"Error stopping chn #{chn}\"")
        end
        @ui.wnd.stimulinfo.fill_labels(["C#{chn} STOPPED",nil,nil]) if(@ui)
      elsif(msg_id==Moo::MSG_AV_AUDIO_STOP_ALL)
        unless(@audioplayer)
          @session.send_cmd(Moo::MSG_AV_AUDIO_NAK,'"Player not open"')
          return
        end
        begin
          res=@audioplayer.stop_all()
        rescue => err
          @session.send_cmd(Moo::MSG_AV_AUDIO_NAK,"\"Trying to stop all channels: #{err}\"")
          return
        end
        if(res)
          @session.send_cmd(Moo::MSG_AV_AUDIO_ACK)
        else
          @session.send_cmd(Moo::MSG_AV_AUDIO_NAK,"\"Error stopping all channels\"")
        end
        @ui.wnd.stimulinfo.fill_labels(["STOPPED",nil,nil]) if(@ui)

      #
      # STILLS
      #

      elsif(msg_id==Moo::MSG_AV_OPEN_STILLS)
        if(pkt.length!=4)
          @session.send_cmd(Moo::MSG_AV_STILLS_NAK,'"Must send x,y (resolution of stills), output def string, screen def string"')
          return
        end
        
        if(@stillsplayer)
          @session.send_cmd(Moo::MSG_AV_STILLS_NAK,'"Player already open"')
          return
        end
        
        rx=pkt.shift.to_i
        ry=pkt.shift.to_i
        od,sd=parse_still_def_strings(pkt.shift,pkt.shift)
        if(od.is_a?(String))
          @session.send_cmd(Moo::MSG_AV_STILLS_NAK,"\"Stills def string parsing bad (#{od})\"")
          return
        end
           
        begin
          @stillsplayer=Stimuli::Video::new(rx,ry,Moo::STILLBASE,nil,od,sd,@session.dir_base)
        rescue => err
          @stillsplayer=nil
          lg("WAWAWEAWA #{err}\n#{err.backtrace.join("\n")}")
          @session.send_cmd(Moo::MSG_AV_STILLS_NAK,"\"#{err}\"")
          return
        end
        @session.send_cmd(Moo::MSG_AV_STILLS_ACK)
      elsif(msg_id==Moo::MSG_AV_STILLS_STILL)
        unless(@stillsplayer)
          @session.send_cmd(Moo::MSG_AV_STILLS_NAK,'"Player not open"')
          return
        end
        if(pkt.length!=2)
          @session.send_cmd(Moo::MSG_AV_STILLS_NAK,'"Must send still name, screen number"')
          return
        end
        still_name=pkt.shift
        screen_no=pkt.shift.to_i

        begin
          @stillsplayer.feed_still(still_name,[screen_no])
        rescue => err
          @session.send_cmd(Moo::MSG_AV_STILLS_NAK,"\"Trying to display #{still_name} to #{screen_no}: #{err}\"")
          return
        end
        @session.send_cmd(Moo::MSG_AV_STILLS_ACK)
        @ui.wnd.stimulinfo.fill_labels([nil,"#{still_name}->#{screen_no}",nil]) if(@ui)
      elsif(msg_id==Moo::MSG_AV_STILLS_FILL_RGB)
        unless(@stillsplayer)
          @session.send_cmd(Moo::MSG_AV_STILLS_NAK,'"Player not open"')
          return
        end
        if(pkt.length!=4)
          @session.send_cmd(Moo::MSG_AV_STILLS_NAK,'"Must send r,g,b,screen number"')
          return
        end
        rgb=[pkt.shift.to_i,pkt.shift.to_i,pkt.shift.to_i]
        screen_no=pkt.shift.to_i
        begin
          @stillsplayer.feed_rgb(rgb,[screen_no])
        rescue => err
          @session.send_cmd(Moo::MSG_AV_STILLS_NAK,"\"Trying to fill scn #{screen_no} with #{rgb}: #{err}\"")
          return
        end
        @session.send_cmd(Moo::MSG_AV_STILLS_ACK)
        l=sprintf("RGB(%2.2x,%2.2x,%2.2x)->#{screen_no}",*rgb)
        @ui.wnd.stimulinfo.fill_labels([nil,l,nil]) if(@ui)
      elsif(msg_id==Moo::MSG_AV_STILLS_ACTIVATE)
        unless(@stillsplayer)
          @session.send_cmd(Moo::MSG_AV_STILLS_NAK,'"Player not open"')
          return
        end
        if(pkt.length!=1)
          @session.send_cmd(Moo::MSG_AV_STILLS_NAK,'"Must send screen number"')
          return
        end
        screen_no=pkt.shift.to_i
        begin
          @stillsplayer.activate(true,[screen_no])
        rescue => err
          @session.send_cmd(Moo::MSG_AV_STILLS_NAK,"\"Trying to activate scn #{screen_no}: #{err}\"")
          return
        end
        @session.send_cmd(Moo::MSG_AV_STILLS_ACK)
        @ui.wnd.stimulinfo.fill_labels([nil,"s##{screen_no} active",nil]) if(@ui)
      elsif(msg_id==Moo::MSG_AV_STILLS_DEACTIVATE)
        unless(@stillsplayer)
          @session.send_cmd(Moo::MSG_AV_STILLS_NAK,'"Player not open"')
          return
        end
        if(pkt.length!=1)
          @session.send_cmd(Moo::MSG_AV_STILLS_NAK,'"Must send screen number"')
          return
        end
        screen_no=pkt.shift.to_i
        begin
          @stillsplayer.activate(false,[screen_no])
        rescue => err
          @session.send_cmd(Moo::MSG_AV_STILLS_NAK,"\"Trying to deactivate scn #{screen_no}: #{err}\"")
          return
        end
        @session.send_cmd(Moo::MSG_AV_STILLS_ACK)
        @ui.wnd.stimulinfo.fill_labels([nil,"s##{screen_no} inactive",nil]) if(@ui)
      elsif(msg_id==Moo::MSG_AV_STILLS_GET_IPD)
        unless(@stillsplayer && @stillsplayer.ipd)
          @session.send_cmd(Moo::MSG_AV_STILLS_NAK,'"Player not open, or no HMD present"')
          return
        end
        @mst.session.send_cmd(Moo::MSG_AV_STILLS_IPD_VAL,@gle.hmd_get_ipd(@hmd).to_s)
      elsif(msg_id==Moo::MSG_AV_STILLS_SET_IPD)
        unless(@stillsplayer && @stillsplayer.ipd)
          @session.send_cmd(Moo::MSG_AV_STILLS_NAK,'"Player not open, or no HMD present"')
          return
        end
        if(pkt.length!=1)
          @session.send_cmd(Moo::MSG_AV_STILLS_NAK,'"Must send IPD value"')
          return
        end
        @stillsplayer.gle.hmd_set_ipd(@stillsplayer.hmd,pkt[0].to_f)
      elsif(msg_id==Moo::MSG_AV_STILLS_ROTATE_SCREEN)
        unless(@stillsplayer)
          @session.send_cmd(Moo::MSG_AV_STILLS_NAK,'"Player not open"')
          return
        end
        if(pkt.length!=4)
          @session.send_cmd(Moo::MSG_AV_STILLS_NAK,'"Must send screen nr,, yaw, pitch, roll, "')
          return
        end
        screen_no=pkt.shift.to_i
        y=pkt.shift.to_f
        p=pkt.shift.to_f
        r=pkt.shift.to_f
        @stillsplayer.rotate_screen(screen_no,y,p,r)
        @session.send_cmd(Moo::MSG_AV_STILLS_ACK)

      #
      # TRANSDUCER FILES
      #

      elsif(msg_id==Moo::MSG_AV_OPEN_TRANSD)
        if(pkt.length!=4)
          @session.send_cmd(Moo::MSG_AV_TRANSD_NAK,'"Must send type, subd, ports, freq"')
          return
        end
        if(@transdplayer)
          @session.send_cmd(Moo::MSG_AV_TRANSD_NAK,'"Player already open"')
          return
        end
        
        type=pkt.shift
        if(type==Moo::TRANSDUCER_VIRTUAL)
          comedi_dev=nil
        else
          dt=begin
               Moo::comedi_device_detector(type)
             rescue => err
               @session.send_cmd(Moo::MSG_AV_TRANSD_NAK,"\"Type #{type} invalid (#{err})\"")
               return
             end
          comedi_dev="/dev/comedi#{dt}"
        end
        
        subd=pkt.shift.to_i
        
        ports=pkt.shift.split('|').map do |s|
          a=s.split('/')
          if(a.length!=3)
            @session.send_cmd(Moo::MSG_AV_TRANSD_NAK,"\"Ports string invalid (#{s})\"")
            return
          end
          [a[0].to_i,a[1].to_f,a[2].to_f]
        end
        
        freq=pkt.shift.to_f
        
        @transdplayer_idle=Array::new(ports.length,true)
        begin
          @transdplayer=Stimuli::Transducer::new(Moo::TRANSDUCERBASE,Hw::Comedi_ao,comedi_dev,subd,ports,freq)
        rescue => err
          lg("TRANSDUCER OPEN ERROR #{err}\n#{err.backtrace.join("\n")}")
            
          @transdplayer_idle=nil
          @transdplayer=nil
          @session.send_cmd(Moo::MSG_AV_TRANSD_NAK,"\"#{err}\"")
          return
        end
        @session.send_cmd(Moo::MSG_AV_TRANSD_ACK)
      elsif(msg_id==Moo::MSG_AV_TRANSD_PLAY)
        unless(@transdplayer)
          @session.send_cmd(Moo::MSG_AV_TRANSD_NAK,'"Player not open"')
          return
        end
        if(pkt.length!=4)
          @session.send_cmd(Moo::MSG_AV_TRANSD_NAK,'"Must send port nr, sample name, immediate flg and repeat count"')
          return
        end
        port_nr=pkt.shift.to_i
        sample_name=pkt.shift
        immediate_flg=(pkt.shift)=='1' ? true : false
        n_rpt=pkt.shift.to_i

        res=begin
              @transdplayer.play(port_nr,sample_name,immediate_flg,n_rpt)
            rescue => err
              @session.send_cmd(Moo::MSG_AV_TRANSD_NAK,"\"Trying to play #{sample_name} on port #{port_nr}: #{err}\"")
              return
            end
        if(res)
          @session.send_cmd(Moo::MSG_AV_TRANSD_ACK)
          @ui.wnd.stimulinfo.fill_labels([nil,nil,sprintf("%s/%d",sample_name,port_nr)]) if(@ui)
        else
          @session.send_cmd(Moo::MSG_AV_TRANSD_NAK,"\"Error queueing #{sample_name} on port #{port_nr}\"")
        end
      elsif(msg_id==Moo::MSG_AV_TRANSD_STOP)
        unless(@transdplayer)
          @session.send_cmd(Moo::MSG_AV_TRANSD_NAK,'"Player not open"')
          return
        end
        if(pkt.length!=1)
          @session.send_cmd(Moo::MSG_AV_TRANSD_NAK,'"Must send port nr"')
          return
        end
        port_nr=pkt.shift.to_i
        @transdplayer.stop(port_nr)
        @ui.wnd.stimulinfo.fill_labels([nil,nil,false]) if(@ui)
        
      #
      # MOVIES
      #

      elsif(msg_id==Moo::MSG_AV_OPEN_MOVIES)
        if(pkt.length!=4)
          @session.send_cmd(Moo::MSG_AV_MOVIES_NAK,'"Must send x,y (resolution of screen), output def string, screen def string"')
          return
        end
        
        if(@moviesplayer)
          @session.send_cmd(Moo::MSG_AV_MOVIES_NAK,'"Player already open"')
          return
        end
        
        rx=pkt.shift.to_i
        ry=pkt.shift.to_i
        od,sd=parse_still_def_strings(pkt.shift,pkt.shift)
        if(od.is_a?(String))
          @session.send_cmd(Moo::MSG_AV_MOVIES_NAK,"\" output def string parsing bad (#{od})\"")
          return
        end
           
        begin
          @moviesplayer=Stimuli::Video::new(rx,ry,nil,Moo::MOVIEBASE,od,sd,@session.dir_base)
        rescue => err
          moviesplayer=nil
          lg("WAWAWEAWAmovie #{err}\n#{err.backtrace.join("\n")}")
          @session.send_cmd(Moo::MSG_AV_MOVIES_NAK,"\"#{err}\"")
          return
        end
        @session.send_cmd(Moo::MSG_AV_MOVIES_ACK)
      elsif(msg_id==Moo::MSG_AV_MOVIES_MOVIE)
        unless(@moviesplayer)
          @session.send_cmd(Moo::MSG_AV_MOVIES_NAK,'"Player not open"')
          return
        end
        if(pkt.length!=4)
          @session.send_cmd(Moo::MSG_AV_MOVIES_NAK,'"Must send movie name, screen number"')
          return
        end
        movie_name=pkt.shift
        screen_no=pkt.shift.to_i
        loop_flg=pkt.shift.to_i==1
        override_flg=pkt.shift.to_i==1
        begin
          @moviesplayer.feed_movie(movie_name,[screen_no],loop_flg,override_flg)
        rescue => err
          lg("WAWAWEAWOmovie #{err}\n#{err.backtrace.join("\n")}")
          @session.send_cmd(Moo::MSG_AV_MOVIES_NAK,"\"Trying to display #{movie_name} to #{screen_no}: #{err}\"")
          return
        end
        @session.send_cmd(Moo::MSG_AV_MOVIES_ACK)
        @ui.wnd.stimulinfo.fill_labels([nil,"#{movie_name}->#{screen_no}",nil]) if(@ui)
      elsif(msg_id==Moo::MSG_AV_MOVIES_ROTATE_SCREEN)
        unless(@moviesplayer)
          @session.send_cmd(Moo::MSG_AV_MOVIES_NAK,'"Player not open"')
          return
        end
        if(pkt.length!=4)
          @session.send_cmd(Moo::MSG_AV_MOVIES_NAK,'"Must send screen nr,, yaw, pitch, roll, "')
          return
        end
        screen_no=pkt.shift.to_i
        y=pkt.shift.to_f
        p=pkt.shift.to_f
        r=pkt.shift.to_f
        @moviesplayer.rotate_screen(screen_no,y,p,r)
        @session.send_cmd(Moo::MSG_AV_MOVIES_ACK)
      elsif(msg_id==Moo::MSG_AV_MOVIES_PAUSE)
        unless(@moviesplayer)
          @session.send_cmd(Moo::MSG_AV_MOVIES_NAK,'"Player not open"')
          return
        end
        if(pkt.length!=1)
          @session.send_cmd(Moo::MSG_AV_MOVIES_NAK,'"Must send 1 for pause, 0 for resume"')
          return
        end
        pause_flg=pkt.shift
        cond=nil
        if(pause_flg=='1')
          cond=true
        elsif(pause_flg=='0')
          cond=false
        else
          @session.send_cmd(Moo::MSG_AV_MOVIES_NAK,'"Bad pause condition (#{pause_flg})"')
          return
        end
        @moviesplayer.pause(cond)
      elsif(msg_id==Moo::MSG_AV_MOVIES_FILL_RGB)
        unless(@moviesplayer)
          @session.send_cmd(Moo::MSG_AV_MOVIES_NAK,'"Player not open"')
          return
        end
        if(pkt.length!=4)
          @session.send_cmd(Moo::MSG_AV_MOVIES_NAK,'"Must send r,g,b,screen number"')
          return
        end
        rgb=[pkt.shift.to_i,pkt.shift.to_i,pkt.shift.to_i]
        screen_no=pkt.shift.to_i
        begin
          @moviesplayer.feed_rgb(rgb,[screen_no])
        rescue => err
          @session.send_cmd(Moo::MSG_AV_MOVIES_NAK,"\"Trying to fill scn #{screen_no} with #{rgb}: #{err}\"")
          return
        end
        @session.send_cmd(Moo::MSG_AV_MOVIES_ACK)
        l=sprintf("RGB(%2.2x,%2.2x,%2.2x)->#{screen_no}",*rgb)
        @ui.wnd.stimulinfo.fill_labels([nil,l,nil]) if(@ui)
      elsif(msg_id==Moo::MSG_AV_MOVIES_GET_IPD)
        unless(@moviesplayer && @moviesplayer.ipd)
          @session.send_cmd(Moo::MSG_AV_MOVIES_NAK,'"Player not open, or no HMD present"')
          return
        end
        @mst.session.send_cmd(Moo::MSG_AV_MOVIES_IPD_VAL,@gle.hmd_get_ipd(@hmd).to_s)
      elsif(msg_id==Moo::MSG_AV_MOVIES_SET_IPD)
        unless(@moviesplayer && @moviesplayer.ipd)
          @session.send_cmd(Moo::MSG_AV_MOVIES_NAK,'"Player not open, or no HMD present"')
          return
        end
        if(pkt.length!=1)
          @session.send_cmd(Moo::MSG_AV_MOVIES_NAK,'"Must send IPD value"')
          return
        end
        @moviesplayer.gle.hmd_set_ipd(@moviesplayer.hmd,pkt[0].to_f)
        
      #
      # GENERIC MEDIA
      #
        
      elsif(msg_id==Moo::MSG_AV_INQUIRE)
        if(pkt.length!=2)
          @session.send_cmd(Moo::MSG_AV_INQUIRE_REPLY,"\"Bad packet (#{pkt})\"")
          return
        end
        case pkt[0]
        when 'A'
          unless(@audioplayer)
            @session.send_cmd(Moo::MSG_AV_INQUIRE_REPLY,"\"Audio request sent, but audio player not started\"")
            return
          end
          res=@audioplayer.exists?(pkt[1])
          @session.send_cmd(Moo::MSG_AV_INQUIRE_REPLY,sprintf("%s,%s,%d",pkt[0],pkt[1],res ? 1 : 0))
        when 'S'
          unless(@stillsplayer)
            @session.send_cmd(Moo::MSG_AV_INQUIRE_REPLY,"\"Stills existence request sent, but stills player not started\"")
            return
          end
          res=@stillsplayer.still_exists(pkt[1])
          @session.send_cmd(Moo::MSG_AV_INQUIRE_REPLY,sprintf("%s,%s,%d",pkt[0],pkt[1],res ? 1 : 0))
        when 'M'
          unless(@moviesplayer)
            @session.send_cmd(Moo::MSG_AV_INQUIRE_REPLY,"\"Movies existence request sent, but movie player not started\"")
            return
          end
          res=@moviesplayer.movie_exists(pkt[1])
          @session.send_cmd(Moo::MSG_AV_INQUIRE_REPLY,sprintf("%s,%s,%d",pkt[0],pkt[1],res ? 1 : 0))
        when 'T'
          unless(@transdplayer)
            @session.send_cmd(Moo::MSG_AV_INQUIRE_REPLY,"\"Transducer request sent, but transducer player not started\"")
            return
          end
          res=@transdplayer.exists?(pkt[1])
          @session.send_cmd(Moo::MSG_AV_INQUIRE_REPLY,sprintf("%s,%s,%d",pkt[0],pkt[1],res ? 1 : 0))
        else
          @session.send_cmd(Moo::MSG_AV_INQUIRE_REPLY,"\"Unknown media type #{pkt[0]}\"")
          return
        end
        
      #
      # HW INPUT
      #
        
      elsif(msg_id==Moo::MSG_ACTIVATE_HW_INPUT)
        if(pkt.length<1)
          @session.send_cmd(Moo::MSG_HW_INPUT_NAK,'"Must send at least one device."')
          return
        end
        if(@hwuser)
          @session.send_cmd(Moo::MSG_HW_INPUT_NAK,'"Hw input user already open"')
          return
        end
        
        @hwuser_inputs={}
        pkt.each do |s|
          a=s.split('|')
          if(a.length<2)
            @session.send_cmd(Moo::MSG_HW_INPUT_NAK,"\"Input element bad: #{s}\"")
            return
          end
          if(@hwuser_inputs[a[0]])
            @session.send_cmd(Moo::MSG_HW_INPUT_NAK,"\"Input element with id #{a[0]} doubly specified\"")
            return
          end
          type_a=Hw::Hw_user::DEVICES[a[1].to_sym]
          unless(type_a)
            @session.send_cmd(Moo::MSG_HW_INPUT_NAK,"\"Input element with id #{a[0]} has unknown type #{a[1]}\"")
            return
          end
          
        #
        # If needed, look for button names
        #

          @hwuser_inputs[a[0]]=(type_a[0]==Hw::DEV_GAMEPAD || type_a[0]==Hw::DEV_SPNAV) ? [a[0],a[1],a[2..-1]] : [a[0],a[1]]
        end
        begin
          @hwuser=Hw::Hw_user::new(@hwuser_inputs.values,@session.dir_base)
        rescue => err
          lg("Ouch! Error creating hw user daemon: #{err}\n#{err.backtrace.join("\n")}")
          @session.send_cmd(Moo::MSG_HW_INPUT_NAK,"\"#{err}\"")
          return
        end
        @session.send_cmd(Moo::MSG_HW_INPUT_ACK)
        
      #
      # Seed random generator
      #
        
      elsif(msg_id==Moo::MSG_SEED_RNDGEN)
        v=nil
        if(pkt.length<1)
          lg("Seeding random generator with time value")
          v=Time::now.to_i
        else
          v=pkt[0].to_i
          lg("Seeding random generator with #{v}")
        end
        srand(v)
        Maths::seed_randgen(v)

      else
        ret=nil
        if(@session.inst)
          ret=@session.inst.manage(msg_id,pkt)
        else          
          case(@session.type)
          when(Moo::SESSION_IMMEDIATE)
            ret=@session.manage_immediate(msg_id,pkt)
          when(Moo::SESSION_SHORT_SEQ)
            ret=@session.manage_short_seq(msg_id,pkt)
          when(Moo::SESSION_LONG_SEQ)
            ret=@session.manage_long_seq(msg_id,pkt)
          end
        end
        lg("Master: dropping msg #{msg_id} coming from #{sender} (unmanaged)") unless(ret)
      end
    end

    def parse_still_def_strings(o_s,s_s)
      a=o_s.split('|')
      outputs=a.map.with_index do |el,i|
        vi=Vive::VISOR_TAGS[el]
        next(el) if(vi) # pass string if visor is specified        
        aa=el.split('#')
        return "#{el}: badly specified screen (must be x,y,w,h)" unless(aa.length==4)
        ["s_#{i+1}",Moo::STIM_DISPLAY,aa.map do |el2|
           el2.to_i
         end]
      end
      a=s_s.split('|')
      screens=a.map.with_index do |el,i|
        aa=el.split('#')
        type=case aa[0]
             when 'F'
               Stimuli::Video::SCREEN_FLAT
             when 'SI'
               Stimuli::Video::SCREEN_SPHERICAL_INTERIOR
             when 'SE'
               Stimuli::Video::SCREEN_SPHERICAL_EXTERIOR
             else
               return "#{el}: bad video stimuli screen type (#{aa[0]})"
             end
        
        [type]+case type
               when Stimuli::Video::SCREEN_FLAT
                 return "#{el}: bad flat screen def (must have 8 paras)" if(aa.length!=9)
                 (aa[1..-1]).map do |el2|
                   el2.to_f
                 end
               when Stimuli::Video::SCREEN_SPHERICAL_INTERIOR,Stimuli::Video::SCREEN_SPHERICAL_EXTERIOR
                 return "#{el}: bad spherical screen def (must have 4 paras)" if(aa.length!=5)
                 (aa[1..-1]).map do |el2|
                   el2.to_f
                 end
               end
      end
      [outputs,screens]
    end
    
    def request_engage
      @session.set_state(Session::SSTATE_ENGAGING)
      @move_to_height_begun=nil
    end
    
    def request_park
      @session.set_state(Session::SSTATE_PARKING)
    end

    def process_message_not_connected(sender,msg_id,pkt,rawmsg)
      case(msg_id)
      when(MSG_LOGIN)
        lg("*** Received login request from #{sender}:\n\n#{rawmsg}\n\n")
        
        if(pkt.length<2)
          lg("Master: dropping login msg coming from #{sender} (bad payload)")
          return
        end

        sess_type=pkt.shift.to_i
        his_port=pkt.shift.to_i
        
        unless(SESSION_TYPES[sess_type])
          lg("Master: dropping login msg coming from #{sender} (unknown session type #{sess_type})")
          send_unconnected_msg(sender[3],his_port,MSG_LOGIN_NAK,'"bad session type"')
          return
        end

        mode=nil
        sess_inst=nil

        if(sess_type==SESSION_IMMEDIATE)
          if(pkt.length<3)
            lg("Master: dropping immediate login msg coming from #{sender} (bad payload)")
            send_unconnected_msg(sender[3],his_port,MSG_LOGIN_NAK,'"immediate login requires at least 5 parameters"')
            return
          end
          @immediate_max_lin=pkt.shift.to_f*1000.0
          @immediate_max_ang=pkt.shift.to_f
          @height_at_start=pkt.shift.to_f

          mode=Moo::MODE_DOF
        elsif(sess_type==SESSION_SHORT_SEQ || sess_type==SESSION_LONG_SEQ)
          if(pkt.length<2)
            lg("Master: dropping seq login msg coming from #{sender} (bad payload)")
            send_unconnected_msg(sender[3],his_port,MSG_LOGIN_NAK,'"seq login requires at least 4 parameters"')
            return
          end
          m=pkt.shift
          if(m=='L')
            mode=Moo::MODE_LENGTH
          elsif(m=='D')
            mode=Moo::MODE_DOF
          else
            lg("Master: dropping seq login msg coming from #{sender} (bad mode tag #{m})")
            send_unconnected_msg(sender[3],his_port,MSG_LOGIN_NAK,'"seq login: bad mode tag"')
            return
          end
            
          @height_at_start=pkt.shift.to_f          
        elsif(sess_type==SESSION_JOYSTICK)
          if(pkt.length<4)
            lg("Master: dropping joystick login msg coming from #{sender} (bad payload)")
            send_unconnected_msg(sender[3],his_port,MSG_LOGIN_NAK,'"joystick login requires at least 6 parameters"')
            return
          end
          @joystick_speed=pkt[0].to_f

          #
          # Rot center order rcvd in meters and in Moog order (heave,surge,lateral - that is, y,z,x)
          #

          @joystick_rot_center=[pkt[3].to_f*1000.0,pkt[1].to_f*1000.0,pkt[2].to_f*1000.0].pack('fff')
          pkt=pkt[4..-1]          
#          mode=Moo::MODE_LENGTH
          mode=Moo::MODE_DOF
        end

        if(pkt.length>Main_process::KEYS.length)
          lg("Master: dropping login msg coming from #{sender} (too many parameters: #{pkt})")
          send_unconnected_msg(sender[3],his_port,MSG_LOGIN_NAK,'"too many parameters"')
          return
        end

        btn_labels=(sess_inst) ? sess_inst.btn_labels : begin
                                                          a=Array::new(Main_process::KEYS.length,nil) 
                                                          pkt.each_with_index do |lbl,i|
                                                            lbl.strip!
                                                            a[i]=lbl.length>0 ? lbl : nil
                                                          end
                                                          a
                                                        end
        btn_labels.each do |l|
          m=/[^0-9a-zA-Z ]/.match(l)
          if(m)
            lg("Master: dropping login msg coming from #{sender} (bad btn lbl [#{l}])")
            send_unconnected_msg(sender[3],his_port,MSG_LOGIN_NAK,"\"bad button label (#{l})\"")
            return
          end
        end          

        @session_must_abort=false
        begin
          @session=Session::new(self,sender[3],sender[1],sess_type,mode,his_port,btn_labels,sess_inst)
        rescue => err
          msg="Error starting session (#{err})"
          lg(msg)
          send_unconnected_msg(sender[3],his_port,MSG_LOGIN_NAK,"\"#{msg}\"")
          return
        end
        sess_inst.open_log_files if(sess_inst)
        
        #
        # For immediate or sequence sessions, complain if the height is out of range
        # For joystick sessions, update rotation center
        #
        
        if(sess_type==SESSION_IMMEDIATE || sess_type==SESSION_SHORT_SEQ || sess_type==SESSION_LONG_SEQ)
          msg=nil
          
          if(@height_at_start>0)
            msg='"HEIGHT AT START MUST BE NEGATIVE"'
          else          
            pos=Moo::moog_idea_to_server_idea([0.0,0.0,@height_at_start,0.0,0.0,0.0])
            valid,lengths=@session.srvr.get_actuator_lengths(pos)
            
            msg='"BAD HEIGHT AT START"'unless(valid)
          end

          if(msg)
            @session.send_cmd(Moo::MSG_LOGIN_NAK,msg)
            @session.close
            @session=nil
            return
          end
        elsif(sess_type==SESSION_JOYSTICK)        
          @session.srvr.modify_rotcenter(@joystick_rot_center)
        end        
      when(MSG_LIST_SESSIONS)
        if(pkt.length!=2)
          lg("Master: dropping list session msg coming from #{sender} (bad payload - #{pkt})")
          return
        end
        his_port=pkt.shift.to_i
        date=pkt.shift

        dir_base="#{Moo::SESSBASE}/20#{date[0,2]}/#{date[2,2]}/#{date[4,2]}/"
        unless(File::directory?(dir_base))
          send_unconnected_msg(sender[3],his_port,MSG_LIST_SESSIONS_NAK,'"date not found"')
          return
        end

        skt=UDPSocket::new('AF_INET')
        skt.setsockopt(:SOCKET,:REUSEPORT,true)
        skt.bind(@server_local_address,@server_outport)
        skt.connect(sender[3],his_port)

        hc={}
        Dir::foreach(dir_base) do |dn|
          next if(dn[0,1]=='.')
          path=dir_base+dn+'/'
          next unless(File::directory?(path))
          h=Moo::read_info(path)
          next unless(h.is_a?(Hash))
          hc[dn]=h
        end

        hc.keys.sort .each do |dn|
          h=hc[dn]
          
          skt.send(Moo::prepare_master_pkt(0,MSG_LIST_SESSIONS_SESSION,"#{dn},\"#{h[:address]}\",\"#{h[:type]}\""),0)
        end
        
        skt.send(Moo::prepare_master_pkt(0,MSG_LIST_SESSIONS_DONE,nil),0)
        skt.close()
      when(MSG_POST_PRODUCE)
        if(pkt.length<3)
          lg("Master: dropping list session msg coming from #{sender} (bad payload)")
          return
        end

        his_port=pkt.shift.to_i

        tag=pkt.shift.strip
        unless(/^[0-9]{6}\.[0-9]{6}$/.match(tag))
          send_unconnected_msg(sender[3],his_port,MSG_POST_PRODUCE_NAK,"\"bad tag (#{tag})\"")
          return
        end
        dir_base="#{Moo::SESSBASE}/20#{tag[0,2]}/#{tag[2,2]}/#{tag[4,2]}/#{tag[7,6]}/"
        unless(File::directory?(dir_base))
          send_unconnected_msg(sender[3],his_port,MSG_POST_PRODUCE_NAK,'"tag not found"')
          return
        end

        millisec_interval=pkt.shift.to_i
        if(millisec_interval<=0 || millisec_interval>MAX_MILLISEC_INTERVAL)
          send_unconnected_msg(sender[3],his_port,MSG_POST_PRODUCE_NAK,"\"Bad millisec interval (#{millisec_interval})\"")
          return
        end

        # ret=if(pkt.length>0)
        #       Moo::send_session_report(dir_base,tag,millisec_interval,pkt)
        #     else
        #       Moo::post_produce(dir_base,tag,millisec_interval)              
        #     end
        
        ppr=Post_producer::new(dir_base,tag,millisec_interval)
        res=ppr.generate
        res=ppr.mail(pkt) if(res==true && pkt.length>0)

        if(res.is_a?(String))
          send_unconnected_msg(sender[3],his_port,MSG_POST_PRODUCE_NAK,res)
        else
          send_unconnected_msg(sender[3],his_port,MSG_POST_PRODUCE_DONE,nil)
        end
      else
        lg("Master: dropping msg #{msg_id} coming from #{sender} when not connected")
      end
    end

    def freeze_session
      return unless(@session)

      @session.set_state(Session::SSTATE_FROZEN)
    end

    def abort_session
      return unless(@session)

      lg("*** Abort requested")
      msg("MUST ABORT")

      if(@type==SESSION_SHORT_SEQ)
        @session.abort_short_seq
      elsif(@type==SESSION_LONG_SEQ)
        @session.abort_long_seq
      else      
        @session.set_state(Session::SSTATE_PARKING) if(@session.state==Session::SSTATE_ENGAGED)
      end
      @session_must_abort=true
    end

    def send_unconnected_msg(addr,outport,msg,payload)
      skt=UDPSocket::new('AF_INET')
      skt.setsockopt(:SOCKET,:REUSEPORT,true)
      skt.bind(@server_local_address,@server_outport)

      skt.connect(addr,outport)

      skt.send(Moo::prepare_master_pkt(@session ? Cg::time_diff_from_now(@session.tstart[1]) : 0,msg,payload),0)

      skt.close()
    end

    def ping()
      Udplistener::ping(@server_local_address,PING_PORT,Moo::prepare_master_pkt(0,MSG_PING,nil))
    end

    def msg(str,msgsize=Status3_info::DEF_MSGSIZE)
      lg("Message: [#{str}]")
      @ui.update_state_msg(str,msgsize) if(@ui)
    end

    def close_session_threads
      if(@audioplayer)
        @audioplayer.stop_thr
        @audioplayer=nil
        @ui.wnd.stimulinfo.fill_labels([false,nil,nil]) if(@ui)
      end
      if(@stillsplayer)
        @stillsplayer.stop
        @stillsplayer=nil
        @ui.wnd.stimulinfo.fill_labels([nil,false,nil]) if(@ui)
      end
      if(@moviesplayer)
        @moviesplayer.stop
        @moviesplayer=nil
        @ui.wnd.stimulinfo.fill_labels([nil,false,nil]) if(@ui)
      end
      if(@transdplayer)        
        @transdplayer.stop_thread
        @transdplayer=nil
        @ui.wnd.stimulinfo.fill_labels([nil,nil,false]) if(@ui)
      end
      if(@hwuser)
        @hwuser.stop
        @hwuser=nil
      end
    end
  end
end
