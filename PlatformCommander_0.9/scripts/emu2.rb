# emu2.rb

=begin

***--MANAGED--***

==========================================================================
Copyright (C) 2020 Carlo Prelz, University of Bern

carlo.prelz@humdek.unibe.ch

This file is part of Platform Commander.

Platform Commander is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Platform Commander is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
==========================================================================

Moog emulator version using the new glengine

=end

require_relative('moo')
require_relative('main_process')
require('optparse')
require('glengine')

module Moo
  class Platf_state
    STATE_OFF=0
    STATE_RESET=1
    STATE_ENGAGING=2
    STATE_ENGAGED=3
    STATE_PARKING=4
    STATE_FAULTY=5

    ENGAGING_TIME=1.5
    OFF_HEIGHT=Moo::MOOG_H_TO_SRV_H*900.0
    ENGAGED_HEIGHT=Moo::MOOG_H_TO_SRV_H*1000.0

    attr_reader(:state,:cur_mach_state,:override_height)
    
    def initialize(emu)
      @emu=emu
      @state=-1
      @cur_mach_state=Moo::MACH_STATE_IDLE
      set_state(STATE_OFF)
    end

    def set_state(state)
      if(state==@state)
        lg("Oops: already at state #{@state}")
        return
      end

      @state=state

      case @state 
      when STATE_OFF,STATE_RESET
        @override_height=OFF_HEIGHT
      when STATE_FAULTY
        @override_height=OFF_HEIGHT
      when STATE_ENGAGING
        @engaging_start=Time::now.to_f
      when STATE_PARKING
        @parking_start=Time::now.to_f
        @park_fromheight=@emu.eplatf.dof[1]
      else
        @engaging_start=nil
        @parking_start=nil
        @override_height=nil
      end

      #      lg("EMU: ORA A STATE #{@state}")
    end

    def new_cmd(cmd)
      #      lg("EMU: CMD #{cmd.to_s(16)}")
      case cmd 
      when Moo::CMD_NEW_POSITION
        set_state(STATE_RESET) if(@state==STATE_OFF)
      when Moo::CMD_RESET
        @cur_mach_state=Moo::MACH_STATE_IDLE
        set_state(STATE_RESET)
      when Moo::CMD_DOF_MODE
        return
      when Moo::CMD_LENGTH_MODE
        lg("SORRY! Emulator not supporting length mode.")
        @cur_mach_state=Moo::MACH_STATE_FAULT2
        set_state(STATE_FAULTY)
      when Moo::CMD_ENGAGE
        if(@state==STATE_RESET)
          set_state(STATE_ENGAGING)
          @cur_mach_state=Moo::MACH_STATE_STBY
        end
      when Moo::CMD_PARK
        if(@state==STATE_ENGAGED)
          set_state(STATE_PARKING)
          @cur_mach_state=Moo::MACH_STATE_PARKING
        end
      when 4294967295        
        set_state(STATE_OFF) if(@state==STATE_RESET)
      else
        lg("SORRY!!! CANNOT PROCESS CMD #{cmd.to_s(16)}")
      end
    end

    def loop
      case(@state)
      when(STATE_ENGAGING)
        offs=Time::now.to_f-@engaging_start
        if(offs>ENGAGING_TIME)
          set_state(STATE_ENGAGED)
          @cur_mach_state=Moo::MACH_STATE_ENGAGED
        else
          @override_height=OFF_HEIGHT+((ENGAGED_HEIGHT-OFF_HEIGHT)*(offs/ENGAGING_TIME))
        end
      when(STATE_PARKING)
        offs=Time::now.to_f-@parking_start
        if(offs>ENGAGING_TIME)
          set_state(STATE_RESET)
          @cur_mach_state=Moo::MACH_STATE_IDLE
        else
          @override_height=@park_fromheight-(@park_fromheight-OFF_HEIGHT)*(offs/ENGAGING_TIME)
        end
      when(STATE_FAULTY)
        @emu.send_cur_pkt
        sleep(Emu::SLEEPTIME)        
#      else
#        STDERR.print("<#{@state}>")
      end
    end          
  end
    
  class Emulated_platf
    ACTUATOR_LIMITS=[Moo::ACTU_MIN_MM+Moo::ACTU_FIXED_MM,Moo::ACTU_MAX_MM+Moo::ACTU_FIXED_MM,Moo::ACTU_PARK_MM+Moo::ACTU_FIXED_MM]    
    PLATF_ROTCENTER=[0.0,0.0,0.0].pack('fff')
    ACTUATOR_THICKNESS=[40,20]
    ACTUATOR_COLORS=[
      [[30,30,30],[80,80,80],[20,20,20]].map do |a|
        Glengine::convert_col_arr_to_num(a)
      end,
      [[20,20,20],[10,10,10],[80,80,80]].map do |a|
        Glengine::convert_col_arr_to_num(a)
      end
    ]
    ACTUATOR_SHINE=1.8
    ACTUATOR_REFINE=30
    
    PLATF_THICK=45.0

    DEF_HEIGHT1=500.0
    DEF_DISTANCE1=2500.0
    DEF_CAMPOS1=[0.0,DEF_HEIGHT1,DEF_DISTANCE1].pack('f*')
#    DEF_CAMORIENT1=[0.0,0.0,0.0,1.0].pack('f*')
    DEF_CAMORIENT1=Maths::mat_unit
#    DEF_HEIGHT2=3000.0
#    DEF_DISTANCE2=0.0
#    DEF_CAMPOS2=[0.0,DEF_HEIGHT2,DEF_DISTANCE2].pack('f*')
#    DEF_CAMORIENT2=Maths::tait_bryan_to_quat([0.0,90.0*Moo::DEG2RAD,0.0].pack('fff'))
    DEF_HEIGHT2=3000.0
    DEF_DISTANCE2=0.0
    DEF_CAMPOS2=[0.0,DEF_HEIGHT2,DEF_DISTANCE2].pack('f*')
#    DEF_CAMORIENT2=Maths::mat_to_quat(Maths::mat_rot_axis([-1.0,0.0,0.0].pack('fff'),-70.0*Moo::DEG2RAD))
    DEF_CAMORIENT2=Maths::mat_rot_axis([-1.0,0.0,0.0].pack('fff'),90.0*Moo::DEG2RAD)
#    DEF_CAMORIENT2=Maths::tait_bryan_to_quat([2.0*Moo::DEG2RAD,-181.0*Moo::DEG2RAD,0.0].pack('fff'))

    BKG_COL=[5,5,15]
    LIGHTS=[[-1500.0,2500.0,1050.0,255,255,255,9000.0],[500.0,150.0,1100.0,150,150,150,7000.0]]

    FOVY=25.0*Moo::DEG2RAD
    NEAR=1.0
    FAR=1800.0
    CAMERA1=['supervise1',Moo::CAM_DISPLAY,[0,0,400,300],true,FOVY,NEAR,FAR]
    CAMERA2=['supervise2',Moo::CAM_DISPLAY,[0,300,400,300],true,FOVY,NEAR,FAR]

    # amb, diff, spec
    BASE_COLORS=[[5,35,0],[90,220,70],[50,150,30]].map do |a|
      Glengine::convert_col_arr_to_num(a)
    end
    BASE_SHINE=2.5
    PALL_COLORS=[[35,5,0],[200,100,100],[100,0,0]].map do |a|
      Glengine::convert_col_arr_to_num(a)
    end
    # ROTCENTERREF_SIZES=[PLATF_THICK*8.0,PLATF_THICK*0.3]
    # ROTCENTERREF_COLORS=[[200,180,20],[200,180,20],[200,180,20]].map do |a|
    #   Glengine::convert_col_arr_to_num(a)
    # end

    attr_reader(:platf,:dof)
    
    def initialize(state,shd)
      @state=state
      @gle=(shd) ? Glengine::new(Glengine::T_COMPAT,BKG_COL,LIGHTS,nil) : false
      @tlower,@tupper,tstring=Platf::trias
      @platf=Platf::new(@tlower.joints.flatten.pack('f*'),@tupper.joints.flatten.pack('f*'),PLATF_ROTCENTER,*ACTUATOR_LIMITS)

      if(@gle)
        build_platform
        
        @cameras=[@gle.add_camera(*CAMERA1,false),@gle.add_camera(*CAMERA2,true)]
        @gle.orient_camera_mat(@cameras[0],DEF_CAMPOS1,DEF_CAMORIENT1,Math::sqrt(DEF_HEIGHT1*DEF_HEIGHT1+DEF_DISTANCE1*DEF_DISTANCE1))
        @gle.orient_camera_mat(@cameras[1],DEF_CAMPOS2,DEF_CAMORIENT2,Math::sqrt(DEF_HEIGHT2*DEF_HEIGHT2+DEF_DISTANCE2*DEF_DISTANCE2))

        n_nodes,n_points,n_trias,n_textures=@gle.wait_for_expose

        shaders=shd.map do |s|
          Glengine::adapt_shader_source(SHDBASE+s,LIGHTS.length,n_nodes,n_points,n_trias,n_textures)
        end
        #        puts  shaders.join("\n")
        @gle.load_shaders(*shaders)

        position_platform
      end
      set_pos([0.0,Moo::ACTU_IDLE_H*1000.0,0.0,0.0,0.0,0.0])
    end

    def set_pos(dof)
      return if(dof==@dof)
      @dof=dof

      return unless(@gle)
      
      trot=@platf.move(@dof)
#      lg("=========== SEPO #{sprintf("%8.4f %8.4f %8.4f %8.4f %8.4f %8.4f",*@dof)} -> trot #{sprintf("%8.4f %8.4f %8.4f %8.4f %8.4f %8.4f",trot[0][1],trot[1][1],trot[2][1],trot[3][1],trot[4][1],trot[5][1])}")

      @actuators.each_with_index do |aa,i|
        aa.each_with_index do |a,j|
          trote=trot[i*2+j]
          m=Maths::composed_mat([[MATTYPE_RAW,a[1][0]],
                                 [MATTYPE_RAW,trote[0]],
                                 [MATTYPE_RAW,a[1][1]]])
          @gle.position_node(a[0],m)
          @gle.position_node(a[2],Maths::composed_mat([[MATTYPE_TRASL,-trote[1]+ACTUATOR_LIMITS[2]+PLATF_THICK*2,0.0,0.0]]))
        end
      end
      @gle.position_node(@top,Maths::composed_mat([[MATTYPE_RAW,trot[6]]]))
    end

    def build_platform
      extl_lower=Math::sin(30.0*DEG2RAD)*@tlower.bord
      extl_upper=Math::sin(30.0*DEG2RAD)*@tupper.bord
      
      @base_pos=[0.0,0.0,0.0]
      @base=@gle.add_node(Glengine::N_MOOG_PLATE,nil,{name:'Lower_plate',offset:[0.0,0.0,0.0],len1:@tlower.d2+extl_lower*2,
                                                       len2:@tlower.d1+extl_lower*2,thickness:PLATF_THICK,col:BASE_COLORS+[BASE_SHINE],rot_angle:180.0*DEG2RAD})

      @actuators=3.times.to_a.map do |v|
        c1=@gle.add_node(Glengine::N_CYLINDER,@base,{name:"Act#{v}c1",offset:[0.0,0.0,0.0],radius:ACTUATOR_THICKNESS[0]/2.0,
                                                     length:ACTUATOR_LIMITS[0],refinements:ACTUATOR_REFINE,col:ACTUATOR_COLORS[0]+[BASE_SHINE]})        
        matarr1=[Maths::composed_mat([[MATTYPE_TRASL,-ACTUATOR_LIMITS[2]/2.0,0.0,0.0],[MATTYPE_ROTAXIS,0.0,0.0,1.0,90*DEG2RAD]]),
                 Maths::composed_mat([[MATTYPE_TRASL,*@tlower.joints[v][0]]])]        
        c1b=@gle.add_node(Glengine::N_CYLINDER,c1,{name:"Act#{v}c1b",offset:[0.0,0.0,0.0],radius:ACTUATOR_THICKNESS[1]/2.0,
                                                   length:ACTUATOR_LIMITS[1],refinements:ACTUATOR_REFINE,col:ACTUATOR_COLORS[1]+[BASE_SHINE]})        
        matarr1b=Maths::composed_mat([[MATTYPE_TRASL,-ACTUATOR_LIMITS[2]*0.5,0.0,0.0]])
        
        c2=@gle.add_node(Glengine::N_CYLINDER,@base,{name:"Act#{v}c2",offset:[0.0,0.0,0.0],radius:ACTUATOR_THICKNESS[0]/2.0,
                                                     length:ACTUATOR_LIMITS[0],refinements:ACTUATOR_REFINE,col:ACTUATOR_COLORS[0]+[BASE_SHINE]})        
        matarr2=[Maths::composed_mat([[MATTYPE_TRASL,-ACTUATOR_LIMITS[2]/2.0,0.0,0.0],[MATTYPE_ROTAXIS,0.0,0.0,1.0,90*DEG2RAD]]),
                 Maths::composed_mat([[MATTYPE_TRASL,*@tlower.joints[v][1]]])]
        c2b=@gle.add_node(Glengine::N_CYLINDER,c2,{name:"Act#{v}c2b",offset:[0.0,0.0,0.0],radius:ACTUATOR_THICKNESS[1]/2.0,
                                                   length:ACTUATOR_LIMITS[1],refinements:ACTUATOR_REFINE,col:ACTUATOR_COLORS[1]+[BASE_SHINE]})        
        matarr2b=Maths::composed_mat([[MATTYPE_TRASL,-ACTUATOR_LIMITS[2]*0.5,0.0,0.0]])
        
        [[c1,matarr1,c1b,matarr1b],[c2,matarr2,c2b,matarr2b]]
      end
      
      @top=@gle.add_node(Glengine::N_MOOG_PLATE,@base,{name:'Upper_plate',offset:[0.0,0.0,0.0],len1:@tupper.d2+extl_lower*2,
                                                       len2:@tlower.d1+extl_lower*2,thickness:PLATF_THICK,col:BASE_COLORS+[BASE_SHINE]})
#      @seat=@gle.add_node(Glengine::N_PLY,@top,{name:'seat',offset:[0.0,0.0,0.0],fname:PLYBASE+'chair.ply',
#                                                scale:400.0,col:[0x001010,0x2290eb,0x202030,0.9]})
      @pallina=@gle.add_node(Glengine::N_SPHERE,@top,{name:'pallina',offset:[0.0,0.0,0.0],radius:@tupper.tr[0][2]/15.0,col:PALL_COLORS+[BASE_SHINE],refinements:4})

      # rcr1=@gle.add_node(Glengine::N_CYLINDER,@top,{name:'rcr1',offset:[0.0,0.0,0.0],length:ROTCENTERREF_SIZES[0],radius:ROTCENTERREF_SIZES[1],
      #                                               col:ROTCENTERREF_COLORS+[BASE_SHINE],refinements:ACTUATOR_REFINE})
      # rcr2=@gle.add_node(Glengine::N_CYLINDER,rcr1,{name:'rcr2',offset:[0.0,0.0,0.0],length:ROTCENTERREF_SIZES[0],radius:ROTCENTERREF_SIZES[1],
      #                                               col:ROTCENTERREF_COLORS+[BASE_SHINE],refinements:ACTUATOR_REFINE})
      # rcr3=@gle.add_node(Glengine::N_CYLINDER,rcr1,{name:'rcr3',offset:[0.0,0.0,0.0],length:ROTCENTERREF_SIZES[0],radius:ROTCENTERREF_SIZES[1],
      #                                               col:ROTCENTERREF_COLORS+[BASE_SHINE],refinements:ACTUATOR_REFINE})
      # @rotcenterref=[rcr1,rcr2,rcr3]        
    end

    def position_platform
      pos=[0.0,0.0,0.0,0.0,0.0,0.0]
      @gle.movrot_node(@base,*pos)

      @actuators.each do |aa|
        aa.each do |c1,m1,c2,m2|
          @gle.position_node(c1,m1)
          @gle.position_node(c2,m2)
        end
      end      
      
      @gle.movrot_node(@top,*pos)
#      @gle.position_node(@seat,Maths::composed_mat([[MATTYPE_ROTAXIS,1.0,0.0,0.0,90*DEG2RAD],[MATTYPE_ROTAXIS,0.0,1.0,0.0,90*DEG2RAD],
#                                                    [MATTYPE_ROTAXIS,0.0,0.0,1.0,3*DEG2RAD],[MATTYPE_TRASL,0.0,350.0,0.0]]))
      @gle.movrot_node(@pallina,0.0,0.0,@tupper.tr[0][2]*0.85,0.0,0.0,0.0)
      
      # @gle.position_node(@rotcenterref[1],Maths::composed_mat([[MATTYPE_ROTAXIS,0.0,1.0,0.0,90*DEG2RAD]]))
      # @gle.position_node(@rotcenterref[2],Maths::composed_mat([[MATTYPE_ROTAXIS,0.0,0.0,1.0,90*DEG2RAD]]))
    end
    
    def cur_pkt
      Moo::compose_response(@dof,1,Moo::MODE_DOF,@state.cur_mach_state,[Moo::RESP_ESTOP_SENSE])
    end
  end
      
  class Emu        
    SLEEPTIME=1.0/60.0
    MAX_TIME_WITHOUT_PACKETS=2.0
    
    attr_reader(:eplatf,:state)
    
    def initialize(addr,inport,outport,moog_inport,moog_outport,shd,opt_k)
      @addr,@inport,@outport,@moog_inport,@moog_outport,@shd=addr,inport,outport,moog_inport,moog_outport,shd

      @last_rcvd_pkt_time=Time::now.to_f
      @last_sent_pkt_time=nil

      FileUtils::mkdir_p(Moo::LOGBASE)
      Moo::open_logfile(Moo::LOGBASE,'emu')

      @mp_pid=fork do
        Main_process::new(@addr,@outport,@inport,@addr,@addr,@moog_outport,@moog_inport,opt_k).run
      end
      at_exit do
        begin
          Process::kill('KILL',@mp_pid)
        rescue
        end
      end

      @req_send=false
      @state=Platf_state::new(self)
      @latest_cmd=-1
      @eplatf=Emulated_platf::new(@state,@shd)

      @skt=Moo::Udplistener::new(@addr,@moog_inport)

    end      

    def runme
      loop do
        if((Time::now.to_f-@last_rcvd_pkt_time)>MAX_TIME_WITHOUT_PACKETS && @state.state!=Platf_state::STATE_FAULTY)
          @cur_mach_state=Moo::MACH_STATE_FAULT2
          @state.set_state(Platf_state::STATE_FAULTY)
          send_cur_pkt
        end

        send_cur_pkt if(!@last_sent_pkt_time || (Time::now.to_f-@last_sent_pkt_time)>=Emu::SLEEPTIME)
          
        @state.loop
        pkts=@skt.poll
        if(pkts)
          pkts.each do |p|
            process_pkt(p)
          end
          
          sleep(SLEEPTIME)
        end
        begin
          pid,status=Process::waitpid2(@mp_pid,Process::WNOHANG)
          if(pid==@mp_pid)
            lg("Main process died! #{status}")
            break
          end
        rescue => err
          lg("Ouch! #{err}")
          break
        end
      end
    end

    def process_pkt(pkt)
      @last_rcvd_pkt_time=Time::now.to_f

      if(@opkt!=pkt[2])
        @opkt=pkt[2]
        a=@opkt.unpack('Ng*')
        dof=Moo::moog_idea_to_server_idea(a[1,6])
        dof[1]=@state.override_height if(@state.override_height)
#        lg("PKT [#{a[0].to_s(16)}]/|#{sprintf("%8.4f %8.4f %8.4f %8.4f %8.4f %8.4f %8.4f",*(a[1,7]))}| DOFFO #{sprintf("%8.4f %8.4f %8.4f %8.4f %8.4f %8.4f",*dof)}")
        @eplatf.set_pos(dof) 
        process_cmd(a[0])
      end
    end

    def send_cur_pkt
      pkt=@eplatf.cur_pkt
      @skt.sendto(@addr,@moog_outport,pkt) unless(@state.state==Platf_state::STATE_OFF)
      @last_sent_pkt_time=Time::now.to_f
#      STDERR.print('S')
    end

    def process_cmd(cmd)
      if(cmd!=@latest_cmd)
        @latest_cmd=cmd
        @state.new_cmd(@latest_cmd)
      end
    end
  end
end

if(__FILE__==$0)
  opt_g=false
  opt_k=false
  OptionParser.new do |opts|
    opts.banner="Usage: #{$0} [-G -K]"

    opts.on('-G','--no-3d-graphics','Disable three--d moog emulation window') do 
      opt_g=true
    end
    opts.on('-K','--use-keyboard','Ask main process to use keyboard\'s numeric pad for buttons') do 
      opt_k=true
    end
  end.parse!
  shd=opt_g ? false : ['ve1','fr1']
  
  Moo::Emu::new(Moo::SERVER_LOCAL_ADDRESS,Moo::SERVER_OUTPORT,Moo::SERVER_INPORT,11431,11432,shd,opt_k).runme
end

