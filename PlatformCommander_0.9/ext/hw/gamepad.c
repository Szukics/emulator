/* gamepad.c */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================
 *
 * Handling of a playstation-like USB gamepad
 */

#include "hw.h"
#include <pthread.h>
#include <poll.h>

#define POLL_TIMEOUT 350

extern VALUE mod_hw,cls_hw_gamepad;

static void free_hw_gamepad(void *p);
static void stop_thread(hw_gamepad_stc *s);
static void *gp_thr(void *arg);

static size_t hw_gamepad_memsize(const void *p)
{
  return sizeof(hw_gamepad_stc);
}

const rb_data_type_t hw_gamepad_dtype={"hw_gamepad",
				       {0,free_hw_gamepad,hw_gamepad_memsize,},
				       0,0,
				       RUBY_TYPED_FREE_IMMEDIATELY,};

VALUE new_hw_gamepad(VALUE self,VALUE v_devname)
{
  hw_gamepad_stc *s;
  VALUE sdata=TypedData_Make_Struct(cls_hw_gamepad,hw_gamepad_stc,&hw_gamepad_dtype,s),v;
  int i;

  VALUE mod_moo=rb_define_module("Moo");

  bzero(s,sizeof(hw_gamepad_stc));

  s->devname=strdup(RSTRING_PTR(v_devname));
  
  VALUE v_idname=rb_funcall(mod_moo,rb_intern("find_input_device"),1,v_devname);

  if(v_idname==Qfalse)
    rb_raise(rb_eArgError,"%s: Device %s not found",__func__,s->devname);
  s->idname=strdup(RSTRING_PTR(v_idname));
  s->unit=open(s->idname,O_RDONLY);
  if(s->unit<0)
    rb_raise(rb_eArgError,"%s: Device %s (%s) could not be opened (%s)",__func__,
	     s->devname,s->idname,strerror(errno));
  
/*
 * The thread for managing the gamepad
 */

  int sts;
  
  sts=pthread_mutex_init(&s->mtx,NULL);
  if(sts)
    rb_raise(rb_eArgError,"%s: error #%d creating mutex (%s)",__func__,sts,strerror(errno));
  
  s->leave_thread=0; 

  sts=pthread_create(&s->thr,NULL,gp_thr,s);
  if(sts)
    rb_raise(rb_eArgError,"%s: error #%d starting thread (%s)",__func__,sts,strerror(errno));

  return sdata;  
}

static void free_hw_gamepad(void *p)
{
  hw_gamepad_stc *s=(hw_gamepad_stc *)p;
  int i;

  stop_thread(s);

  free(s->devname);
  free(s->idname);

  free(p);
}

VALUE hw_gamepad_poll(VALUE self)
{
  hw_gamepad_stc *s;  
  TypedData_Get_Struct(self,hw_gamepad_stc,&hw_gamepad_dtype,s);

  if(!s->events)
    return Qfalse;

  gp_event_stc *evq,*evp1,*evp2;

  pthread_mutex_lock(&s->mtx);
  evq=s->events;
  s->events=NULL;
  pthread_mutex_unlock(&s->mtx);

  VALUE to_ret=rb_ary_new(),v;

  for(evp1=evq;evp1;)
  {
    evp2=evp1;
    evp1=evp2->next;

    v=rb_ary_new_capa(4);
      
    rb_ary_store(v,0,INT2FIX(evp2->ie.type));
    rb_ary_store(v,1,INT2FIX(evp2->ie.code));
    rb_ary_store(v,2,INT2FIX(evp2->ie.value));
    rb_ary_store(v,3,rb_str_new((char *)(&evp2->reftime),sizeof(struct timespec)));

    rb_ary_push(to_ret,v);
    
    free(evp2);
  }    

  return to_ret;
}

VALUE hw_gamepad_stop_thread(VALUE self)
{
  hw_gamepad_stc *s;  
  TypedData_Get_Struct(self,hw_gamepad_stc,&hw_gamepad_dtype,s);

  stop_thread(s);

  return self;
}

static void stop_thread(hw_gamepad_stc *s)
{
  if(s->thr && !s->leave_thread)
  {
    pthread_t thr=s->thr;
    s->thr=0;
    s->leave_thread=1;
    pthread_join(s->thr,NULL);
  }
}

static void *gp_thr(void *arg)
{
  hw_gamepad_stc *s=(hw_gamepad_stc *)arg;
  struct pollfd pfd={s->unit,POLLIN,0};
  struct input_event ie;
  int len,res;
  gp_event_stc *ev=NULL,*evp;

  while(!s->leave_thread)
  {
    pfd.events=POLLIN;
    res=poll(&pfd,1,POLL_TIMEOUT);
    
    if(res<0)
    {
      lg("%s: Error in polling (%s)",__func__,strerror(errno));
      break;
    }

    if(res!=0)
    {
      len=read(s->unit,&ie,sizeof(struct input_event));
      
      if(len!=sizeof(struct input_event))
      {
	lg("%s: Error in reading (%s). Read %d bytes instead of %d",__func__,strerror(errno),len,sizeof(struct input_event));
	continue;
      }
      
      if(ie.type==EV_KEY || ie.type==EV_ABS)
      {
	ev=malloc(sizeof(gp_event_stc));
	ev->ie=ie;
	clock_gettime(CLOCK_MONOTONIC,&ev->reftime);
	ev->next=NULL;
	pthread_mutex_lock(&s->mtx);
	if(!s->events)
	  s->events=ev;
	else
	{
	  for(evp=s->events;evp->next;evp=evp->next)
	    ;
	  evp->next=ev;
	}
	pthread_mutex_unlock(&s->mtx);
      }
      
//      lg("EVE type %x code %d(0x%x) value %d",ie.type,ie.code,ie.code,ie.value);
    } 
  }

  close(s->unit);

  lg("Gamepad thr out.");
    
  return NULL;
}
	
 
