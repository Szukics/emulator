/* dist_init.c */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================
 *
 * Generic hardware extensions - definitions and declarations
 */

#include "hw.h"

VALUE mod_hw,cls_hw_user,cls_hw_gamepad,cls_hw_spnav,cls_hw_xsense;

extern VALUE new_hw_user(VALUE self,VALUE v_dev_array,VALUE v_log_dir);
extern VALUE hw_user_add_monitored_device(VALUE self,VALUE v_type,VALUE v_dev);
extern VALUE hw_user_poll(VALUE self,VALUE v_objrec,VALUE v_func);
extern VALUE hw_user_stop_thread(VALUE self);

extern VALUE new_hw_gamepad(VALUE self,VALUE v_devname);
extern VALUE hw_gamepad_poll(VALUE self);
extern VALUE hw_gamepad_stop_thread(VALUE self);

extern VALUE new_hw_spnav(VALUE self,VALUE v_devname);
extern VALUE hw_spnav_poll(VALUE self);
extern VALUE hw_spnav_stop_thread(VALUE self);

extern VALUE new_hw_xsense(VALUE self,VALUE v_devname);
extern VALUE hw_xsense_send_msg(VALUE self,VALUE v_msg_id,VALUE v_payload);
extern VALUE hw_xsense_poll(VALUE self);
extern VALUE hw_xsense_stop_thread(VALUE self);

void Init_hw(void)
{
  VALUE v;
  char **ptr;
  
  mod_hw=rb_define_module("Hw");  
  rb_define_const(mod_hw,"LOG_NAME",rb_str_new_cstr(HWUSER_LOG_NAME));
  rb_define_const(mod_hw,"DEV_GAMEPAD",INT2FIX(HW_DEV_GAMEPAD));
  rb_define_const(mod_hw,"DEV_SPNAV",INT2FIX(HW_DEV_SPNAV));
  rb_define_const(mod_hw,"DEV_XSENSE",INT2FIX(HW_DEV_XSENSE));

  cls_hw_user=rb_define_class_under(mod_hw,"Hw_user",rb_cObject);
  rb_define_singleton_method(cls_hw_user,"new",new_hw_user,2);
  rb_define_const(cls_hw_user,"LOG_NAME",rb_str_new_cstr(HWUSER_LOG_NAME));
  rb_define_method(cls_hw_user,"add_monitored_device",hw_user_add_monitored_device,2);
  rb_define_method(cls_hw_user,"poll",hw_user_poll,2);
  rb_define_method(cls_hw_user,"stop_thread",hw_user_stop_thread,0);
  
  cls_hw_gamepad=rb_define_class_under(mod_hw,"Gamepad",rb_cObject);
  rb_define_const(cls_hw_gamepad,"B_FL1",INT2FIX(HW_GP_B_FL1));
  rb_define_const(cls_hw_gamepad,"B_FL2",INT2FIX(HW_GP_B_FL2));
  rb_define_const(cls_hw_gamepad,"B_FR1",INT2FIX(HW_GP_B_FR1));
  rb_define_const(cls_hw_gamepad,"B_FR2",INT2FIX(HW_GP_B_FR2));
  rb_define_const(cls_hw_gamepad,"B_SELECT",INT2FIX(HW_GP_B_SELECT));
  rb_define_const(cls_hw_gamepad,"B_START",INT2FIX(HW_GP_B_START));
  rb_define_const(cls_hw_gamepad,"B_TRIANGLE",INT2FIX(HW_GP_B_TRIANGLE));
  rb_define_const(cls_hw_gamepad,"B_SQUARE",INT2FIX(HW_GP_B_SQUARE));
  rb_define_const(cls_hw_gamepad,"B_CIRCLE",INT2FIX(HW_GP_B_CIRCLE));
  rb_define_const(cls_hw_gamepad,"B_CROSS",INT2FIX(HW_GP_B_CROSS));
  rb_define_singleton_method(cls_hw_gamepad,"new",new_hw_gamepad,1);
  rb_define_method(cls_hw_gamepad,"poll",hw_gamepad_poll,0);
  rb_define_method(cls_hw_gamepad,"stop_thread",hw_gamepad_stop_thread,0);

  cls_hw_spnav=rb_define_class_under(mod_hw,"Spnav",rb_cObject);
  rb_define_const(cls_hw_spnav,"B_LEFT",INT2FIX(HW_SP_B_LEFT));
  rb_define_const(cls_hw_spnav,"B_RIGHT",INT2FIX(HW_SP_B_RIGHT));
  rb_define_singleton_method(cls_hw_spnav,"new",new_hw_spnav,1);
  rb_define_method(cls_hw_spnav,"poll",hw_spnav_poll,0);
  rb_define_method(cls_hw_spnav,"stop_thread",hw_spnav_stop_thread,0);

  cls_hw_xsense=rb_define_class_under(mod_hw,"Xsense",rb_cObject);
  rb_define_singleton_method(cls_hw_xsense,"new",new_hw_xsense,1);
  rb_define_method(cls_hw_xsense,"send_msg",hw_xsense_send_msg,2);
  rb_define_method(cls_hw_xsense,"poll",hw_xsense_poll,0);
  rb_define_method(cls_hw_xsense,"stop_thread",hw_xsense_stop_thread,0);
}

