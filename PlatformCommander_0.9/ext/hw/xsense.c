/* xsense.c */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================
 *
 * management of a Xsens MTx-series motion device
 */

#include "hw.h"
#include <pthread.h>
#include <poll.h>

#define XSENSE_SPEED B115200
#define POLL_TIMEOUT 350
#define BFR_SIZE 0x100

extern VALUE mod_hw,cls_hw_xsense;

static void free_hw_xsense(void *p);
static void stop_thread(hw_xsense_stc *s);
static void *thr(void *arg);
static int parse_msg(const uint8_t *bfr,const int len,int *consumed,uint8_t *msg_id,uint8_t **retbfr,int *mlen);

static size_t hw_xsense_memsize(const void *p)
{
  return sizeof(hw_xsense_stc);
}

const rb_data_type_t hw_xsense_dtype={"hw_xsense",
				      {0,free_hw_xsense,hw_xsense_memsize,},
				      0,0,
				      RUBY_TYPED_FREE_IMMEDIATELY,};

VALUE new_hw_xsense(VALUE self,VALUE v_devname)
{
  hw_xsense_stc *s;
  VALUE sdata=TypedData_Make_Struct(cls_hw_xsense,hw_xsense_stc,&hw_xsense_dtype,s),v;
  int i;

  bzero(s,sizeof(hw_xsense_stc));
  s->devname=strdup(RSTRING_PTR(v_devname));
  
  VALUE mod_moo=rb_define_module("Moo"),v_idname=rb_funcall(mod_moo,rb_intern("find_serial_device"),1,v_devname);
  
  if(v_idname==Qfalse)
    rb_raise(rb_eArgError,"%s: Serial device %s not found",__func__,s->devname);
  s->idname=strdup(RSTRING_PTR(v_idname));
  
  s->unit=open(s->idname,O_RDWR|O_NOCTTY|O_NONBLOCK);  
  if(s->unit<0)
    rb_raise(rb_eArgError,"%s: Device %s could not be opened (%s)",__func__,s->idname,strerror(errno));
  fcntl(s->unit,F_SETFL,O_RDWR);
  if(tcgetattr(s->unit,&s->orig_tios)<0)
    rb_raise(rb_eArgError,"%s: Device %s: error in tcgetattr (%s)",__func__,s->idname,strerror(errno));
  memcpy(&s->orig_tios,&s->tios,sizeof(struct termios));  

  s->tios.c_cflag&=(~(CLOCAL|CREAD|CSIZE|CSTOPB|CRTSCTS));
  s->tios.c_cflag|=((CLOCAL|CREAD|CS8|CSTOPB)&(~PARENB));
  s->tios.c_iflag|=IGNPAR;
  s->tios.c_iflag&=~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL|IXON|IXOFF|IXANY|INPCK|ECHO|ECHONL|ICANON|ISIG|IEXTEN);
  s->tios.c_oflag&=~OPOST;
  s->tios.c_cc[VMIN]=1; // minimum number of chars to read in noncanonical (raw mode)
  s->tios.c_cc[VTIME]=5; // time in deciseconds to wait for data in noncanonical mode (raw mode)

  cfsetspeed(&s->tios,XSENSE_SPEED);

  if(tcsetattr(s->unit,TCSAFLUSH,&s->tios))
    rb_raise(rb_eArgError,"%s: Device %s: error in tcsetattr (%s)",__func__,s->devname,strerror(errno));
  
/*
 * The thread for managing the xsense
 */

  int sts;
  
  sts=pthread_mutex_init(&s->mtx,NULL);
  if(sts)
    rb_raise(rb_eArgError,"%s: error #%d creating mutex (%s)",__func__,sts,strerror(errno));
  
  s->leave_thread=0; 

  sts=pthread_create(&s->thr,NULL,thr,s);
  if(sts)
    rb_raise(rb_eArgError,"%s: error #%d starting thread (%s)",__func__,sts,strerror(errno));

  return sdata;  
}

static void free_hw_xsense(void *p)
{
  hw_xsense_stc *s=(hw_xsense_stc *)p;
  int i;

  stop_thread(s);

  if(tcsetattr(s->unit,TCSAFLUSH,&s->orig_tios))
    rb_raise(rb_eArgError,"%s: Device %s: error in tcsetattr/close (%s)",__func__,s->devname,strerror(errno));
  close(s->unit);

  close(s->unit);
  free(s->devname);
  free(s->idname);

  free(p);
}

VALUE hw_xsense_send_msg(VALUE self,VALUE v_msg_id,VALUE v_payload)
{
  hw_xsense_stc *s;  
  Data_Get_Struct(self,hw_xsense_stc,s);

  int msg_id=FIX2INT(v_msg_id),msg_len=(v_payload==Qnil) ? 0 : RSTRING_LEN(v_payload),sum,i;
  uint8_t bfr[msg_len+5];

  if(msg_len<0 || msg_len>0xff)
    rb_raise(rb_eArgError,"%s: Extended length still not supported",__func__);

  bfr[0]=0xfa;
  bfr[1]=0xff;
  bfr[2]=msg_id;
  bfr[3]=msg_len;
  if(msg_len>0)
    memcpy(bfr+4,RSTRING_PTR(v_payload),msg_len);

  for(sum=0,i=1;i<msg_len+4;i++)
    sum+=bfr[i];
  bfr[4+msg_len]=0x100-(sum&0xff);

  int res=write(s->unit,bfr,5+msg_len);
  if(res<0)
    rb_raise(rb_eArgError,"%s: Error writing! (%s)",__func__,strerror(errno));
  tcdrain(s->unit);

  return self;
}

VALUE hw_xsense_poll(VALUE self)
{
  hw_xsense_stc *s;  
  TypedData_Get_Struct(self,hw_xsense_stc,&hw_xsense_dtype,s);

  if(!s->events)
    return Qfalse;

  xsense_event_stc *evq,*evp1,*evp2;

  pthread_mutex_lock(&s->mtx);
  evq=s->events;
  s->events=NULL;
  pthread_mutex_unlock(&s->mtx);

  VALUE to_ret=rb_ary_new(),v;

  for(evp1=evq;evp1;)
  {
    evp2=evp1;
    evp1=evp2->next;

    v=rb_ary_new_capa(3);
      
    rb_ary_store(v,0,INT2FIX(evp2->d.msgtype));
    rb_ary_store(v,1,rb_str_new((char *)evp2->d.msg,evp2->d.msglen));
    rb_ary_store(v,2,rb_str_new((char *)&evp2->reftime,sizeof(struct timespec)));    

    rb_ary_push(to_ret,v);
    
    free(evp2->d.msg);
    free(evp2);
  }    

  return to_ret;
}

VALUE hw_xsense_stop_thread(VALUE self)
{
  hw_xsense_stc *s;  
  TypedData_Get_Struct(self,hw_xsense_stc,&hw_xsense_dtype,s);

  stop_thread(s);

  return self;
}

static void stop_thread(hw_xsense_stc *s)
{
  if(s->thr && !s->leave_thread)
  {
    s->leave_thread=1;
    pthread_join(s->thr,NULL);
    s->thr=(pthread_t)0;
  }
}

static void *thr(void *arg)
{
  hw_xsense_stc *s=(hw_xsense_stc *)arg;
  struct pollfd pfd={s->unit,POLLIN,0};
  struct input_event ie;
  int len,res,i,j,cur_bfr_len,stale_len=0,parsed_len=0,mlen=0,consumed;
  uint8_t cond,bfr[BFR_SIZE],msg_id=0xff,*msg_ptr=NULL;
  xsense_event_stc *ev=NULL,*evp;

  while(!s->leave_thread)
  {
    pfd.events=POLLIN;
    res=poll(&pfd,1,POLL_TIMEOUT);
    
    if(res<0)
    {
      lg("%s: Error in polling (%s)",__func__,strerror(errno));
      break;
    }

    if(res!=0)
    {
      i=read(s->unit,bfr+stale_len,BFR_SIZE-stale_len);
      if(i<=0)
	break;

      parsed_len=0;
      cur_bfr_len=stale_len+i;

      while(1)
      {
	j=parse_msg(bfr+parsed_len,cur_bfr_len-parsed_len,&consumed,&msg_id,&msg_ptr,&mlen);
	if(j)
	{
//	  lg("-->> recv %x (%d bytes)",msg_id,mlen);
	  
	  ev=malloc(sizeof(xsense_event_stc));
	  ev->d.msgtype=msg_id;
	  ev->d.msglen=mlen;
	  ev->d.msg=msg_ptr;
	  clock_gettime(CLOCK_MONOTONIC,&ev->reftime);
	  ev->next=NULL;

	  pthread_mutex_lock(&s->mtx);
	  if(!s->events)
	    s->events=ev;
	  else
	  {
	    for(evp=s->events;evp->next;evp=evp->next)
	      ;
	    evp->next=ev;
	  }
	  pthread_mutex_unlock(&s->mtx);
	}
	
	parsed_len+=consumed;

	if(!j)
	  break;
      }
      stale_len=cur_bfr_len-parsed_len;
      memmove(bfr,bfr+parsed_len,stale_len);
    } 
  }

  lg("Xsense thr out.");
    
  return NULL;
}
	
static int parse_msg(const uint8_t *bfr,const int len,int *consumed,uint8_t *msg_id,uint8_t **retbfr,int *mlen)
{
  int i,j,begpos,sum;
  uint8_t *found=NULL;

  for(i=0;i<len-2;i++)
  {
    if(bfr[i]!=0xfa || bfr[i+1]!=0xff)
      continue;
    *(msg_id)=bfr[i+2];
    *(mlen)=bfr[i+3];
    
    if((*mlen)==0xff)
    {
      lg("%s: ERROR! Extended len not handled (msg %x)",__func__,*msg_id);
      hexprint((char *)bfr,len);
      abort();
    }

    if((i+4+(*mlen))>=len)
    {
      (*consumed)=i-1;      
      return 0;
    }    

    for(sum=0,j=1;j<(*mlen)+5;j++)
    {
      sum+=bfr[i+j];
//      lg("Aft %d (%x) -> %x",j,bfr[i+j],sum);
    }

    if((sum&0xff)!=0)
    {
      lg("%s: ERROR! Bad checksum %x (from %d)",__func__,sum,i);
      hexprint((char *)bfr,len);
      continue;
    }

    (*retbfr)=malloc(*mlen);
    memcpy(*retbfr,bfr+i+4,*mlen);
    (*consumed)=i+5+(*mlen);
    
    return 1;
  }

//  lg("VUOTA %d/%d",i,len);

  (*consumed)=len-2;  
  return 0;
}

