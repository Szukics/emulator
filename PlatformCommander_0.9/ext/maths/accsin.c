/* accsin.c */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================
 *
 * Sinusoid acceleration algorithm
 */

#include "maths_include.h"

extern VALUE cls_maths_accsin;

#define MAX_TIME 60.0
#define MIN_TIME_DIFF 0.001

#define MAX_MAX_ACCEL 5.0
#define MIN_MAX_ACCEL_DIFF 0.00001

#define PI_SQ (M_PI*2.0)

static void free_maths_accsin(void *p);
static char *get_dist(const float time,const float max_accel,float *distance);
static char *get_time(const float distance,const float max_accel,float *res_time);
static char *get_max_accel(const float distance,const float time,float *res_max_accel);

#define ST 0.001

VALUE new_maths_accsin(VALUE self)
{
  maths_accsin_stc *s;
  VALUE sdata=Data_Make_Struct(cls_maths_accsin,maths_accsin_stc,NULL,free_maths_accsin,s);

  bzero(s,sizeof(maths_accsin_stc));
  
  return sdata;
}

static void free_maths_accsin(void *p)
{
//  maths_accsin_stc *s=(maths_accsin_stc *)p;

  free(p);
}

VALUE maths_accsin_load(VALUE self,VALUE v_dof_from,VALUE v_dof_vect,VALUE v_type,VALUE v_f1,VALUE v_f2,VALUE v_f3)
{
  maths_accsin_stc *s;  
  Data_Get_Struct(self,maths_accsin_stc,s);

  s->type=FIX2INT(v_type);

  hv v1;

  memcpy(s->l.from,RSTRING_PTR(v_dof_from),sizeof(hv));
  memcpy(s->a.from,RSTRING_PTR(v_dof_from)+sizeof(hv),sizeof(hv));
  memcpy(v1,RSTRING_PTR(v_dof_vect),sizeof(hv));
  vect_normalize(v1,s->l.dir);
  memcpy(v1,RSTRING_PTR(v_dof_vect)+sizeof(hv),sizeof(hv));
  vect_normalize(v1,s->a.dir);

  double f1=NUM2DBL(v_f1),f2=NUM2DBL(v_f2),f3=NUM2DBL(v_f3);  
  VALUE to_ret=rb_ary_new_capa(2);
  char *res1=NULL,*res2=NULL;

  switch(s->type)
  {
  case ACCSIN_BOTH_LIN_ANG_ACC:
    s->l.acc=f1;
    s->l.dist=f2;
    s->a.acc=f3;

    res1=get_time(s->l.dist,s->l.acc,&s->time);
    res2=get_dist(s->time,s->a.acc,&s->a.dist);

    rb_ary_store(to_ret,0,DBL2NUM(s->time));
    rb_ary_store(to_ret,1,DBL2NUM(s->a.dist));    
    
    break;
  case ACCSIN_BOTH_LIN_ANG_DISP:
    s->l.acc=f1;
    s->l.dist=f2;
    s->a.dist=f3;

    res1=get_time(s->l.dist,s->l.acc,&s->time);
    res2=get_max_accel(s->a.dist,s->time,&s->a.acc);

    rb_ary_store(to_ret,0,DBL2NUM(s->time));
    rb_ary_store(to_ret,1,DBL2NUM(s->a.acc));    
    
    break;
  case ACCSIN_BOTH_ANG_LIN_ACC:
    s->a.acc=f1;
    s->a.dist=f2;
    s->l.acc=f3;

    res1=get_time(s->a.dist,s->a.acc,&s->time);
    res2=get_dist(s->time,s->l.acc,&s->l.dist);

    rb_ary_store(to_ret,0,DBL2NUM(s->time));
    rb_ary_store(to_ret,1,DBL2NUM(s->l.dist));    

    break;
  case ACCSIN_BOTH_ANG_LIN_DISP:
    s->a.acc=f1;
    s->a.dist=f2;
    s->l.dist=f3;

    res1=get_time(s->a.dist,s->a.acc,&s->time);
    res2=get_max_accel(s->l.dist,s->time,&s->l.acc);

    rb_ary_store(to_ret,0,DBL2NUM(s->time));
    rb_ary_store(to_ret,1,DBL2NUM(s->l.acc));    
    
    break;
  case ACCSIN_LIN_ACC_ANG_ACC_TIME:
    s->l.acc=f1;
    s->a.acc=f2;
    s->time=f3;

    res1=get_dist(s->time,s->l.acc,&s->l.dist);
    res2=get_dist(s->time,s->a.acc,&s->a.dist);

    rb_ary_store(to_ret,0,DBL2NUM(s->l.dist));
    rb_ary_store(to_ret,1,DBL2NUM(s->a.dist));    

    break;
  case ACCSIN_LIN_ACC_ANG_DISP_TIME:
    s->l.acc=f1;
    s->a.dist=f2;
    s->time=f3;

    res1=get_dist(s->time,s->l.acc,&s->l.dist);
    res2=get_max_accel(s->a.dist,s->time,&s->a.acc);

    rb_ary_store(to_ret,0,DBL2NUM(s->l.dist));
    rb_ary_store(to_ret,1,DBL2NUM(s->a.acc));    
    
    break;
  case ACCSIN_LIN_DISP_ANG_ACC_TIME:
    s->l.dist=f1;
    s->a.acc=f2;
    s->time=f3;

    res1=get_max_accel(s->l.dist,s->time,&s->l.acc);
    res2=get_dist(s->time,s->a.acc,&s->a.dist);

    rb_ary_store(to_ret,0,DBL2NUM(s->l.acc));
    rb_ary_store(to_ret,1,DBL2NUM(s->a.dist));    
    
    break;
  case ACCSIN_LIN_DISP_ANG_DISP_TIME:
    s->l.dist=f1;
    s->a.dist=f2;
    s->time=f3;

    res1=get_max_accel(s->l.dist,s->time,&s->l.acc);
    res2=get_max_accel(s->a.dist,s->time,&s->a.acc);

    rb_ary_store(to_ret,0,DBL2NUM(s->l.acc));
    rb_ary_store(to_ret,1,DBL2NUM(s->a.acc));    
    
    break;
  }

  if(res1 || res2)
  {
    rb_ary_store(to_ret,0,res1 ? rb_str_new_cstr(res1) : Qnil);
    rb_ary_store(to_ret,1,res2 ? rb_str_new_cstr(res2) : Qnil);
    return to_ret;
  }

  vect_mult_const(s->l.dir,s->l.dist,s->l.diff);
  vect_add(s->l.from,s->l.diff,s->l.to);
  vect_mult_const(s->a.dir,s->a.dist,s->a.diff);
  vect_add(s->a.from,s->a.diff,s->a.to);
  lg("quindi: L con dir %10.3f, %10.3f, %10.3f e dist %10.3f va da %10.3f, %10.3f, %10.3f a %10.3f, %10.3f, %10.3f",
     s->l.dir[0],s->l.dir[1],s->l.dir[2],s->l.dist,s->l.from[0],s->l.from[1],s->l.from[2],s->l.to[0],s->l.to[1],s->l.to[2]);
  lg("quindi: A con dir %10.3f, %10.3f, %10.3f e dist %10.3f va da %10.3f, %10.3f, %10.3f a %10.3f, %10.3f, %10.3f",
     s->a.dir[0],s->a.dir[1],s->a.dir[2],s->a.dist,s->a.from[0],s->a.from[1],s->a.from[2],s->a.to[0],s->a.to[1],s->a.to[2]);

  s->loaded_flg=1;

  return to_ret;
}

VALUE maths_accsin_pos_at(VALUE self,VALUE v_at)
{
  maths_accsin_stc *s;  
  Data_Get_Struct(self,maths_accsin_stc,s);

  if(!s->loaded_flg)
  {
    lg("%s: Please first load parameters!",__func__);
    return Qfalse;
  }

  float pos_at=NUM2DBL(v_at);
  hv l,a;

  accsin_pos_at(s,pos_at,l,a);
  
  VALUE to_ret=rb_ary_new_capa(2);

  rb_ary_store(to_ret,0,rb_str_new((char *)l,sizeof(hv)));
  rb_ary_store(to_ret,1,rb_str_new((char *)a,sizeof(hv)));
  
  return to_ret;
}

VALUE maths_accsin_end_pos_time(VALUE self)
{
  maths_accsin_stc *s;  
  Data_Get_Struct(self,maths_accsin_stc,s);

  VALUE to_ret=rb_ary_new_capa(3);

  rb_ary_store(to_ret,0,rb_str_new((char *)s->l.to,sizeof(hv)));
  rb_ary_store(to_ret,1,rb_str_new((char *)s->a.to,sizeof(hv)));
  rb_ary_store(to_ret,2,DBL2NUM(s->time));

  return to_ret;
}

void sinacc_speed_pos(const float max_acc,const float period_len,const float cur_time,float *speed,float *pos)
{
  float f1=(max_acc*period_len)/PI_SQ,f2=PI_SQ*cur_time/period_len;

  (*speed)=f1*(1.0-cosf(f2));
  (*pos)=f1*(cur_time-period_len/PI_SQ*sinf(f2));
}

void accsin_pos_at(maths_accsin_stc *s,const float at,hv l,hv a)
{  
  float res_l,res_a,f;

  sinacc_speed_pos(s->l.acc,s->time,at*s->time,&f,&res_l);
  sinacc_speed_pos(s->a.acc,s->time,at*s->time,&f,&res_a);

  res_l/=s->l.dist;
  res_a/=s->a.dist;
  
  int i;
  
  for(i=0;i<3;i++)
  {
    l[i]=s->l.from[i]+s->l.diff[i]*res_l;
    a[i]=s->a.from[i]+s->a.diff[i]*res_a;
  }
}

static char *get_dist(const float time,const float max_accel,float *distance)
{
  float speed,pos;

  sinacc_speed_pos(max_accel,time,time,&speed,&pos);

  (*distance)=pos;
  
  return NULL;  
}

static char *get_time(const float distance,const float max_accel,float *res_time)
{
  float speed,pos,time,cur_times[2]={0.0,MAX_TIME};
  static char bfr[256];

  sinacc_speed_pos(max_accel,MAX_TIME,MAX_TIME,&speed,&pos);

  if(pos<distance)
  {
    sprintf(bfr,"Distance %f cannot be covered with max acc %f (max possible %f)",distance,max_accel,pos);
    return bfr;
  }
  
  while((cur_times[1]-cur_times[0])>MIN_TIME_DIFF)
  {
    time=cur_times[0]+(cur_times[1]-cur_times[0])*0.5;

    sinacc_speed_pos(max_accel,time,time,&speed,&pos);

    if(pos>distance)
      cur_times[1]=time;
    else
      cur_times[0]=time;
//    lg("-> %f to %f (%f)",cur_times[0],cur_times[1],cur_times[1]-cur_times[0]);    
  }

  (*res_time)=time;
  return NULL;
}

static char *get_max_accel(const float distance,const float time,float *res_max_accel)
{
  float speed,pos,max_accel,cur_max_accels[2]={0.0,MAX_MAX_ACCEL};
  static char bfr[256];

  sinacc_speed_pos(MAX_MAX_ACCEL,time,time,&speed,&pos);

  if(pos<distance)
  {
    sprintf(bfr,"Distance %f cannot be covered in time %f (max possible %f)",distance,time,pos);
    return bfr;
  }
  
  while((cur_max_accels[1]-cur_max_accels[0])>MIN_MAX_ACCEL_DIFF)
  {
    max_accel=cur_max_accels[0]+(cur_max_accels[1]-cur_max_accels[0])*0.5;

    sinacc_speed_pos(max_accel,time,time,&speed,&pos);

    if(pos>distance)
      cur_max_accels[1]=max_accel;
    else
      cur_max_accels[0]=max_accel;
//    lg("-> %f to %f (%f)",cur_max_accels[0],cur_max_accels[1],cur_max_accels[1]-cur_max_accels[0]);    
  }  
  
  (*res_max_accel)=max_accel;
  return NULL;
}
