/* init.c */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================
 *
 * Mathematical extensions - declarations & definitions
 */

/* init.c */

#include "maths_include.h"

VALUE mod_maths,cls_maths_smoother,cls_maths_casteljau,cls_maths_accsin,cls_maths_dho;

static VALUE maths_vect_neg(VALUE self,VALUE v_v);
static VALUE maths_vect_add(VALUE self,VALUE v_v1,VALUE v_v2);
static VALUE maths_vect_sub(VALUE self,VALUE v_v1,VALUE v_v2);
static VALUE maths_vect_mult_const(VALUE self,VALUE v_v1,VALUE v_v2);
static VALUE maths_vect_normalize(VALUE self,VALUE v_v);
static VALUE maths_vect_unit_prop(VALUE self,VALUE v_v);
static VALUE maths_vect_center(VALUE self,VALUE v_v1,VALUE v_v2);
static VALUE maths_vect_dot_prod(VALUE self,VALUE v_v1,VALUE v_v2);
static VALUE maths_vect_three_points_normal(VALUE self,VALUE v_v1,VALUE v_v2,VALUE v_v3);
static VALUE maths_composed_mat(VALUE self,VALUE v_arr);
static VALUE maths_apply_traslrot(VALUE self,VALUE v_base,VALUE v_traslrot);
static VALUE maths_vector_over_another(VALUE self,VALUE v_v1,VALUE v_v2);
static VALUE maths_mat_print(VALUE self,VALUE v_tag,VALUE v_m);
static VALUE maths_mat_unit(VALUE self);
static VALUE maths_mat_trasl(VALUE self,VALUE v_x,VALUE v_y,VALUE v_z);
static VALUE maths_mat_mult(VALUE self,VALUE v_m1,VALUE v_m2,VALUE v_only_rotscale);
static VALUE maths_mat_vect_mult(VALUE self,VALUE v_m,VALUE v_v);
static VALUE maths_mat_rot_axis(VALUE self,VALUE v_axis,VALUE v_angle);
static VALUE maths_apply_mat_to_dots(VALUE self,VALUE v_m,VALUE v_dots);
static VALUE maths_tait_bryan_to_mat(VALUE self,VALUE v_tb);
static VALUE maths_quat_unit(VALUE self);
static VALUE maths_quat_normalize(VALUE self,VALUE v_q);
static VALUE maths_quat_invert(VALUE self,VALUE v_q);
static VALUE maths_quat_quat_mult(VALUE self,VALUE v_q1,VALUE v_q2);
static VALUE maths_rot_vect_by_quat(VALUE self,VALUE v_q,VALUE v_v);
static VALUE maths_quat_to_tait_bryan(VALUE self,VALUE v_quat);
static VALUE maths_tait_bryan_to_quat(VALUE self,VALUE v_tb);
static VALUE maths_yaw_from_quat(VALUE self,VALUE v_q);
static VALUE maths_yawpitch_from_quat(VALUE self,VALUE v_q);
//static VALUE maths_yaw_from_quat_diff(VALUE self,VALUE v_q1,VALUE v_q2);
static VALUE maths_mat_to_quat(VALUE self,VALUE v_m);
static VALUE maths_quat_to_mat(VALUE self,VALUE v_q);
static VALUE maths_quat_vect_over_another(VALUE self,VALUE v_v1,VALUE v_v2);
static VALUE maths_point_distance(VALUE self,VALUE v_p1,VALUE v_p2);
static VALUE maths_three_points_to_plane(VALUE self,VALUE v_p1,VALUE v_p2,VALUE v_p3);
static VALUE maths_line_plane_intersect(VALUE self,VALUE v_plane_point,VALUE v_plane_normal,VALUE v_line_point,VALUE v_line_dir);
static VALUE maths_triangle_angles(VALUE self,VALUE v_p1,VALUE v_p2,VALUE v_p3);
static VALUE maths_angle_btw_vects(VALUE self,VALUE v_v1,VALUE v_v2);
static VALUE maths_yaw_pitch_angle(VALUE self,VALUE v_yaw_from,VALUE v_pitch_from,VALUE v_yaw_to,VALUE v_pitch_to);
static VALUE maths_velocity(VALUE self,VALUE v_from,VALUE v_to,VALUE v_time);
static VALUE maths_interpolate_series(VALUE self,VALUE v_series,VALUE v_interval);
static VALUE maths_seed_randgen(VALUE self,VALUE v_seed);
static VALUE maths_rand_block(VALUE self,VALUE v_size);
static VALUE maths_lines_closest_points(VALUE self,VALUE v_p1,VALUE v_v1,VALUE v_p2,VALUE v_v2,VALUE v_keepbehind);

static VALUE maths_tester(VALUE self);

static VALUE maths_transdsample_shift_expand(VALUE self,VALUE v_data,VALUE v_bitshift,VALUE v_expand_flg);

extern VALUE new_maths_smoother(VALUE self,VALUE v_hist_length,VALUE v_discard_thresh);
extern VALUE maths_smoother_feed(VALUE self,VALUE v_new_value);

extern VALUE new_maths_casteljau(VALUE self,VALUE v_array);
extern VALUE maths_casteljau_calc(VALUE self,VALUE f_pos);

extern VALUE new_maths_accsin(VALUE self);
extern VALUE maths_accsin_load(VALUE self,VALUE v_dof_from,VALUE v_dof_vect,VALUE v_type,VALUE v_p1,VALUE v_p2,VALUE v_p3);
extern VALUE maths_accsin_pos_at(VALUE self,VALUE v_at);
extern VALUE maths_accsin_end_pos_time(VALUE self);

extern VALUE new_maths_dho(VALUE self,VALUE v_m,VALUE v_spring_c,VALUE v_damping,VALUE v_init_pos,VALUE v_init_vel,
			   VALUE v_t_step,VALUE v_t_max,VALUE v_energy_min);
extern VALUE maths_dho_damp_success(VALUE self);
extern VALUE maths_dho_duration(VALUE self);
extern VALUE maths_dho_arrays(VALUE self);
extern VALUE maths_dho_calc_seq(VALUE self,VALUE v_fr_pos,VALUE v_to_pos);
extern VALUE maths_dho_pos_at(VALUE self,VALUE v_at);
extern VALUE maths_dho_end_pos_time(VALUE self);

extern void lg(const char *fmt,...);

const static hv vert_v={0.0,1.0,0.0};
const static hv forw_v={0.0,0.0,-1.0};
			    
void Init_maths(void)
{
  mod_maths=rb_define_module("Maths");

  rb_define_const(mod_maths,"RAD2DEG",DBL2NUM(RAD2DEG));
  rb_define_const(mod_maths,"DEG2RAD",DBL2NUM(DEG2RAD));
  
  rb_define_const(mod_maths,"MATTYPE_UNIT",INT2FIX(MATTYPE_UNIT));
  rb_define_const(mod_maths,"MATTYPE_TRASL",INT2FIX(MATTYPE_TRASL));
  rb_define_const(mod_maths,"MATTYPE_ROTAXIS",INT2FIX(MATTYPE_ROTAXIS));
  rb_define_const(mod_maths,"MATTYPE_SCALE",INT2FIX(MATTYPE_SCALE));
  rb_define_const(mod_maths,"MATTYPE_QUAT",INT2FIX(MATTYPE_QUAT));
  rb_define_const(mod_maths,"MATTYPE_RAW",INT2FIX(MATTYPE_RAW));

  rb_define_singleton_method(mod_maths,"vect_negate",maths_vect_neg,1);
  rb_define_singleton_method(mod_maths,"vect_add",maths_vect_add,2);
  rb_define_singleton_method(mod_maths,"vect_sub",maths_vect_sub,2);
  rb_define_singleton_method(mod_maths,"vect_mult_const",maths_vect_mult_const,2);
  rb_define_singleton_method(mod_maths,"vect_normalize",maths_vect_normalize,1);
  rb_define_singleton_method(mod_maths,"vect_unit_prop",maths_vect_unit_prop,1);
  rb_define_singleton_method(mod_maths,"vect_center",maths_vect_center,2);
  rb_define_singleton_method(mod_maths,"vect_dot_prod",maths_vect_dot_prod,2);
  rb_define_singleton_method(mod_maths,"vect_three_points_normal",maths_vect_three_points_normal,3);
  rb_define_singleton_method(mod_maths,"composed_mat",maths_composed_mat,1);
  rb_define_singleton_method(mod_maths,"apply_traslrot",maths_apply_traslrot,2);
  rb_define_singleton_method(mod_maths,"vector_over_another",maths_vector_over_another,2);
  rb_define_singleton_method(mod_maths,"mat_print",maths_mat_print,2);
  rb_define_singleton_method(mod_maths,"mat_unit",maths_mat_unit,0);
  rb_define_singleton_method(mod_maths,"mat_trasl",maths_mat_trasl,3);
  rb_define_singleton_method(mod_maths,"mat_mult",maths_mat_mult,3);
  rb_define_singleton_method(mod_maths,"mat_vect_mult",maths_mat_vect_mult,2);
  rb_define_singleton_method(mod_maths,"mat_rot_axis",maths_mat_rot_axis,2);
  rb_define_singleton_method(mod_maths,"apply_mat_to_dots",maths_apply_mat_to_dots,2);
  rb_define_singleton_method(mod_maths,"tait_bryan_to_mat",maths_tait_bryan_to_mat,1);
  rb_define_singleton_method(mod_maths,"quat_unit",maths_quat_unit,0);
  rb_define_singleton_method(mod_maths,"quat_normalize",maths_quat_normalize,1);
  rb_define_singleton_method(mod_maths,"quat_invert",maths_quat_invert,1);
  rb_define_singleton_method(mod_maths,"rot_vect_by_quat",maths_rot_vect_by_quat,2);
  rb_define_singleton_method(mod_maths,"quat_quat_mult",maths_quat_quat_mult,2);
  rb_define_singleton_method(mod_maths,"quat_to_tait_bryan",maths_quat_to_tait_bryan,1);
  rb_define_singleton_method(mod_maths,"tait_bryan_to_quat",maths_tait_bryan_to_quat,1);
  rb_define_singleton_method(mod_maths,"yaw_from_quat",maths_yaw_from_quat,1);
  rb_define_singleton_method(mod_maths,"yawpitch_from_quat",maths_yawpitch_from_quat,1);
//  rb_define_singleton_method(mod_maths,"yaw_from_quat_diff",maths_yaw_from_quat_diff,2);
  rb_define_singleton_method(mod_maths,"mat_to_quat",maths_mat_to_quat,1);
  rb_define_singleton_method(mod_maths,"quat_to_mat",maths_quat_to_mat,1);
  rb_define_singleton_method(mod_maths,"quat_vect_over_another",maths_quat_vect_over_another,2);
  rb_define_singleton_method(mod_maths,"point_distance",maths_point_distance,2);
  rb_define_singleton_method(mod_maths,"three_points_to_plane",maths_three_points_to_plane,3);
  rb_define_singleton_method(mod_maths,"line_plane_intersect",maths_line_plane_intersect,4);
  rb_define_singleton_method(mod_maths,"triangle_angles",maths_triangle_angles,3);
  rb_define_singleton_method(mod_maths,"angle_btw_vects",maths_angle_btw_vects,2);
  rb_define_singleton_method(mod_maths,"yaw_pitch_angle",maths_yaw_pitch_angle,4);
  rb_define_singleton_method(mod_maths,"velocity",maths_velocity,3);
  rb_define_singleton_method(mod_maths,"interpolate_series",maths_interpolate_series,2);
  rb_define_singleton_method(mod_maths,"seed_randgen",maths_seed_randgen,1);
  rb_define_singleton_method(mod_maths,"rand_block",maths_rand_block,1);
  rb_define_singleton_method(mod_maths,"lines_closest_points",maths_lines_closest_points,5);

  rb_define_singleton_method(mod_maths,"tester",maths_tester,0);

  rb_define_singleton_method(mod_maths,"transdsample_shift_expand",maths_transdsample_shift_expand,3);

  cls_maths_smoother=rb_define_class_under(mod_maths,"Smoother",rb_cObject);
  rb_define_singleton_method(cls_maths_smoother,"new",new_maths_smoother,2);
  rb_define_method(cls_maths_smoother,"feed",maths_smoother_feed,1);

  cls_maths_casteljau=rb_define_class_under(mod_maths,"Casteljau",rb_cObject);
  rb_define_singleton_method(cls_maths_casteljau,"new",new_maths_casteljau,1);
  rb_define_method(cls_maths_casteljau,"calc",maths_casteljau_calc,1);

  cls_maths_accsin=rb_define_class_under(mod_maths,"Accsin",rb_cObject);
  rb_define_const(cls_maths_accsin,"BOTH_LIN_ANG_ACC",INT2FIX(ACCSIN_BOTH_LIN_ANG_ACC));
  rb_define_const(cls_maths_accsin,"BOTH_LIN_ANG_DISP",INT2FIX(ACCSIN_BOTH_LIN_ANG_DISP));
  rb_define_const(cls_maths_accsin,"BOTH_ANG_LIN_ACC",INT2FIX(ACCSIN_BOTH_ANG_LIN_ACC));
  rb_define_const(cls_maths_accsin,"BOTH_ANG_LIN_DISP",INT2FIX(ACCSIN_BOTH_ANG_LIN_DISP));
  rb_define_const(cls_maths_accsin,"LIN_ACC_ANG_ACC_TIME",INT2FIX(ACCSIN_LIN_ACC_ANG_ACC_TIME));
  rb_define_const(cls_maths_accsin,"LIN_ACC_ANG_DISP_TIME",INT2FIX(ACCSIN_LIN_ACC_ANG_DISP_TIME));
  rb_define_const(cls_maths_accsin,"LIN_DISP_ANG_ACC_TIME",INT2FIX(ACCSIN_LIN_DISP_ANG_ACC_TIME));
  rb_define_const(cls_maths_accsin,"LIN_DISP_ANG_DISP_TIME",INT2FIX(ACCSIN_LIN_DISP_ANG_DISP_TIME));
  rb_define_singleton_method(cls_maths_accsin,"new",new_maths_accsin,0);
  rb_define_method(cls_maths_accsin,"load",maths_accsin_load,6);
  rb_define_method(cls_maths_accsin,"pos_at",maths_accsin_pos_at,1);
  rb_define_method(cls_maths_accsin,"end_pos_time",maths_accsin_end_pos_time,0);

  cls_maths_dho=rb_define_class_under(mod_maths,"Dho",rb_cObject);
  rb_define_singleton_method(cls_maths_dho,"new",new_maths_dho,8);
  rb_define_method(cls_maths_dho,"damp_success",maths_dho_damp_success,0);
  rb_define_method(cls_maths_dho,"duration",maths_dho_duration,0);
  rb_define_method(cls_maths_dho,"arrays",maths_dho_arrays,0);
  rb_define_method(cls_maths_dho,"calc_seq",maths_dho_calc_seq,2);
  rb_define_method(cls_maths_dho,"pos_at",maths_dho_pos_at,1);
  rb_define_method(cls_maths_dho,"end_pos_time",maths_dho_end_pos_time,0);
}

static VALUE maths_vect_neg(VALUE self,VALUE v_v)
{
  hv v;

  memcpy(v,RSTRING_PTR(v_v),sizeof(hv));

  v[0]=-v[0];
  v[1]=-v[1];
  v[2]=-v[2];

  return rb_str_new((char *)v,sizeof(hv));  
}

static VALUE maths_vect_add(VALUE self,VALUE v_v1,VALUE v_v2)
{
  hv v1,v2,v3;

  memcpy(v1,RSTRING_PTR(v_v1),sizeof(hv));
  memcpy(v2,RSTRING_PTR(v_v2),sizeof(hv));

  vect_add(v1,v2,v3);

  return rb_str_new((char *)v3,sizeof(hv));  
}

static VALUE maths_vect_sub(VALUE self,VALUE v_v1,VALUE v_v2)
{
  hv v1,v2,v3;

  memcpy(v1,RSTRING_PTR(v_v1),sizeof(hv));
  memcpy(v2,RSTRING_PTR(v_v2),sizeof(hv));

  vect_sub(v1,v2,v3);
  
  return rb_str_new((char *)v3,sizeof(hv));  
}

static VALUE maths_vect_mult_const(VALUE self,VALUE v_v,VALUE v_const)
{
  hv v1,v2;
  float c=NUM2DBL(v_const);

  memcpy(v1,RSTRING_PTR(v_v),sizeof(hv));
  vect_mult_const(v1,c,v2);
  
  return rb_str_new((char *)v2,sizeof(hv));  
}

static VALUE maths_vect_normalize(VALUE self,VALUE v_v)
{
  hv v1,v2;

  memcpy(v1,RSTRING_PTR(v_v),sizeof(hv));

  float modulo=vect_normalize(v1,v2);

  VALUE to_ret=rb_ary_new_capa(2);

  rb_ary_store(to_ret,0,rb_str_new((char *)v2,sizeof(hv)));
  rb_ary_store(to_ret,1,DBL2NUM(modulo));

  return to_ret;
}

static VALUE maths_vect_unit_prop(VALUE self,VALUE v_v)
{
  hv v1,v2;

  memcpy(v1,RSTRING_PTR(v_v),sizeof(hv));
  vect_unit_prop(v1,v2);
  return rb_str_new((char *)v2,sizeof(hv));
}

static VALUE maths_vect_center(VALUE self,VALUE v_v1,VALUE v_v2)
{
  hv v1,v2,v3;

  memcpy(v1,RSTRING_PTR(v_v1),sizeof(hv));
  memcpy(v2,RSTRING_PTR(v_v2),sizeof(hv));

  vect_center(v1,v2,v3);
  
  return rb_str_new((char *)v3,sizeof(hv));
}

static VALUE maths_vect_dot_prod(VALUE self,VALUE v_v1,VALUE v_v2)
{
  hv v1,v2;

  memcpy(v1,RSTRING_PTR(v_v1),sizeof(hv));
  memcpy(v2,RSTRING_PTR(v_v2),sizeof(hv));

  return DBL2NUM(vect_dot_prod(v1,v2));
}

static VALUE maths_vect_three_points_normal(VALUE self,VALUE v_v1,VALUE v_v2,VALUE v_v3)
{
  hv v[3],vn;

  memcpy(v[0],RSTRING_PTR(v_v1),sizeof(hv));
  memcpy(v[1],RSTRING_PTR(v_v2),sizeof(hv));
  memcpy(v[2],RSTRING_PTR(v_v3),sizeof(hv));

  vect_three_points_normal(v,vn);

  return rb_str_new((char *)vn,sizeof(hv));
}

static VALUE maths_composed_mat(VALUE self,VALUE v_arr)
{
  int n_el=RARRAY_LEN(v_arr),i;
  hm4 m1,m2,resm;
  hv vect;
  hv4 q;
  float f;
  VALUE v;
  mattype_enum type;

  mat4_unit(resm);

  for(i=0;i<n_el;i++)
  {
    v=rb_ary_entry(v_arr,i);
    type=FIX2INT(rb_ary_entry(v,0));

    memcpy(m1,resm,sizeof(hm4));    

    switch(type)
    {
    case MATTYPE_UNIT:
      mat4_unit(m2);
      break;
    case MATTYPE_TRASL:
      vect[0]=NUM2DBL(rb_ary_entry(v,1));
      vect[1]=NUM2DBL(rb_ary_entry(v,2));
      vect[2]=NUM2DBL(rb_ary_entry(v,3));
      mat4_trasl(vect,m2);
      break;
    case MATTYPE_ROTAXIS:
      vect[0]=NUM2DBL(rb_ary_entry(v,1));
      vect[1]=NUM2DBL(rb_ary_entry(v,2));
      vect[2]=NUM2DBL(rb_ary_entry(v,3));
      f=NUM2DBL(rb_ary_entry(v,4));
      mat4_rot_axis(vect,f,m2);
      break;
    case MATTYPE_SCALE:
      vect[0]=vect[1]=vect[2]=NUM2DBL(rb_ary_entry(v,1));
      mat4_scale(vect,m2);
      break;
    case MATTYPE_QUAT:
      memcpy(q,RSTRING_PTR(rb_ary_entry(v,1)),sizeof(hv4));
      quat_to_hm4(q,m2);
      break;
    case MATTYPE_RAW:
      memcpy(m2,RSTRING_PTR(rb_ary_entry(v,1)),sizeof(hm4));
      break;
    }

    mat4_mult(m1,m2,resm);
  }
  
  return rb_str_new((char *)resm,sizeof(hm4));
}

/* This one is for the moog! */

static VALUE maths_apply_traslrot(VALUE self,VALUE v_base,VALUE v_traslrot)
{
  float *bv=(float *)RSTRING_PTR(v_base),*trv=(float *)RSTRING_PTR(v_traslrot),*fp=bv+9,trasldist;
//  hv4 q;
  hv b1,b2,mv,trasldot,traslvect;
  hm4 mtb,m1,m2,rotm;

//tait_bryan_to_quat(trv+3,q);  
//quat_to_hm4(q,m1);
  mat4_tait_bryan(trv+3,m1);

  mat4_unit(m2);
  m2[3][0]=trv[0];
  m2[3][1]=trv[1];
  m2[3][2]=trv[2];

  mat4_mult(m1,m2,mtb);    

  int i,j;
  VALUE to_ret=rb_ary_new_capa(7),v;

  for(i=0;i<6;i++)
  {
    j=(i+5)%6;
    
    memcpy(b1,fp+3*i,sizeof(hv));
    memcpy(b2,fp+3*j,sizeof(hv));
    
    mat4_vect_mult(mtb,b2,trasldot);
//    lg("=> %d :from %.3f,%.3f,%.3f  to %.3f,%.3f,%.3f  => %.3f,%.3f,%.3f",i,b1[0],b1[1],b1[2],b2[0],b2[1],b2[2],trasldot[0],trasldot[1],trasldot[2]);
  
    vect_sub(trasldot,b1,mv);
    trasldist=vect_normalize(mv,traslvect);
    
//    lg("Point %d: %.3f,%.3f,%.3f -> ori %.3f,%.3f,%.3f (len %.3f)",
//       i,trasldot[0],trasldot[1],trasldot[2],
//       traslvect[0],traslvect[1],traslvect[2],trasldist);

    mat4_vect_over_another(traslvect,vert_v,rotm);

    v=rb_ary_new_capa(4);
    rb_ary_store(v,0,rb_str_new((char *)trasldot,sizeof(hv)));
    rb_ary_store(v,1,rb_str_new((char *)traslvect,sizeof(hv)));
    rb_ary_store(v,2,rb_str_new((char *)rotm,sizeof(hm4)));
    rb_ary_store(v,3,DBL2NUM(trasldist));
    rb_ary_store(to_ret,i,v);    
  }

  rb_ary_store(to_ret,6,rb_str_new((char *)mtb,sizeof(hm4)));    

  return to_ret;
}

static VALUE maths_vector_over_another(VALUE self,VALUE v_v1,VALUE v_v2)
{
  hv v1,v2;

  memcpy(v1,RSTRING_PTR(v_v1),sizeof(hv));
  memcpy(v2,RSTRING_PTR(v_v2),sizeof(hv));

  hm4 m;

  mat4_vect_over_another(v1,v2,m);

  return rb_str_new((char *)m,sizeof(hm4));
}

static VALUE maths_mat_print(VALUE self,VALUE v_tag,VALUE v_m)
{
  hm4 m;

  memcpy(m,RSTRING_PTR(v_m),sizeof(hm4));
  mat4_print(RSTRING_PTR(v_tag),m);

  return self;
}

static VALUE maths_mat_unit(VALUE self)
{
  hm4 m;

  mat4_unit(m);
  return rb_str_new((char *)m,sizeof(hm4));
}

static VALUE maths_mat_trasl(VALUE self,VALUE v_x,VALUE v_y,VALUE v_z)
{
  hm4 m;
  hv vect;

  vect[0]=NUM2DBL(v_x);
  vect[1]=NUM2DBL(v_y);
  vect[2]=NUM2DBL(v_z);

  mat4_trasl(vect,m);
  return rb_str_new((char *)m,sizeof(hm4));
}

static VALUE maths_mat_mult(VALUE self,VALUE v_m1,VALUE v_m2,VALUE v_only_rotscale)
{
  hm4 m1,m2,mr;

  memcpy(m1,RSTRING_PTR(v_m1),sizeof(hm4));
  memcpy(m2,RSTRING_PTR(v_m2),sizeof(hm4));

  if(v_only_rotscale!=Qtrue)
    mat4_mult(m1,m2,mr);
  else
    mat4_mult_only_rotscale(m1,m2,mr);
  
  return rb_str_new((char *)mr,sizeof(hm4));
}

static VALUE maths_mat_vect_mult(VALUE self,VALUE v_m,VALUE v_v)
{
  hm4 m;
  hv v1,v2;

  memcpy(m,RSTRING_PTR(v_m),sizeof(hm4));
  memcpy(v1,RSTRING_PTR(v_v),sizeof(hv));

  mat4_vect_mult(m,v1,v2);

  return rb_str_new((char *)v2,sizeof(hv));
}

static VALUE maths_mat_rot_axis(VALUE self,VALUE v_axis,VALUE v_angle)
{
  float ang=NUM2DBL(v_angle);
  hv v;
  hm4 m;

  memcpy(v,RSTRING_PTR(v_axis),sizeof(hv));
  mat4_rot_axis(v,ang,m);
  
  return rb_str_new((char *)m,sizeof(hm4));
}

static VALUE maths_apply_mat_to_dots(VALUE self,VALUE v_m,VALUE v_dots)
{
  int n_dots=RARRAY_LEN(v_dots),i;
  hm4 mat;
  hv d;
  VALUE v,to_ret=rb_ary_new_capa(n_dots);

  memcpy(mat,RSTRING_PTR(v_m),sizeof(hm4));
  
  for(i=0;i<n_dots;i++)
  {
    v=rb_ary_entry(v_dots,i);
    d[0]=NUM2DBL(rb_ary_entry(v,0));
    d[1]=NUM2DBL(rb_ary_entry(v,1));
    d[2]=NUM2DBL(rb_ary_entry(v,2));
    mat4_vect_mult(mat,d,d);
    v=rb_ary_new_capa(3);
    rb_ary_store(v,0,DBL2NUM(d[0]));
    rb_ary_store(v,1,DBL2NUM(d[1]));
    rb_ary_store(v,2,DBL2NUM(d[2]));
    rb_ary_store(to_ret,i,v);
  }

  return to_ret;
}


static VALUE maths_tait_bryan_to_mat(VALUE self,VALUE v_tb)
{
  hv tb;
  hm4 m;

  memcpy(tb,RSTRING_PTR(v_tb),sizeof(hv));

  mat4_tait_bryan(tb,m);

  return rb_str_new((char *)m,sizeof(hm4));
}

static VALUE maths_quat_unit(VALUE self)
{
  hv4 q;

  quat_unit(q);
  return rb_str_new((char *)q,sizeof(hv4));
}

static VALUE maths_quat_normalize(VALUE self,VALUE v_q)
{
  hv4 q;

  memcpy(q,RSTRING_PTR(v_q),sizeof(hv4));
  quat_normalize(q);
  return rb_str_new((char *)q,sizeof(hv4));
}

static VALUE maths_quat_invert(VALUE self,VALUE v_q)
{
  hv4 q1,q2;

  memcpy(q1,RSTRING_PTR(v_q),sizeof(hv4));
  quat_invert(q1,q2);
  return rb_str_new((char *)q2,sizeof(hv4));
}

static VALUE maths_quat_quat_mult(VALUE self,VALUE v_q1,VALUE v_q2)
{
  hv4 q1,q2,q3;
  
  memcpy(q1,RSTRING_PTR(v_q1),sizeof(hv4));
  memcpy(q2,RSTRING_PTR(v_q2),sizeof(hv4));

  quat_quat_mult(q1,q2,q3);
  
  return rb_str_new((char *)q3,sizeof(hv4));
}

static VALUE maths_rot_vect_by_quat(VALUE self,VALUE v_q,VALUE v_v)
{
  hv4 q;
  hv v1,v2;

  memcpy(q,RSTRING_PTR(v_q),sizeof(hv4));
  memcpy(v1,RSTRING_PTR(v_v),sizeof(hv));

  rot_vect_by_quat(q,v1,v2);
  
  return rb_str_new((char *)v2,sizeof(hv));
}

static VALUE maths_quat_to_tait_bryan(VALUE self,VALUE v_quat)
{
  hv4 q;
  hv tb;

  memcpy(q,RSTRING_PTR(v_quat),sizeof(hv4));

  quat_to_tait_bryan(q,tb);

  return rb_str_new((char *)tb,sizeof(hv));
}

static VALUE maths_tait_bryan_to_quat(VALUE self,VALUE v_tb)
{
  hv tb;
  hv4 q;

  memcpy(tb,RSTRING_PTR(v_tb),sizeof(hv));
  tait_bryan_to_quat(tb,q);
  return rb_str_new((char *)q,sizeof(hv4));
}

static VALUE maths_yaw_from_quat(VALUE self,VALUE v_q)
{
  hv4 q;
  hv v;

  memcpy(q,RSTRING_PTR(v_q),sizeof(hv4));
#if 0  
  quat_to_tait_bryan(q,v);
  
  return DBL2NUM(v[0]);
#else
  rot_vect_by_quat(q,forw_v,v);
  return DBL2NUM(asinf(v[0]/sqrtf(v[0]*v[0]+v[2]*v[2])));
#endif  
}

static VALUE maths_yawpitch_from_quat(VALUE self,VALUE v_q)
{
  hv4 q;
  hv v;

  memcpy(q,RSTRING_PTR(v_q),sizeof(hv4));
  rot_vect_by_quat(q,forw_v,v);

  VALUE to_ret=rb_ary_new_capa(2);

  rb_ary_store(to_ret,0,DBL2NUM(asinf(v[0]/sqrtf(v[0]*v[0]+v[2]*v[2]))));
  rb_ary_store(to_ret,1,DBL2NUM(asinf(v[2]/sqrtf(v[2]*v[2]+v[1]*v[1]))));
  
  return to_ret;
}

#if 0
static VALUE maths_yaw_from_quat_diff(VALUE self,VALUE v_q1,VALUE v_q2)
{
  hv4 q1,q2,q3;
  hv v;

  memcpy(q1,RSTRING_PTR(v_q1),sizeof(hv4));
  quat_invert(q1,q1);
  memcpy(q2,RSTRING_PTR(v_q2),sizeof(hv4));
#if 0
  quat_quat_mult(q1,q2,q3);
  quat_to_tait_bryan(q3,v);
//  printf("\r<%10.3f><%10.3f><%10.3f>",v[0],v[1],v[2]);
  return DBL2NUM(v[0]);
#else
  VALUE to_ret=rb_ary_new_capa(4);
  
  quat_to_tait_bryan(q2,v);
  rb_ary_store(to_ret,0,DBL2NUM(v[0]));
  
  quat_quat_mult(q1,q2,q3);  
  quat_to_tait_bryan(q3,v);
  rb_ary_store(to_ret,1,DBL2NUM(v[0]));
	       
  quat_quat_mult(q2,q1,q3);
  quat_to_tait_bryan(q3,v);
  rb_ary_store(to_ret,2,DBL2NUM(v[0]));

  quat_invert(q2,q1);
  rot_vect_by_quat(q1,forw_v,v);
  
  rb_ary_store(to_ret,3,DBL2NUM(asinf(v[0]/sqrtf(v[0]*v[0]+v[2]*v[2]))));  
  
  return to_ret;
#endif  
}
#endif
    
static VALUE maths_mat_to_quat(VALUE self,VALUE v_m)
{
  hm4 m;
  hv4 q;

  memcpy(m,RSTRING_PTR(v_m),sizeof(hm4));
  hm4_to_quat(m,q);
  
  return rb_str_new((char *)q,sizeof(hv4));
}

static VALUE maths_quat_to_mat(VALUE self,VALUE v_q)
{
  hm4 m;
  hv4 q;

  memcpy(q,RSTRING_PTR(v_q),sizeof(hv4));
  quat_to_hm4(q,m);
  
  return rb_str_new((char *)m,sizeof(hm4));
}

static VALUE maths_quat_vect_over_another(VALUE self,VALUE v_v1,VALUE v_v2)
{
  hv v1,v2;
  hv4 q;

  memcpy(v1,RSTRING_PTR(v_v1),sizeof(hv));
  memcpy(v2,RSTRING_PTR(v_v2),sizeof(hv));

  quat_vect_over_another(v1,v2,q);

  return rb_str_new((char *)q,sizeof(hv4));  
}


static VALUE maths_point_distance(VALUE self,VALUE v_p1,VALUE v_p2)
{
  hv p1,p2;

  memcpy(p1,RSTRING_PTR(v_p1),sizeof(hv));
  memcpy(p2,RSTRING_PTR(v_p2),sizeof(hv));

  return DBL2NUM(point_distance(p1,p2));
}

static VALUE maths_three_points_to_plane(VALUE self,VALUE v_p1,VALUE v_p2,VALUE v_p3)
{
  hv p1,p2,p3;

  memcpy(p1,RSTRING_PTR(v_p1),sizeof(hv));
  memcpy(p2,RSTRING_PTR(v_p2),sizeof(hv));
  memcpy(p3,RSTRING_PTR(v_p3),sizeof(hv));

  hv4 coef;

  three_points_to_plane(p1,p2,p3,coef);

  return rb_str_new((char *)coef,sizeof(hv4));
}

static VALUE maths_line_plane_intersect(VALUE self,VALUE v_plane_point,VALUE v_plane_normal,VALUE v_line_point,VALUE v_line_dir)
{
  hv v1,v2,v3,v4,v5;
  float fact=0.0;

  memcpy(v1,RSTRING_PTR(v_plane_point),sizeof(hv));
  memcpy(v2,RSTRING_PTR(v_plane_normal),sizeof(hv));
  memcpy(v3,RSTRING_PTR(v_line_point),sizeof(hv));
  memcpy(v4,RSTRING_PTR(v_line_dir),sizeof(hv));

  if(!line_plane_intersect(v1,v2,v3,v4,v5,&fact) || fact<0)
  {
    lg("Gh! fact %f",fact);
    return Qfalse;
  }

  return rb_str_new((char *)v5,sizeof(hv));
}

static VALUE maths_triangle_angles(VALUE self,VALUE v_p1,VALUE v_p2,VALUE v_p3)
{
  hv p1,p2,p3;
  float l1,l2,l3;

  memcpy(p1,RSTRING_PTR(v_p1),sizeof(hv));
  memcpy(p2,RSTRING_PTR(v_p2),sizeof(hv));
  memcpy(p3,RSTRING_PTR(v_p3),sizeof(hv));
  
  triangle_angles(p1,p2,p3,&l1,&l2,&l3);
  
  VALUE to_ret=rb_ary_new_capa(3);

  rb_ary_store(to_ret,0,DBL2NUM(l1));
  rb_ary_store(to_ret,1,DBL2NUM(l2));
  rb_ary_store(to_ret,2,DBL2NUM(l3));

  return to_ret;
}

static VALUE maths_angle_btw_vects(VALUE self,VALUE v_v1,VALUE v_v2)
{
  hv v1,v2;

  memcpy(v1,RSTRING_PTR(v_v1),sizeof(hv));
  memcpy(v2,RSTRING_PTR(v_v2),sizeof(hv));

  return DBL2NUM(angle_btw_vects(v1,v2));
}

static VALUE maths_yaw_pitch_angle(VALUE self,VALUE v_yaw_from,VALUE v_pitch_from,VALUE v_yaw_to,VALUE v_pitch_to)
{
  float yaw_from=NUM2DBL(v_yaw_from),pitch_from=NUM2DBL(v_pitch_from),yaw_to=NUM2DBL(v_yaw_to),pitch_to=NUM2DBL(v_pitch_to);

  return DBL2NUM(yaw_pitch_angle(yaw_from,pitch_from,yaw_to,pitch_to));
}

/*
 * Receives pos 1 and pos 2 as server-format positions, returns linear & angular velocity
 */

static VALUE maths_velocity(VALUE self,VALUE v_from,VALUE v_to,VALUE v_time)
{
  hv lin1,ang1,lin2,ang2;
  float time=NUM2DBL(v_time),lvel,avel;
  int i;

  for(i=0;i<3;i++)
  {
    lin1[i]=NUM2DBL(rb_ary_entry(v_from,i))/1000.0;
    ang1[i]=NUM2DBL(rb_ary_entry(v_from,i+3))*180.0/M_PI;
    lin2[i]=NUM2DBL(rb_ary_entry(v_to,i))/1000.0;
    ang2[i]=NUM2DBL(rb_ary_entry(v_to,i+3))*180.0/M_PI;
  }

  velocity(lin1,ang1,lin2,ang2,time,&lvel,&avel);

  VALUE to_ret=rb_ary_new_capa(2);

  rb_ary_store(to_ret,0,DBL2NUM(lvel));
  rb_ary_store(to_ret,1,DBL2NUM(avel));
  
  return to_ret;
}

static VALUE maths_interpolate_series(VALUE self,VALUE v_series,VALUE v_interval)
{
  timepoints_stc vin,vout;
  unsigned int interval=FIX2INT(v_interval),i;
  timepoint_stc *vp;
  VALUE v,v2;

  vin.n_points=RARRAY_LEN(v_series);
  vin.n_values=RSTRING_LEN(rb_ary_entry(rb_ary_entry(v_series,0),1))/sizeof(float);
  vin.tp=malloc(sizeof(timepoint_stc)*vin.n_points);

  for(vp=vin.tp,i=0;i<vin.n_points;i++,vp++)
  {
    v=rb_ary_entry(v_series,i);
    vp->msec=FIX2INT(rb_ary_entry(v,0));
    v2=rb_ary_entry(v,1);
    if(RSTRING_LEN(v2)!=sizeof(float)*vin.n_values)
      rb_raise(rb_eArgError,"%s: Bad data string at pos %d (len %ld)!",__func__,i,RSTRING_LEN(v2));
    
    vp->values=malloc(sizeof(float)*vin.n_values);
    memcpy(vp->values,RSTRING_PTR(v2),sizeof(float)*vin.n_values);
  }
  
  interpolate_series(&vin,interval,&vout);

  VALUE to_ret;
  
  if(vout.n_points<=0)
    to_ret=Qfalse;
  else
  {
    to_ret=rb_ary_new_capa(vout.n_points);
    
    for(vp=vout.tp,i=0;i<vout.n_points;i++,vp++)
    {
      v=rb_ary_new_capa(2);
      rb_ary_store(v,0,INT2FIX(vp->msec));
      rb_ary_store(v,1,rb_str_new((char *)vp->values,sizeof(float)*vout.n_values));
      rb_ary_store(to_ret,i,v);
      
      free(vp->values);    
    }
    free(vout.tp);
  }
    
  for(vp=vin.tp,i=0;i<vin.n_points;i++,vp++)
    free(vp->values);  
  free(vin.tp);

  return to_ret;
}

static VALUE maths_seed_randgen(VALUE self,VALUE v_seed)
{
  long int seed=FIX2INT(v_seed);

  srand48(seed);
  return self;
}

static VALUE maths_rand_block(VALUE self,VALUE v_size)
{
  int size=FIX2INT(v_size),i;
  char *block=malloc(size);

  for(i=0;i<size;i++)
    block[i]=lrand48()%256;

  VALUE to_ret=rb_str_new(block,size);

  free(block);
  return to_ret;
}

static VALUE maths_lines_closest_points(VALUE self,VALUE v_p1,VALUE v_v1,VALUE v_p2,VALUE v_v2,VALUE v_keepbehind)
{
  hv p1,v1,p2,v2,cp1,cp2;
  float sc,tc;

  memcpy(p1,RSTRING_PTR(v_p1),sizeof(hv));
  memcpy(v1,RSTRING_PTR(v_v1),sizeof(hv));
  memcpy(p2,RSTRING_PTR(v_p2),sizeof(hv));
  memcpy(v2,RSTRING_PTR(v_v2),sizeof(hv));

  lines_closest_points(p1,v1,p2,v2,cp1,cp2,&sc,&tc);

  if((sc<=0 || tc<=0) && v_keepbehind!=Qtrue)
    return Qnil;

  VALUE to_ret=rb_ary_new_capa(2);

  rb_ary_store(to_ret,0,rb_str_new((char *)cp1,sizeof(hv)));
  rb_ary_store(to_ret,1,rb_str_new((char *)cp2,sizeof(hv)));

  return to_ret;
}

static VALUE maths_tester(VALUE self)
{
  hv fw={0.0,0.0,-1.0},up={0.0,1.0,0.0},mypos={-10.0,15.0,7.5},lookat={23.0,7.3,-20.0};
  hm4 m1,m2;

  mat4_lookat(mypos,lookat,up,m1);

  hv v,lookdir={0.0,0.0,0.0};

  vect_sub(lookat,mypos,v);
  vect_normalize(v,lookdir);
  
  float dotp=vect_dot_prod(fw,lookdir);
  hv crossp;
  hv4 q;
  
  vect_cross_prod(fw,lookdir,crossp);
  quat_from_vect_angle(crossp,dotp,q);
  mat4_lookat_from_quat(mypos,q,m2);

  mat4_print("Mine",m1);
  mat4_print("From quat",m2);

  return self;
}

/*
 * Pass a block of 16-bit unsigned data (coming from ffmpeg output). All samples have to be right_shifted the given amount of bits.
 * If requested, the operation is performed expanding the existing range
 */

static VALUE maths_transdsample_shift_expand(VALUE self,VALUE v_data,VALUE v_bitshift,VALUE v_expand_flg)
{
  int n_samples=RSTRING_LEN(v_data)/sizeof(uint16_t),bs=FIX2INT(v_bitshift),i;
  uint16_t *indata=(uint16_t *)RSTRING_PTR(v_data),*outdata=malloc(sizeof(uint16_t)*n_samples),*dp1,*dp2;

  lg("Expanding: %s",v_expand_flg==Qtrue ? "YES" : "no");

  if(v_expand_flg!=Qtrue)
    for(dp1=indata,dp2=outdata,i=0;i<n_samples;i++,dp1++,dp2++)
      *dp2=(*dp1)>>bs;
  else
  {
    uint16_t minv=0xffff,maxv=0,maxtgt=0xffff>>bs;
    for(dp1=indata,i=0;i<n_samples;i++,dp1++)
    {
      if((*dp1)<minv)
	minv=(*dp1);
      if((*dp1)>maxv)
	maxv=(*dp1);
    }

    uint16_t range=maxv-minv;
    float fact=(float)maxtgt/range;

    for(dp1=indata,dp2=outdata,i=0;i<n_samples;i++,dp1++,dp2++)
      *dp2=(uint16_t)((*dp1)-minv)*fact;
  }

  VALUE to_ret=rb_str_new((char *)outdata,sizeof(uint16_t)*n_samples);

  free(outdata);
  return to_ret;
}

inline void vect_negate(const hv c1,hv c2)
{
  c2[0]=-c1[0];
  c2[1]=-c1[1];
  c2[2]=-c1[2];
}

inline void vect_add(const hv c1,const hv c2,hv c3)
{
  c3[0]=c1[0]+c2[0];
  c3[1]=c1[1]+c2[1];
  c3[2]=c1[2]+c2[2];
}
inline void vect_add_d(const hv_d c1,const hv_d c2,hv_d c3)
{
  c3[0]=c1[0]+c2[0];
  c3[1]=c1[1]+c2[1];
  c3[2]=c1[2]+c2[2];
}

inline void vect_sub(const hv c1,const hv c2,hv c3)
{
  c3[0]=c1[0]-c2[0];
  c3[1]=c1[1]-c2[1];
  c3[2]=c1[2]-c2[2];
}
inline void vect_sub_d(const hv_d c1,const hv_d c2,hv_d c3)
{
  c3[0]=c1[0]-c2[0];
  c3[1]=c1[1]-c2[1];
  c3[2]=c1[2]-c2[2];
}

inline void vect_mult(const hv c1,const hv c2,hv c3)
{
  c3[0]=c1[0]*c2[0];
  c3[1]=c1[1]*c2[1];
  c3[2]=c1[2]*c2[2];
}

inline void vect_mult_const(const hv v1,const float c,hv v2)
{
  v2[0]=v1[0]*c;
  v2[1]=v1[1]*c;
  v2[2]=v1[2]*c;
}

inline void vect_mult_const_d(const hv_d v1,const double c,hv_d v2)
{
  v2[0]=v1[0]*c;
  v2[1]=v1[1]*c;
  v2[2]=v1[2]*c;
}

inline float vect_normalize(const hv inv,hv outv)
{
  double sum=inv[0]*inv[0]+inv[1]*inv[1]+inv[2]*inv[2];
  if(sum<DBL_EPSILON)
    return 0.0;
  
  double len=sqrt(sum);

  outv[0]=inv[0]/len;
  outv[1]=inv[1]/len;
  outv[2]=inv[2]/len;

  return len;
}

inline float vect_modulo(const hv c)
{
  return sqrtf(c[0]*c[0]+c[1]*c[1]+c[2]*c[2]);
}

inline float vect_dot_prod(const hv a,const hv b)
{
  return a[0]*b[0]+a[1]*b[1]+a[2]*b[2];
}
inline double vect_dot_prod_d(const hv_d a,const hv_d b)
{
  return a[0]*b[0]+a[1]*b[1]+a[2]*b[2];
}

inline void vect_cross_prod(const hv c1,const hv c2,hv c3)
{
  c3[0]=c1[1]*c2[2]-c1[2]*c2[1];
  c3[1]=c1[2]*c2[0]-c1[0]*c2[2];
  c3[2]=c1[0]*c2[1]-c1[1]*c2[0];
}

inline void vect_center(const hv v1,const hv v2,hv vr)
{
  int i;

  for(i=0;i<3;i++)
    vr[i]=v1[i]+(v2[i]-v1[i])/2.0;
}

inline float vect_max(const hv v)
{
  if(v[0]>v[1])
    return (v[0]>v[2] ? v[0] : v[2]);
  return (v[1]>v[2] ? v[1] : v[2]);
}

void vect_three_points_normal(const hv p[3],hv norm)
{
  hv v1,v2,v3;

  vect_sub(p[1],p[0],v1);
  vect_sub(p[2],p[0],v2);
  vect_cross_prod(v1,v2,v3);

  vect_normalize(v3,norm);
}

// https://math.stackexchange.com/questions/1993953/closest-points-between-two-lines APPARENTLY BAD
// http://geomalgorithms.com/a07-_distance.html

void lines_closest_points(const hv p1,const hv v1,const hv p2,const hv v2,hv cp1,hv cp2,float *scr,float *tcr)
{
#if 0
  hv d1,d2,crp,n1,n2,v;
  float f;
  
  vect_sub(p1,p2,d1);
  vect_sub(p2,p1,d2);

  vect_cross_prod(v2,v1,crp);
  vect_cross_prod(crp,v1,n1);
  vect_cross_prod(crp,v2,n2);
  
  f=vect_dot_prod(d2,n2)/vect_dot_prod(v1,n2);
  vect_mult_const(v1,f,v);
  vect_add(p1,v,cp1);

  f=vect_dot_prod(d1,n1)/vect_dot_prod(v2,n1);
  vect_mult_const(v2,f,v);
  vect_add(p2,v,cp2);
  /* lg("   %8.3f,%8.3f,%8.3f and  %8.3f,%8.3f,%8.3f GIVE %8.3f,%8.3f,%8.3f and  %8.3f,%8.3f,%8.3f", */
  /*    v1[0],v1[1],v1[2],v2[0],v2[1],v2[2], */
  /*    cp1[0],cp1[1],cp1[2],cp2[0],cp2[1],cp2[2]); */
#else
  hv_d p1_d,p2_d,v1_d,v2_d,w1,w2;
  int i;

  for(i=0;i<3;i++)
  {
    p1_d[i]=p1[i];
    p2_d[i]=p2[i];
    v1_d[i]=v1[i];
    v2_d[i]=v2[i];
  }  

  vect_sub_d(p1_d,p2_d,w1);

  double a=vect_dot_prod_d(v1_d,v1_d),b=vect_dot_prod_d(v1_d,v2_d),c=vect_dot_prod_d(v2_d,v2_d),
    d=vect_dot_prod_d(v1_d,w1),e=vect_dot_prod_d(v2_d,w1),dd=a*c-b*b,sc,tc;
  
  if(dd<1e-10) // almost parallel
  {
    sc=0.0;
    tc=(b>c) ? d/b : e/c;
  }
  else
  {
    sc=(b*e-c*d)/dd;
    tc=(a*e-b*d)/dd;
  }

//  if(sc<0 || tc<0)
//    fprintf(stderr,"<%.2f,%.2f>",sc,tc);

  (*scr)=(float)sc;
  (*tcr)=(float)tc;
  
  vect_mult_const_d(v1_d,sc,w1);
  vect_add_d(w1,p1_d,w1);
  
  vect_mult_const_d(v2_d,tc,w2);
  vect_add_d(w2,p2_d,w2);

  for(i=0;i<3;i++)
  {
    cp1[i]=w1[i];
    cp2[i]=w2[i];
  } 
#endif  
}

/*
 * I have a vector, first value is fixed. Vector must be unit-long.
 * Here I compute the other two components
 */

void vect_unit_prop(const hv pin,hv pout)
{
  pout[0]=pin[0];
  
  if(pin[2]==0.0)
  {
    if(pin[1]==0.0)
      pout[1]=pout[2]=sqrtf((1.0-pout[0]*pout[0])/2.0);
    else
    {
      pout[2]=0.0;
      pout[1]=sqrtf(1.0-pout[0]*pout[0]);
      if(pin[1]<0)
	pout[1]=-pout[1];
    }
  }
  else
  {
    float ratio=pin[1]/pin[2];
    
    pout[2]=sqrtf((1.0-pin[0]*pin[0])/(ratio*ratio+1.0));

    if(pin[2]<0)
      pout[2]=-pout[2];
    
    pout[1]=ratio*pout[2];
//    lg("From %.3f,%.3f,%.3f to %.3f,%.3f,%.3f",pin[0],pin[1],pin[2],pout[0],pout[1],pout[2]);
  }  
}

float angle_btw_vects(const hv v1,const hv v2)
{
  float dp=vect_dot_prod(v1,v2),lenprod=vect_modulo(v1)*vect_modulo(v2);

  if(lenprod==0.0)
    return 0.0;

  return acosf(dp/lenprod);
}

inline void mat4_unit(hm4 m)
{
  int i;
  
  bzero(m,sizeof(hm4));
  for(i=0;i<4;i++)
    m[i][i]=1.0;
}

inline void mat4_trasl(const hv v,hm4 m)
{
  mat4_unit(m);

  m[3][0]=v[0];
  m[3][1]=v[1];
  m[3][2]=v[2];
}

inline void mat4_scale(const hv v,hm4 m)
{
  mat4_unit(m);
  
  m[0][0]=v[0];
  m[1][1]=v[1];
  m[2][2]=v[2];
}

inline void mat4_shear(const hv v,hm4 m)
{
  mat4_unit(m);
  
  m[0][3]=v[0];
  m[1][3]=v[1];
  m[2][3]=v[2];
}

void mat4_transpose(const hm4 m1,hm4 m2)
{
  int i,j;

  for(i=0;i<4;i++)
    for(j=0;j<4;j++)
      m2[j][i]=m1[i][j];
}

void mat4_mult(const hm4 m1,const hm4 m2,hm4 m3)
{
  int i,j;
  
  for(i=0;i<4;i++)
    for(j=0;j<4;j++)
      m3[i][j]=(m1[i][0]*m2[0][j])+(m1[i][1]*m2[1][j])+(m1[i][2]*m2[2][j])+(m1[i][3]*m2[3][j]);
}

void mat4_mult_only_rotscale(const hm4 m1,const hm4 m2,hm4 m3)
{
  int i,j;

  memcpy(m3,m1,sizeof(hm4));  
  
  for(i=0;i<3;i++)
    for(j=0;j<3;j++)
      m3[i][j]=(m1[i][0]*m2[0][j])+(m1[i][1]*m2[1][j])+(m1[i][2]*m2[2][j]);
}

void mat4_rot_axis(const hv axis,const float ang,hm4 m)
{
  float c=cosf(ang),s=sinf(ang),x=axis[0],y=axis[1],z=axis[2],x2=x*x,y2=y*y,z2=z*z,sum2=x2+y2+z2,sqsum2=sqrtf(sum2);
  
  mat4_unit(m);

  m[0][0]=(x2+(y2+z2)*c)/sum2;
  m[0][1]=(x*y*(1-c)-z*sqsum2*s)/sum2;
  m[0][2]=(x*z*(1-c)+y*sqsum2*s)/sum2;
  m[1][0]=(x*y*(1-c)+z*sqsum2*s)/sum2;
  m[1][1]=(y2+(x2+z2)*c)/sum2;
  m[1][2]=(y*z*(1-c)-x*sqsum2*s)/sum2;
  m[2][0]=(x*z*(1-c)-y*sqsum2*s)/sum2;
  m[2][1]=(y*z*(1-c)+x*sqsum2*s)/sum2;
  m[2][2]=(z2+(x2+y2)*c)/sum2;
}

void mat4_vect_mult(const hm4 m,const hv v1,hv v2)
{
  hv4 v1t,v2t;
  int i;

  v1t[0]=v1[0];
  v1t[1]=v1[1];
  v1t[2]=v1[2];
  v1t[3]=1.0;

  for(i=0;i<4;i++)
    v2t[i]=m[0][i]*v1t[0]+m[1][i]*v1t[1]+m[2][i]*v1t[2]+m[3][i]*v1t[3];

  v2[0]=v2t[0]/v2t[3];
  v2[1]=v2t[1]/v2t[3];
  v2[2]=v2t[2]/v2t[3];
}

/*
 * This one multiplies by the top-left 3X3 mat
 */

void mat4_vect_mult_submat(const hm4 m,const hv v1,hv v2)
{
  int i;
  
  for(i=0;i<3;i++)
    v2[i]=m[i][0]*v1[0]+m[i][1]*v1[1]+m[i][2]*v1[2];
}

void mat4_vect_over_another(const hv v1,const hv v2,hm4 m)
{
  float angle=acosf(vect_dot_prod(v1,v2));
  hv v,cp;

  vect_cross_prod(v1,v2,v);
  vect_normalize(v,cp);
  
  mat4_rot_axis(cp,angle,m);
}

void mat4_lookat(const hv eye,const hv center,const hv in_up,hm4 res)
{
#if 0
  hv v,forw,side,up2;
//  int i;

  vect_sub(eye,center,v);
  vect_normalize(v,forw);
  vect_cross_prod(in_up,forw,v);
  vect_normalize(v,side);
  vect_cross_prod(forw,side,up2);

  mat4_refchange(eye,side,up2,forw,res);
#else
  hv backw,right;
  
  vect_sub(eye,center,backw);
  vect_normalize(backw,backw);

  vect_cross_prod(in_up,backw,right);
  vect_normalize(right,right);
  
  /* lg("ZOLLA in_up %.3f,%.3f,%.3f backw %.3f,%.3f,%.3f right %.3f,%.3f,%.3f ", */
  /*    in_up[0],in_up[1],in_up[2], */
  /*    backw[0],backw[1],backw[2],right[0],right[1],right[2]); */

  hm4 m,trasl;
  hv v;

  mat4_unit(m);

  m[0][0]=right[0];m[0][1]=right[1];m[0][2]=right[2];
  m[1][0]=in_up[0];m[1][1]=in_up[1];m[1][2]=in_up[2];
  m[2][0]=backw[0];m[2][1]=backw[1];m[2][2]=backw[2];

//  m[3][0]=-eye[0];m[3][1]=-eye[1];m[3][2]=-eye[2];

  vect_negate(eye,v);  
  mat4_trasl(v,trasl);
  mat4_mult(trasl,m,res);
#endif
//  mat4_print("ZIZIZI",res);
}

void mat4_lookat_from_quat(const hv eye,const hv4 rot,hm4 res)
{
  hv4 q;
  hv p;

  quat_invert(rot,q);
  vect_negate(eye,p);

  mat4_unit(res);
  
  res[0][0]=1.0-2.0*q[1]*q[1]-2.0*q[2]*q[2];
  res[1][0]=2.0*q[0]*q[1]-2.0*q[2]*q[3];
  res[2][0]=2.0*q[0]*q[2]+2.0*q[1]*q[3];
  res[3][0]=p[0]*res[0][0]+p[1]*res[1][0]+p[2]*res[2][0];
  
  res[0][1]=2.0*q[0]*q[1]+2.0*q[2]*q[3];
  res[1][1]=1.0-2.0*q[0]*q[0]-2.0*q[2]*q[2];
  res[2][1]=2.0*q[1]*q[2]-2.0*q[0]*q[3];
  res[3][1]=p[0]*res[0][1]+p[1]*res[1][1]+p[2]*res[2][1];
  
  res[0][2]=2.0*q[0]*q[2]-2.0*q[1]*q[3];
  res[1][2]=2.0*q[1]*q[2]+2.0*q[0]*q[3];
  res[2][2]=1.0-2.0*q[0]*q[0]-2.0*q[1]*q[1];
  res[3][2]=p[0]*res[0][2]+p[1]*res[1][2]+p[2]*res[2][2];
}

void mat4_refchange(const hv orig,const hv vx,const hv vy,const hv vz,hm4 res)
{
  int i;
  
  mat4_unit(res);

  for(i=0;i<3;i++)
  {
    res[0][i]=vx[i];
    res[1][i]=vy[i];
    res[2][i]=vz[i];
  }
  res[3][0]=-vect_dot_prod(vx,orig);
  res[3][1]=-vect_dot_prod(vy,orig);
  res[3][2]=-vect_dot_prod(vz,orig);
}

void mat4_perspective(const float fovy,const float aspect,const float near,const float far,hm4 res)
{
  float half_yv=tanf(fovy/2.0)/near,half_xv=half_yv*aspect/**ASPECT_CORRECTOR*/;

  bzero(res,sizeof(hm4));

  res[0][0]=near/half_xv;
  res[1][1]=near/half_yv;
  res[2][2]=-(far+near)/(far-near); 
  res[3][2]=-1.0;
  res[2][3]=-(2.0*far*near)/(far-near); 
}

#if 0
void mat4_perspective_ohmd(const float fovy,const float aspect,const float near,const float far,hm4 res)
{
  float half_fov=fovy*0.5,delta=far-near,sine=sinf(half_fov);

  if(delta==0.0f || sine==0.0f || aspect==0.0f)
  {
    mat4_unit(res);
    return;
  }

  float cotangent=cosf(half_fov)/sine;

  bzero(res,sizeof(hm4));

  res[0][0]=cotangent/aspect;
  res[1][1]=cotangent;
  res[2][2]=-(far+near)/delta;
  res[2][3]=-2.0*near*far/delta;
  res[3][2]=-1.0;
}
#endif
/*
cy / sy is cos/sin yaw (heading) 
cp / sp is cos/sin pitch (elevation)
cr / sr is cos/sin roll (bank)
m[0][0] = cy*cp;
m[0][1] = -sy;
m[0][2] = cy*sp;

m[1][0] = (sr*sp) + (cr*cp*sy);
m[1][1] = cr*cy;
m[1][2] = (cr*sy*sp) - (cp*sr);

m[2][0] = (cp*sr*sy) - (cr *sp);
m[2][1] = cy*sr;
m[2][2] = (cr*cp) + (sr*sy*sp);

ypr -> hab

    m00 = ch * ca;
    m10 = sa;
    m20 = -sh*ca;
    m01 = sh*sb - ch*sa*cb;
    m11 = ca*cb;
    m21 = sh*sa*cb + ch*sb;
    m02 = ch*sa*sb + sh*cb;
    m12 = -ca*sb;
    m22 = -sh*sa*sb + ch*cb;


*/  

void mat4_tait_bryan(const hv ypr,hm4 m)
{
  float cy=cosf(ypr[0]),sy=sinf(ypr[0]),cp=cosf(ypr[1]),sp=sinf(ypr[1]),cr=cosf(ypr[2]),sr=sinf(ypr[2]);

  mat4_unit(m);

/*
  ypr
  jih
  
  M[j][j]=sj*ss+cc;
  M[j][i]=cj*sh;
  M[j][k]=sj*cs-sc;
  M[i][j]=sj*sc-cs;
  M[i][i]=cj*ch;
  M[i][k]=sj*cc+ss;
  M[k][j]=cj*si;
  M[k][i]=-sj;
  M[k][k]=cj*ci;
*/

  float cc=cy*cr,cs=cy*sr,sc=sy*cr,ss=sy*sr;
  
  m[0][0]=sp*ss+cc;
  m[0][1]=cp*sr;
  m[0][2]=sp*cs-sc;
  m[1][0]=sp*sc-cs;
  m[1][1]=cp*cr;
  m[1][2]=sp*cc+ss;
  m[2][0]=cp*sy;
  m[2][1]=-sp;
  m[2][2]=cp*cy;  

  /* m[0][0]=cy*cp; */
  /* m[1][0]=sp; */
  /* m[2][0]=-sy*cp; */
  /* m[0][1]=sy*sr-cy*sp*cr; */
  /* m[1][1]=cp*cr; */
  /* m[2][1]=sy*sp*cr+cy*sr; */
  /* m[0][2]=cy*sp*sr+sy*cr; */
  /* m[1][2]=-cp*sr; */
  /* m[2][2]=-sy*sp*sr+cy*cr; */

  /* m[0][0]=cy*cp; */
  /* m[1][0]=-sy; */
  /* m[2][0]=cy*sp; */
  /* m[0][1]=sr*sp+cr*cp*sy; */
  /* m[1][1]=cr*cy; */
  /* m[2][1]=cr*sy*sp-cp*sr; */
  /* m[0][2]=cp*sr*sy-cr*sp; */
  /* m[1][2]=cy*sr; */
  /* m[2][2]=cr*cp+sr*sy*sp; */
}

void mat4_print(char *title,hm4 m)
{
  fprintf(stderr,"<%s>\n%11.5f %11.5f %11.5f %11.5f\n%11.5f %11.5f %11.5f %11.5f\n%11.5f %11.5f %11.5f %11.5f\n%11.5f %11.5f %11.5f %11.5f\n",title,
	  m[0][0],m[0][1],m[0][2],m[0][3],
	  m[1][0],m[1][1],m[1][2],m[1][3],
	  m[2][0],m[2][1],m[2][2],m[2][3],
	  m[3][0],m[3][1],m[3][2],m[3][3]);
}

void quat_unit(hv4 quat)
{
  bzero(quat,sizeof(hv4));
  quat[3]=1.0;
}

inline float quat_dot_prod(const hv4 a,const hv4 b)
{
  return a[0]*b[0]+a[1]*b[1]+a[2]*b[2]+a[3]*b[3];
}

inline float quat_modulo(const hv4 quat)
{
  return sqrtf(quat_dot_prod(quat,quat));
}

void quat_invert(const hv4 qin,hv4 qout)
{
  qout[0]=-qin[0];
  qout[1]=-qin[1];
  qout[2]=-qin[2];
  qout[3]=qin[3];
}

void quat_normalize(hv4 quat)
{
  float m=quat_modulo(quat);

  if(m==0.0)
    return;  

  quat[0]/=m;
  quat[1]/=m;
  quat[2]/=m;
  quat[3]/=m;
}

void quat_quat_mult(const hv4 q1,const hv4 q2,hv4 q3)
{
  q3[0]=q1[0]*q2[3]+q1[3]*q2[0]+q1[1]*q2[2]-q1[2]*q2[1];
  q3[1]=q1[1]*q2[3]+q1[3]*q2[1]+q1[2]*q2[0]-q1[0]*q2[2];
  q3[2]=q1[2]*q2[3]+q1[3]*q2[2]+q1[0]*q2[1]-q1[1]*q2[0];
  q3[3]=q1[3]*q2[3]-q1[0]*q2[0]-q1[1]*q2[1]-q1[2]*q2[2];  
}

/*
 * www.euclideanspace.com uses the same XYZ as me. For yaw, pitch and roll they use
 * heading, bank and attitude, and they invert pitch & roll
 */

void tait_bryan_to_quat(const hv tb,hv4 q)
{
  float y=tb[0]/2.0,p=tb[1]/2.0,r=tb[2]/2.0,sy=sinf(y),sp=sinf(p),sr=sinf(r),cy=cosf(y),cp=cosf(p),cr=cosf(r);
/* #if 0 */
/*   q[0]=sy*cp*sr+cy*sp*cr; */
/*   q[1]=sy*cp*cr-cy*sp*sr; */
/*   q[2]=cy*cp*sr+sy*sp*cr; */
/*   q[3]=cy*cp*cr-sy*sp*sr; */
/* #else */
/* /\* as in wikipedia https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles *\/ */
/*   q[0]=sy*cp*sr+cy*sp*cr; */
/*   q[1]=sy*cp*cr-cy*sp*sr; */
/*   q[2]=cy*cp*sr-sy*sp*cr; */
/*   q[3]=cy*cp*cr+sy*sp*sr; */
/* #endif */

/* from https://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToEuler/ */

   q[0]=cy*sp*cr+sy*cp*sr; 
   q[1]=sy*cp*cr+cy*sp*sr;
   q[2]=cy*cp*sr-sy*sp*cr;
   q[3]=cy*cp*cr-sy*sp*sr;
}

inline void quat_to_tait_bryan(const hv4 quat,hv tb)
{
  
/* #if 0   */
/*   tb[0]=atan2f(2.0*quat[1]*quat[3]-2.0*quat[2]*quat[0],1.0-2.0*y2-2.0*x2); */
/*   tb[1]=asinf(2.0*quat[2]*quat[1]+2.0*quat[0]*quat[3]); */
/*   tb[2]=-atan2f(2.0*quat[2]*quat[3]-2.0*quat[1]*quat[0],1.0-2.0*z2-2.0*x2); */
/* #else */
/* /\* as in wikipedia https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles *\/ */
/*   tb[0]=atan2f(2.0*(quat[1]*quat[3]+quat[0]*quat[2]),1.0-2.0*(x2+y2)); */
/*   tb[1]=asinf(2.0*(quat[3]*quat[0]-quat[1]*quat[2])); */
/*   tb[2]=atan2f(2.0*(quat[2]*quat[3]+quat[0]*quat[1]),1.0-2.0*(x2+z2)); */
/* #endif */

/* from https://www.euclideanspace.com/maths/geometry/rotations/conversions/eulerToQuaternion/ */
  float test=quat[0]*quat[1]+quat[2]*quat[3];
  
  if(test>0.499) // singularity at north pole
  {
    tb[0]=2.0*atan2f(quat[0],quat[3]);
    tb[1]=0.0;
    tb[2]=M_PI_2;
    return;
  }
  if(test<-0.499) // singularity at south pole
  {
    tb[0]=-2.0*atan2f(quat[0],quat[3]);
    tb[1]=0.0;
    tb[2]=-M_PI_2;
    return;
  }

  float x2=quat[0]*quat[0],y2=quat[1]*quat[1],z2=quat[2]*quat[2];
  
  tb[0]=atan2f(2.0*(quat[1]*quat[3]-quat[0]*quat[2]),1.0-2.0*(y2+z2)); 
  tb[1]=atan2f(2.0*(quat[0]*quat[3]-quat[1]*quat[2]),1.0-2.0*(x2-z2)); 
  tb[2]=asinf(2.0*(quat[0]*quat[1]+quat[2]*quat[3]));
}

void quat_to_hm4(const hv4 q,hm4 m)
{
  bzero(m,sizeof(hm4));
  m[3][3]=1.0;

  float sqq[3],
    dxy=2.0*q[0]*q[1],dxz=2.0*q[0]*q[2],dxw=2.0*q[0]*q[3],
    dyz=2.0*q[1]*q[2],dyw=2.0*q[1]*q[3],
    dzw=2.0*q[2]*q[3];
  int i;
  
  for(i=0;i<3;i++)
    sqq[i]=2.0*q[i]*q[i];

  m[0][0]=1.0-sqq[1]-sqq[2];
  m[1][0]=dxy-dzw;
  m[2][0]=dxz+dyw;

  m[0][1]=dxy+dzw;
  m[1][1]=1.0-sqq[0]-sqq[2];
  m[2][1]=dyz-dxw;

  m[0][2]=dxz-dyw;
  m[1][2]=dyz+dxw;
  m[2][2]=1.0-sqq[0]-sqq[1];
}

void hm4_to_quat(const hm4 m,hv4 q)
{
  q[3]=sqrtf(1.0+m[0][0]+m[1][1]+m[2][2])/2.0;

  float w4=q[3]*4.0;

  q[0]=(m[2][1]-m[1][2])/w4;
  q[1]=(m[0][2]-m[2][0])/w4;
  q[2]=(m[1][0]-m[0][1])/w4;
}

void quat_from_vect_angle(const hv v,const float angle,hv4 q)
{
  hv vnorm={0.0,0.0,0.0};
  float halfsin=sinf(angle/2.0);

  vect_normalize(v,vnorm);
  
  q[0]=vnorm[0]*halfsin;
  q[1]=vnorm[1]*halfsin;
  q[2]=vnorm[2]*halfsin;
  q[3]=cosf(angle/2.0);
}

void rot_vect_by_quat(const hv4 q,const hv vin,hv vout)
{
#if 0
  float n1=q[0]*2.0,n2=q[1]*2.0,n3=q[2]*2.0;
  float n4=q[0]*n1,n5=q[1]*n2,n6=q[2]*n3;
  float n7=q[0]*n2,n8=q[0]*n3,n9=q[1]*n3;
  float n10=q[3]*n1,n11=q[3]*n2,n12=q[3]*n3;
  
  vout[0]=(1.0-(n5+n6))*vin[0]+(n7-n12)*vin[1]+(n8+n11)*vin[2];
  vout[1]=(n7+n12)*vin[0]+(1.0-(n4+n6))*vin[1]+(n9-n10)*vin[2];
  vout[2]=(n8-n11)*vin[0]+(n9+n10)*vin[1]+(1.0-(n4+n5))*vin[2];
#endif
  hv v1,v2;
  int i;
  
  vect_cross_prod(q,vin,v1);
  for(i=0;i<3;i++)
    v1[i]+=vin[i]*q[3];
  vect_cross_prod(q,v1,v2);
  for(i=0;i<3;i++)
    vout[i]=vin[i]+2*v2[i];
}

void quat_vect_over_another(const hv v1,const hv v2,hv4 q)
{
  float angle=acosf(vect_dot_prod(v1,v2));
  hv v,cp;

  vect_cross_prod(v1,v2,v);
  vect_normalize(v,cp);

  quat_from_vect_angle(cp,angle,q);
}

float point_distance(const hv p1,const hv p2)
{
  hv dist;

  vect_sub(p2,p1,dist);

  return vect_modulo(dist);
}

void three_points_to_plane(const hv p1,const hv p2,const hv p3,hv4 coef)
{
  hv v1,v2,v3;

  vect_sub(p2,p1,v1);
  vect_sub(p3,p1,v2);
  vect_cross_prod(v1,v2,v3);

  coef[0]=v3[0];
  coef[1]=v3[1];
  coef[2]=v3[2];
  coef[3]=vect_dot_prod(v3,p3);
}

/*   /\** */
/*  * Determines the point of intersection between a plane defined by a point and a normal vector and a line defined by a point and a direction vector. */
/*  * */
/*  * @param planePoint    A point on the plane. */
/*  * @param planeNormal   The normal vector of the plane. */
/*  * @param linePoint     A point on the line. */
/*  * @param lineDirection The direction vector of the line. */
/*  * @return The point of intersection between the line and the plane, null if the line is parallel to the plane. */
/*  *\/ */
/* public static Vector lineIntersection(Vector planePoint, Vector planeNormal, Vector linePoint, Vector lineDirection) { */
/*     if (planeNormal.dot(lineDirection.normalize()) == 0) { */
/*         return null; */
/*     } */

/*     double t = (planeNormal.dot(planePoint) - planeNormal.dot(linePoint)) / planeNormal.dot(lineDirection.normalize()); */
/*     return linePoint.plus(lineDirection.normalize().scale(t)); */
/* } */

int line_plane_intersect(const hv plane_point,const hv plane_normal,const hv line_point,const hv line_dir,hv intersect,float *fact)
{
  float dp1=vect_dot_prod(plane_normal,line_dir);
  
  if(dp1==0.0)
    return 0;

#if 0  
  float t=(vect_dot_prod(plane_normal,plane_point)-vect_dot_prod(plane_normal,line_point))/dp1;

  vect_mult_const(line_dir,t,intersect);
  vect_add(intersect,line_point,intersect);

  (*fact)=t;
  
#else
  hv p1;

  vect_sub(plane_point,line_point,p1);

  float u=vect_dot_prod(plane_normal,p1)/dp1;

  vect_mult_const(line_dir,u,intersect);
  vect_add(intersect,line_point,intersect);

  (*fact)=u;
#endif
  return 1;
}

void triangle_angles(const hv p1,const hv p2,const hv p3,float *a1,float *a2,float *a3)
{
  float l1=point_distance(p1,p2),l2=point_distance(p2,p3),l3=point_distance(p3,p1),
    l1sq=l1*l1,l2sq=l2*l2,l3sq=l3*l3;

//     a2 = b2 + c2 - 2bc cos A
// 2bc cos A = b2+c2-a2
//     b2 = a2 + c2 - 2ac cos B

  (*a1)=acosf((l2sq+l3sq-l1sq)/(2.0*l2*l3));
  (*a2)=acosf((l1sq+l3sq-l2sq)/(2.0*l1*l3));
  (*a3)=acosf((l1sq+l2sq-l3sq)/(2.0*l1*l2));
//  (*a3)=M_PI-(*a1)-(*a2);
}

/*
 * The resulting angle when given yaw/pitch from and to
 */

float yaw_pitch_angle(const float yaw_from,const float pitch_from,const float yaw_to,const float pitch_to)
{
  hv v,p1,p2;
  hm4 m;

  v[0]=yaw_from;
  v[1]=pitch_from;
  v[2]=0.0;
  mat4_tait_bryan(v,m);
  mat4_vect_mult(m,forw_v,p1);
  
  v[0]=yaw_to;
  v[1]=pitch_to;
  mat4_tait_bryan(v,m);
  mat4_vect_mult(m,forw_v,p2);

  return angle_btw_vects(p1,p2);
}

/*
 * Angular velocity
 * https://www.euclideanspace.com/physics/kinematics/angularvelocity/
 */

void velocity(const hv lin1,const hv ang1,const hv lin2,const hv ang2,const float time,float *lin_vel,float *ang_vel)
{
  hv diff,v;

  vect_sub(lin2,lin1,diff);
  (*lin_vel)=vect_normalize(diff,v)/time;
  vect_sub(ang2,ang1,diff);
  (*ang_vel)=vect_normalize(diff,v)/time;
}

/*
 * Interpolate a time series to a constant interval
 */

static int tp_cmp(const void *p1,const void *p2)
{
  timepoint_stc *d1=(timepoint_stc *)p1,*d2=(timepoint_stc *)p2;

  if(d1->msec<d2->msec)
    return -1;
  if(d1->msec>d2->msec)
    return 1;

  return 0;
}

void interpolate_series(const timepoints_stc *tpin,const unsigned int interval,timepoints_stc *tpout)
{
  timepoint_stc *tploc=malloc(sizeof(timepoint_stc)*tpin->n_points);

  memcpy(tploc,tpin->tp,sizeof(timepoint_stc)*tpin->n_points);
  qsort(tploc,tpin->n_points,sizeof(timepoint_stc),tp_cmp);

  int firsttime=tploc[0].msec-(tploc[0].msec%interval)+interval,lasttime=tploc[tpin->n_points-1].msec-(tploc[tpin->n_points-1].msec%interval);

  tpout->n_values=tpin->n_values;

  if((lasttime-firsttime)<interval)
  {
    lg("%s: Sorry: interval too short (%d)",__func__,lasttime-firsttime);
    tpout->n_points=0;
    free(tploc);
    return;
  }  

  tpout->n_points=(lasttime-firsttime)/interval;
  tpout->tp=malloc(sizeof(timepoint_stc)*tpout->n_points);

  timepoint_stc *pin=tploc+1,*pout=tpout->tp;
  int i,j,tacc;
  float fact;

  for(tacc=firsttime,i=0;i<tpout->n_points;i++,tacc+=interval,pout++)
  {
    pout->values=malloc(sizeof(float)*tpin->n_values);
    pout->msec=tacc;
    while(pin->msec<tacc)
      pin++;
    fact=(float)(tacc-(pin-1)->msec)/(float)(pin->msec-(pin-1)->msec);
    
    for(j=0;j<tpin->n_values;j++)
      pout->values[j]=(pin-1)->values[j]*(1.0-fact)+pin->values[j]*fact;
  }

  free(tploc);
}

void random_reorder(const int n,int reordered[n])
{
  int *loc=malloc(sizeof(int)*n),i,randv;

  for(i=0;i<n;i++)
    loc[i]=i;
  
  for(i=0;i<n;i++)
  {
    randv=lrand48()%(n-i);
    reordered[i]=loc[randv];
    memmove(loc+randv,loc+(randv+1),sizeof(int)*(n-i-randv-1));
  }

  free(loc);  
}

/*
 * Simple lowpass filter
 * (see https://kiritchatterjee.wordpress.com/2014/11/10/a-simple-digital-low-pass-filter-in-c/)
 * LPF: Y(n) = (1-ß)*Y(n-1) + (ß*X(n))) = Y(n-1) - (ß*(Y(n-1)-X(n)))
 *
 * First initialize a maths_lowpass_stc with beta = from 0 to 1, and smooth_data to 0
 * Then call this function with every new value. Smooth_data will contain the value to use (which is also returned)
 */

inline float lowpass_next(maths_lowpass_stc *s,float raw_value)
{
  s->smooth_data+=(s->beta*(raw_value-s->smooth_data));
  return s->smooth_data;
}

