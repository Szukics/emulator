/* udplistener.c */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================
 *
 * Code for a generic udp socket listener
 */

#include <maths_include.h>
#include "moo.h"
#include <pthread.h>
#include <poll.h>
#include <stdlib.h>
#include <sys/syscall.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <sys/timerfd.h>
#include <ifaddrs.h>
#include <netdb.h>

#define POLL_TIMEOUT 350
#define BFR_SIZE 1024

static void free_moo_udplistener(void *p);
static void *thr(void *arg);

typedef struct udplistener_event
{
  struct timespec reftime;
  int pkt_len,port;
  uint32_t addr;
  uint8_t *pkt;
  struct udplistener_event *next;
} udplistener_event_stc;

typedef struct
{
  char *loc_addr/*,*rem_addr*/;
  int skt,inport,outport;

  udplistener_event_stc *events;

  pthread_t thr;
  pthread_mutex_t mtx;
  uint8_t leave_thread;
} moo_udplistener_stc;

extern VALUE cls_moo_udplistener;

VALUE new_moo_udplistener(VALUE self,VALUE v_loc_addr,VALUE v_portin)
{
  moo_udplistener_stc *s;
  VALUE sdata=Data_Make_Struct(cls_moo_udplistener,moo_udplistener_stc,NULL,free_moo_udplistener,s),v;

  bzero(s,sizeof(moo_udplistener_stc));

  s->loc_addr=strdup(RSTRING_PTR(v_loc_addr));  
  s->inport=FIX2INT(v_portin);

  s->skt=socket(AF_INET,SOCK_DGRAM,0);
  if(s->skt<0)
    rb_raise(rb_eArgError,"%s: Cannot open socket (%s)!",__func__,strerror(errno));

  struct sockaddr_in addr;

  bzero(&addr,sizeof(struct sockaddr_in));
  addr.sin_family=AF_INET;
  addr.sin_port=htons(s->inport);
  addr.sin_addr.s_addr=inet_addr(s->loc_addr);
  if(bind(s->skt,(struct sockaddr *)&addr,sizeof(addr))!=0)
    rb_raise(rb_eArgError,"%s: Cannot bind socket to addr %s (%s)!",__func__,s->loc_addr,strerror(errno));
  
/*
 * The thread
 */

  int sts;
  
  sts=pthread_mutex_init(&s->mtx,NULL);
  if(sts)
    rb_raise(rb_eArgError,"%s: error #%d creating mutex (%s)",__func__,sts,strerror(errno));

  s->leave_thread=0; 

  sts=pthread_create(&s->thr,NULL,thr,s);
  if(sts)
    rb_raise(rb_eArgError,"%s: error #%d starting thread (%s)",__func__,sts,strerror(errno));

  return sdata;
}

static void free_moo_udplistener(void *p)
{
  moo_udplistener_stc *s=(moo_udplistener_stc *)p;
  
  s->leave_thread=1;
  pthread_join(s->thr,NULL);

  close(s->skt);
  free(s->loc_addr);

  free(p);
}

VALUE moo_udplistener_sendto(VALUE self,VALUE v_addr,VALUE v_port,VALUE v_pkt)
{
  moo_udplistener_stc *s;  
  Data_Get_Struct(self,moo_udplistener_stc,s);

  struct sockaddr_in addr;
  int res;
  char *addrstr=RSTRING_PTR(v_addr);
  
  bzero(&addr,sizeof(struct sockaddr_in));
  addr.sin_family=AF_INET; 
  res=inet_aton(addrstr,&addr.sin_addr);
  if(!res)
    rb_raise(rb_eArgError,"%s: Cannot get address from %s!",__func__,addrstr);
  addr.sin_port=htons(FIX2INT(v_port));

  res=sendto(s->skt,RSTRING_PTR(v_pkt),RSTRING_LEN(v_pkt),0,&addr,sizeof(struct sockaddr_in));
  if(res!=RSTRING_LEN(v_pkt))
    rb_raise(rb_eArgError,"%s: Cannot send pkt (%s)",__func__,strerror(errno));
  
  return self;
}

VALUE moo_udplistener_poll(VALUE self)
{
  moo_udplistener_stc *s;  
  Data_Get_Struct(self,moo_udplistener_stc,s);

  if(!s->events)
    return Qfalse;

  udplistener_event_stc *evq,*evp1,*evp2;

  pthread_mutex_lock(&s->mtx);
  evq=s->events;
  s->events=NULL;
  pthread_mutex_unlock(&s->mtx);

  VALUE to_ret=rb_ary_new(),v;
  char bfr[256];

  for(evp1=evq;evp1;)
  {
    evp2=evp1;
    evp1=evp2->next;

    v=rb_ary_new_capa(4);

    rb_ary_store(v,0,INT2FIX(evp2->port));
    sprintf(bfr,"%d.%d.%d.%d",(evp2->addr>>24)&0xff,(evp2->addr>>16)&0xff,(evp2->addr>>8)&0xff,evp2->addr&0xff);
    rb_ary_store(v,1,rb_str_new_cstr(bfr));
    rb_ary_store(v,2,rb_str_new((char *)evp2->pkt,evp2->pkt_len));    
    rb_ary_store(v,3,rb_str_new((char *)&evp2->reftime,sizeof(struct timespec)));    
    rb_ary_push(to_ret,v);
		 
    free(evp2->pkt);
    free(evp2);
  }    

  return to_ret;
}

VALUE moo_udplistener_ping(VALUE self,VALUE v_loc_addr,VALUE v_dest_port,VALUE v_payload)
{
  char *loc_addr=RSTRING_PTR(v_loc_addr);
  int dest_port=FIX2INT(v_dest_port),cnt;

  struct ifaddrs *ni,*nip,*ia=NULL;
  char host[NI_MAXHOST],bcast[NI_MAXHOST];
  struct sockaddr_in sa;

  if(getifaddrs(&ni)!=0)
    rb_raise(rb_eArgError,"%s: Cannot get if addrs (%s)!",__func__,strerror(errno));

  for(cnt=0,nip=ni;nip;nip=nip->ifa_next,cnt++)
  {
    if(!nip->ifa_addr || nip->ifa_addr->sa_family!=AF_INET)
      continue;

    getnameinfo(nip->ifa_addr,sizeof(struct sockaddr_in),host,NI_MAXHOST,NULL,0,NI_NUMERICHOST);

    if(strcmp(host,loc_addr))
      continue;

    memcpy(&sa,nip->ifa_addr,sizeof(struct sockaddr_in));
    sa.sin_addr.s_addr|=0xffffffff^((struct sockaddr_in *)nip->ifa_netmask)->sin_addr.s_addr;    
      
    getnameinfo((struct sockaddr *)(&sa),sizeof(struct sockaddr_in),bcast,NI_MAXHOST,NULL,0,NI_NUMERICHOST);
    
    ia=malloc(sizeof(struct ifaddrs));
    memcpy(ia,nip,sizeof(struct ifaddrs));
    break;
  }
  freeifaddrs(ni);

  if(!ia)
    return Qnil;

  int skt=socket(AF_INET,SOCK_DGRAM,0);
  int bc=1;
  
  if(skt<0)
    rb_raise(rb_eArgError,"%s: Cannot open socket (%s)!",__func__,strerror(errno));
  if(setsockopt(skt,SOL_SOCKET,SO_BROADCAST,&bc,sizeof(int))<0)
    rb_raise(rb_eArgError,"%s: Cannot set socket to bcast (%s)!",__func__,strerror(errno));
  
  struct sockaddr_in addr;
  
  bzero(&addr,sizeof(struct sockaddr_in));
  addr.sin_family=AF_INET;
  addr.sin_port=htons(0);
  addr.sin_addr.s_addr=inet_addr(loc_addr);

  if(bind(skt,(struct sockaddr *)&addr,sizeof(addr))!=0)
    rb_raise(rb_eArgError,"%s: Cannot bind socket to addr %s (%s)!",__func__,loc_addr,strerror(errno));

  bzero(&addr,sizeof(struct sockaddr_in));
  addr.sin_family=AF_INET;
  addr.sin_port=htons(dest_port);
  addr.sin_addr.s_addr=inet_addr(bcast);

  if(sendto(skt,RSTRING_PTR(v_payload),RSTRING_LEN(v_payload),0,&addr,sizeof(struct sockaddr_in))<RSTRING_LEN(v_payload))
    rb_raise(rb_eArgError,"%s: Cannot send payload from addr %s (%s)!",__func__,loc_addr,strerror(errno));

  close(skt);

  return self;  
}

static void *thr(void *arg)
{
  lg("UDPLISTENER THR HAS PID %d",syscall(SYS_gettid));

  moo_udplistener_stc *s=(moo_udplistener_stc *)arg;
  struct pollfd pfd={s->skt,POLLIN,0};
  uint8_t bfr[BFR_SIZE];
  int res;
  udplistener_event_stc *ev,*evp;
  struct sockaddr_in src_addr;
  socklen_t addrlen;
  
  while(!s->leave_thread)
  {
    pfd.events=POLLIN;
    res=poll(&pfd,1,POLL_TIMEOUT);

    if(res<0)
    {
      lg("%s/%d: Error in poll (%s)",s->loc_addr,s->inport,strerror(errno));
      break;
    }
    
    if(res>0)
    {
      addrlen=sizeof(struct sockaddr_in);
      
      res=recvfrom(s->skt,bfr,BFR_SIZE,0,&src_addr,&addrlen);
      if(res<0)
      {
	lg("WARINIG! Udplistener stopped (%s)",strerror(errno));	
	break;
      }
      ev=malloc(sizeof(udplistener_event_stc));
      clock_gettime(CLOCK_MONOTONIC,&ev->reftime);
      ev->pkt_len=res;
      ev->pkt=malloc(res);
      memcpy(ev->pkt,bfr,res);
      ev->port=src_addr.sin_port;
      ev->addr=htonl(src_addr.sin_addr.s_addr);      
      ev->next=NULL;
      
      pthread_mutex_lock(&s->mtx);
      if(!s->events)
	s->events=ev;
      else
      {
	for(evp=s->events;evp->next;evp=evp->next)
	  ;
	evp->next=ev;
      }
      pthread_mutex_unlock(&s->mtx);
    }
  }
  
  lg("Udplistener thr out.");
    
  return NULL;
}

    

    
  
