/* server.c */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================
 *
 * Code for the thread that manages the communication with either the hardware Moog platform or the emulator.
 */

#include <carlobase.h>
#include <maths_include.h>
#include "moo.h"
#include <pthread.h>
#include <poll.h>
#include <stdlib.h>
#include <sys/syscall.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <sys/timerfd.h>

#define POLL_TIMEOUT 350
#define BFR_SIZE 1024
#define NSEC_NAP 400000 //1200000

typedef struct
{
  float cur_length;
  uint8_t fail_flg;
} moo_srv_actu_stc;

typedef struct
{
  hv_d lin,rot;
} moo_srv_dof_stc;

typedef struct
{
  moo_srv_dof_stc dof_from,dof_to,dof_cur;
  struct timespec start;
  double length;
  moo_algo_type algo;
  union
  {
    maths_accsin_stc *accsin;
    maths_dho_stc *dho;
  } u;
  float exts_at_begin[MOO_N_ACTU];
} moo_srv_action_stc;

typedef struct
{
  hv lin,rot;
  float extensions[MOO_N_ACTU];
} moo_srv_tosendout_stc;

typedef struct
{
  char *logpath;
  int logunit_in,logunit_out;
  
  moo_mode_enum op_mode;
  
  moo_srvr_state_enum state;
  struct timespec state_change;
  
  char *loc_addr,*rem_addr;
  int inport,outport,skt;

  uint32_t datapacket[8];
  
  moo_srv_action_stc *action;
  uint8_t action_changed;
  moo_srv_dof_stc dof_last_good;

  moo_srv_tosendout_stc tosendout,q;
  
  uint8_t newly_rcvd_flg,closed_flg,first_loop_flg,q_flg;
  response_datapacket_stc response;

  platf_stc p;
  moo_srv_actu_stc actu[MOO_N_ACTU];

  float dofs_from_moog[6];

  pthread_t thr;
  pthread_mutex_t mtx;
  uint8_t leave_thread;

  maths_casteljau_stc interpolator;
} moo_server_stc;

static const int actu_order[MOO_N_ACTU]={3,4,5,0,1,2};
static const hv platf_center={0.0,0.0,0.0};

extern VALUE cls_moo_server;
extern VALUE moo_response_parser(VALUE self,VALUE v_bfr);
extern void build_platform(float *bv,float *bvr,hv rotcenter,platf_stc *p);
extern void modify_platform_rotcenter(platf_stc *p,hv rotcenter);
extern void apply_motion(platf_stc *p);

static void free_moo_server(void *p);
static void change_state(moo_server_stc *s,moo_srvr_state_enum new_state);
static int prepare_packet_state(moo_server_stc *s);
static void prepare_packet_body(moo_server_stc *s);
static void *thr(void *arg);
static void srvr_shutdown(moo_server_stc *s);
static void get_actu_lengths(platf_stc *p,const hv ypr,const hv pos,float lengths[MOO_N_ACTU]);
static void fill_default_idle_packet(moo_server_stc *s);
//static void dump_packet(moo_server_stc *s);

VALUE new_moo_server(VALUE self,VALUE v_lower_joints,VALUE v_upper_joints,VALUE v_rotcenter,VALUE v_loc_addr,VALUE v_rem_addr,
		     VALUE v_inport,VALUE v_outport,VALUE v_logpath)
{
  moo_server_stc *s;
  VALUE sdata=Data_Make_Struct(cls_moo_server,moo_server_stc,NULL,free_moo_server,s),v;
  hv rotcenter;
  int i;
  lg("========> START SERVER");
  
  memcpy(rotcenter,RSTRING_PTR(v_rotcenter),sizeof(hv));

  if(v_logpath!=Qnil)
  {
    s->logpath=strdup(RSTRING_PTR(v_logpath));

    char *bfr=malloc(strlen(s->logpath)+9);

    sprintf(bfr,"%s.raw_in",s->logpath);
    s->logunit_in=open(bfr,O_WRONLY|O_CREAT,0660);

    if(s->logunit_in<=0)
    {
      lg("Server: Error opening log file %s (%s)",bfr,strerror(errno));
      abort();
    }

    sprintf(bfr,"%s.raw_out",s->logpath);
    s->logunit_out=open(bfr,O_WRONLY|O_CREAT,0660);

    if(s->logunit_out<=0)
    {
      lg("Server: Error opening log file %s (%s)",bfr,strerror(errno));
      abort();
    }
  }

  s->interpolator.n_steps=6;
  s->interpolator.steps=malloc(sizeof(double)*12);
  s->interpolator.steps[0]=s->interpolator.steps[1]=s->interpolator.steps[2]=0.0;
  s->interpolator.steps[3]=s->interpolator.steps[4]=s->interpolator.steps[5]=1.0;  

  for(i=0;i<MOO_N_ACTU;i++)
    s->actu[i].cur_length=MOO_ACTU_MIN_MM+MOO_ACTU_FIXED_MM+1;
  s->dof_last_good.lin[0]=MOO_LX_PARK;
  s->dof_last_good.lin[1]=MOO_LY_PARK;
  s->dof_last_good.lin[2]=MOO_LZ_PARK;
  s->dof_last_good.rot[0]=MOO_YAW_PARK;
  s->dof_last_good.rot[1]=MOO_PITCH_PARK;
  s->dof_last_good.rot[2]=MOO_ROLL_PARK;

/*
 * build platform model
 */

  build_platform((float *)RSTRING_PTR(v_lower_joints),(float *)RSTRING_PTR(v_upper_joints),rotcenter,&s->p);

  s->loc_addr=strdup(RSTRING_PTR(v_loc_addr));
  s->rem_addr=strdup(RSTRING_PTR(v_rem_addr));
  s->inport=FIX2INT(v_inport);
  s->outport=FIX2INT(v_outport);
  
  s->skt=socket(AF_INET,SOCK_DGRAM,0);
  if(s->skt<0)
    rb_raise(rb_eArgError,"%s: Cannot open socket (%s)!",__func__,strerror(errno));

  i=IP_PMTUDISC_DONT;
  if(setsockopt(s->skt,IPPROTO_IP,IP_MTU_DISCOVER,&i,sizeof(i))!=0)
    rb_raise(rb_eArgError,"%s: Cannot setsockopt (%s)!",__func__,strerror(errno));

  socklen_t sl=sizeof(i);
  getsockopt(s->skt,SOL_SOCKET,SO_PRIORITY,&i,&sl);
  lg("Sock prio is %d (%d) trying to set to 15",i,sl);
  i=15;
  if(setsockopt(s->skt,SOL_SOCKET,SO_PRIORITY,&i,sizeof(i))!=0)
    lg("%s: Sorry! Cannot set socket priority to %d (%s)!",__func__,i,strerror(errno));
  
  getsockopt(s->skt,SOL_SOCKET,SO_PRIORITY,&i,&sl);
  lg("Sock prio is now %d",i);
  
  struct sockaddr_in addr;

  bzero(&addr,sizeof(struct sockaddr_in));
  addr.sin_family=AF_INET;
  addr.sin_port=htons(s->inport);
  addr.sin_addr.s_addr=inet_addr(s->loc_addr);
  if(bind(s->skt,(struct sockaddr *)&addr,sizeof(addr))!=0)
    rb_raise(rb_eArgError,"%s: Cannot bind socket to addr %s (%s)!",__func__,s->loc_addr,strerror(errno));

  addr.sin_port=htons(s->outport);
  addr.sin_addr.s_addr=inet_addr(s->rem_addr);
  if(connect(s->skt,(struct sockaddr *)&addr,sizeof(addr))!=0)
    rb_raise(rb_eArgError,"%s: Cannot connect socket to %s/%d,%d (%s)!",__func__,s->rem_addr,s->inport,s->outport,strerror(errno));

  lg("*************************** I HAVE OPENED THE LISTENER on port %d",s->skt);
  
  s->first_loop_flg=1;

/*
 * The thread
 */

  int sts;
  
  sts=pthread_mutex_init(&s->mtx,NULL);
  if(sts)
    rb_raise(rb_eArgError,"%s: error #%d creating mutex (%s)",__func__,sts,strerror(errno));

  s->leave_thread=0; 

  sts=pthread_create(&s->thr,NULL,thr,s);
  if(sts)
    rb_raise(rb_eArgError,"%s: error #%d starting thread (%s)",__func__,sts,strerror(errno));

  change_state(s,MOO_SRVR_STATE_IDLE);

  return sdata;
}

static void free_moo_server(void *p)
{
  moo_server_stc *s=(moo_server_stc *)p;

  srvr_shutdown(s);

  free(p);
}

VALUE moo_server_close(VALUE self)
{
  moo_server_stc *s;  
  Data_Get_Struct(self,moo_server_stc,s);

  srvr_shutdown(s);

  return self;
}

VALUE moo_server_feed_action(VALUE self,VALUE v_dof,VALUE v_time,VALUE v_algo,VALUE v_override)
{
  moo_server_stc *s;  
  Data_Get_Struct(self,moo_server_stc,s);

  if(v_override==Qfalse && s->action)
  {
    lg("Server: Warning! trying to feed action but action underway (use override!)");
    return Qfalse;
  }

  int i;
  
  pthread_mutex_lock(&s->mtx);

  s->action=realloc(s->action,sizeof(moo_srv_action_stc));

  memcpy(&s->action->dof_from,&s->dof_last_good,sizeof(moo_srv_dof_stc));

  s->action->dof_to.lin[0]=NUM2DBL(rb_ary_entry(v_dof,0));
  s->action->dof_to.lin[1]=NUM2DBL(rb_ary_entry(v_dof,1));
  s->action->dof_to.lin[2]=NUM2DBL(rb_ary_entry(v_dof,2));
  
  s->action->dof_to.rot[0]=NUM2DBL(rb_ary_entry(v_dof,3));
  s->action->dof_to.rot[1]=NUM2DBL(rb_ary_entry(v_dof,4));
  s->action->dof_to.rot[2]=NUM2DBL(rb_ary_entry(v_dof,5));
  
  s->action->algo=FIX2INT(v_algo);
  if(s->action->algo==ALG_SINEACC)
  {
    Data_Get_Struct(v_time,maths_accsin_stc,s->action->u.accsin);
    s->action->length=s->action->u.accsin->time*1000000.0;
  }
  else if(s->action->algo==ALG_DAMPEDOSC)
  {
    TypedData_Get_Struct(v_time,maths_dho_stc,&maths_dho_dtype,s->action->u.dho);
    s->action->length=(s->action->u.dho->t_step*s->action->u.dho->n_steps)*1000000.0;
  }
  else
    s->action->length=NUM2DBL(v_time)*1000.0;

//  lg("FA lin %8.4f,%8.4f,%8.4f rot %8.4f,%8.4f,%8.4f len %8.4f",
//     s->action->dof_to.lin[0],s->action->dof_to.lin[1],s->action->dof_to.lin[2],
//     s->action->dof_to.rot[0],s->action->dof_to.rot[1],s->action->dof_to.rot[2],
//     s->action->length);

  clock_gettime(CLOCK_MONOTONIC,&s->action->start);

  s->action_changed=1;

  pthread_mutex_unlock(&s->mtx);

  return Qtrue;
}

/*
 * Used by joystick
 */

VALUE moo_server_feed_action_speed(VALUE self,VALUE v_dof,VALUE v_speed)
{
  moo_server_stc *s;  
  Data_Get_Struct(self,moo_server_stc,s);

  if(s->action)
  {
    VALUE to_ret=rb_ary_new_capa(1);
      
    rb_ary_store(to_ret,0,rb_str_new_cstr("B"));    
    return to_ret;
  }  
  
  pthread_mutex_lock(&s->mtx);

  hv cur_lin,cur_rot,lin,rot;
  int i;
  float speed=NUM2DBL(v_speed),cur_lengths[MOO_N_ACTU],lengths[MOO_N_ACTU];

  for(i=0;i<3;i++)
  {
    cur_lin[i]=s->dof_last_good.lin[i];
    cur_rot[i]=s->dof_last_good.rot[i];
    
    lin[i]=NUM2DBL(rb_ary_entry(v_dof,i))*1000.0;
    rot[i]=NUM2DBL(rb_ary_entry(v_dof,i+3));
  }

  get_actu_lengths(&s->p,cur_rot,cur_lin,cur_lengths);
  get_actu_lengths(&s->p,rot,lin,lengths);

  float diff,max_diff=0;
  
  for(i=0;i<MOO_N_ACTU;i++)
  {
    if(lengths[i]<=MOO_ACTU_MIN_MM+MOO_ACTU_FIXED_MM || lengths[i]>=MOO_ACTU_MAX_MM+MOO_ACTU_FIXED_MM)
    {
      pthread_mutex_unlock(&s->mtx);
      VALUE to_ret=rb_ary_new_capa(3);
      
      rb_ary_store(to_ret,0,rb_str_new_cstr("A"));
      rb_ary_store(to_ret,1,INT2FIX(i));
      rb_ary_store(to_ret,2,DBL2NUM(lengths[i]));
      
      return to_ret;
    }
    diff=fabsf(cur_lengths[i]-lengths[i]);
    if(diff>max_diff)
      max_diff=diff;
  }

  s->action=realloc(s->action,sizeof(moo_srv_action_stc));
  memcpy(&s->action->dof_from,&s->dof_last_good,sizeof(moo_srv_dof_stc));
  
  s->action->algo=ALG_LIN;
  clock_gettime(CLOCK_MONOTONIC,&s->action->start);
  memcpy(&s->action->dof_from,&s->dof_last_good,sizeof(moo_srv_dof_stc));
  
  s->action->length=(double)(max_diff/speed)*1000.0;
//  lg("MAXDIFF %f LEN %f",max_diff,s->action->length);

  for(i=0;i<3;i++)
  {
    s->action->dof_to.lin[i]=lin[i];
    s->action->dof_to.rot[i]=rot[i];
  }

  s->action_changed=1;
  pthread_mutex_unlock(&s->mtx);

  return Qtrue;
}

VALUE moo_server_feed_immediate(VALUE self,VALUE v_dof,VALUE v_max_lin,VALUE v_max_ang)
{
  moo_server_stc *s;  
  Data_Get_Struct(self,moo_server_stc,s);
  
  hv lin,rot;
  float *dofv=(float *)RSTRING_PTR(v_dof),max_lin=NUM2DBL(v_max_lin),max_ang=NUM2DBL(v_max_ang);

  moogidea2serveridea(dofv,lin,rot);
  
  float lengths[MOO_N_ACTU];

  get_actu_lengths(&s->p,rot,lin,lengths);

  int i;

  for(i=0;i<MOO_N_ACTU;i++)
    if(lengths[i]<=MOO_ACTU_MIN_MM+MOO_ACTU_FIXED_MM || lengths[i]>=MOO_ACTU_MAX_MM+MOO_ACTU_FIXED_MM)
    {
      VALUE to_ret=rb_ary_new_capa(3);
      
      rb_ary_store(to_ret,0,rb_str_new_cstr("A"));
      rb_ary_store(to_ret,1,INT2FIX(i));
      rb_ary_store(to_ret,2,DBL2NUM(lengths[i]));
      
      return to_ret;
    }

  uint8_t which_dof=0xff;
  float diff;
  
/*
 * Dof's are valid. Check if too large a step is reqd
 */

  for(i=0;i<3;i++)
  {
    diff=fabsf(s->tosendout.lin[i]-lin[i]);
    if(diff>max_lin)
    {
      which_dof=i;
      break;
    }
    diff=fabsf(s->tosendout.rot[i]-rot[i]);
    if(diff>max_ang)
    {
      which_dof=3+i;
      break;
    }
  }
  
  if(which_dof==0xff)
  {  
    pthread_mutex_lock(&s->mtx);

    memcpy(s->q.extensions,lengths,sizeof(float)*MOO_N_ACTU);
    
    memcpy(s->q.lin,lin,sizeof(hv));
    memcpy(s->q.rot,rot,sizeof(hv));

    s->q_flg=1;
  
    pthread_mutex_unlock(&s->mtx);
    
    return Qtrue;
  }
  
  VALUE to_ret=rb_ary_new_capa(3);
  
  rb_ary_store(to_ret,0,rb_str_new_cstr("L"));
  rb_ary_store(to_ret,1,INT2FIX(dof_mapper_to_moog[which_dof]));
  rb_ary_store(to_ret,2,DBL2NUM(diff));
  
  return to_ret;
}

/*
 * DANGEROUS! Feeds without checking
 */

VALUE moo_server_feed_immediate_regardless(VALUE self,VALUE v_dof)
{
  moo_server_stc *s;  
  Data_Get_Struct(self,moo_server_stc,s);
  float *dofv=(float *)RSTRING_PTR(v_dof);
  hv lin,rot;
  
  moogidea2serveridea(dofv,lin,rot);
  
  float lengths[MOO_N_ACTU];
  int i;
  
  get_actu_lengths(&s->p,rot,lin,lengths);    

  pthread_mutex_lock(&s->mtx);
  for(i=0;i<3;i++)
  {    
    s->dof_last_good.lin[i]=lin[i];
    s->dof_last_good.rot[i]=rot[i];
  }

  memcpy(s->q.extensions,lengths,sizeof(float)*MOO_N_ACTU);
  
  memcpy(s->q.lin,lin,sizeof(hv));
  memcpy(s->q.rot,rot,sizeof(hv));
  
  s->q_flg=1;
  
  pthread_mutex_unlock(&s->mtx);

  return self;
}

VALUE moo_server_freeze(VALUE self)
{
  moo_server_stc *s;  
  Data_Get_Struct(self,moo_server_stc,s);
  
/*
 * Freeze only from engaged
 */

  if(s->state!=MOO_SRVR_STATE_ENGAGED)
  {
    lg("Server: ERROR! Cannot freeze from state %s",srvr_state_labels[s->state]);
    return Qfalse;
  }

  if(s->action)
  {
    pthread_mutex_lock(&s->mtx);
    free(s->action);
    s->action=NULL;
    pthread_mutex_unlock(&s->mtx);
    lg("Server frozen");
  }
  else
    lg("WARNING: Server freeze requested, but action not underway");
    
  return self;
}

VALUE moo_server_cur_sts(VALUE self)
{
  moo_server_stc *s;  
  Data_Get_Struct(self,moo_server_stc,s);

  VALUE to_ret=rb_ary_new_capa(6+MOO_N_ACTU*2+4);
  int i;

  hv lin,rot;
  response_datapacket_stc response;
  float actu_exte[MOO_N_ACTU];
  float dofs[6];

  pthread_mutex_lock(&s->mtx);

  for(i=0;i<3;i++)
  {
    lin[i]=s->dof_last_good.lin[i];
    rot[i]=s->dof_last_good.rot[i];
  }

/*
 [0.0, 663.9816284179688, 0.0,
 0.0, -0.0, -0.0,
 854.0659790039062, 854.06591796875, 854.06591796875, 854.0658569335938, 854.06591796875, 854.06591796875,
 0.0, 0.0, -0.1989816278219223, 0.0, 0.0, 0.0,
 [[17], 1, 1, 3],
 3,
 true, false]
*/
  memcpy(&response,&s->response,sizeof(response_datapacket_stc));
  memcpy(actu_exte,s->tosendout.extensions,sizeof(float)*MOO_N_ACTU);
  memcpy(dofs,s->dofs_from_moog,sizeof(float)*6);
  
  rb_ary_store(to_ret,6+MOO_N_ACTU*2+2,s->newly_rcvd_flg ? Qtrue : Qfalse);
  rb_ary_store(to_ret,6+MOO_N_ACTU*2+3,s->action ? Qtrue : Qfalse);

  s->newly_rcvd_flg=0;  
  
  pthread_mutex_unlock(&s->mtx);

  for(i=0;i<3;i++)
  {
    rb_ary_store(to_ret,i,DBL2NUM(lin[i]));
    rb_ary_store(to_ret,3+i,DBL2NUM(rot[i]));
  }
  for(i=0;i<MOO_N_ACTU;i++)
    rb_ary_store(to_ret,6+i,DBL2NUM(actu_exte[i]));
  for(i=0;i<6;i++)
    rb_ary_store(to_ret,6+MOO_N_ACTU+i,DBL2NUM(dofs[i]));

  rb_ary_store(to_ret,6+MOO_N_ACTU*2,moo_response_parser(self,rb_str_new((char *)&response,sizeof(response_datapacket_stc))));
  rb_ary_store(to_ret,6+MOO_N_ACTU*2+1,INT2FIX(s->state));
	       
  return to_ret;
}

VALUE moo_server_engage(VALUE self,VALUE v_op_mode)
{
  moo_server_stc *s;  
  Data_Get_Struct(self,moo_server_stc,s);

  moo_mode_enum op_mode=FIX2INT(v_op_mode);

  switch(op_mode)
  {
  case MOO_MODE_LENGTH:
  case MOO_MODE_DOF:
    break;
  default:
    lg("Server: ERROR! Cannot engage to mode %s",mode_labels[op_mode]);
    return Qfalse;
  }    
  
/*
 * Engage only from idle
 */

  if(s->state!=MOO_SRVR_STATE_IDLE)
  {
    lg("Server: ERROR! Cannot engage from state %s",srvr_state_labels[s->state]);
    return Qfalse;
  }

  s->op_mode=op_mode;
  change_state(s,MOO_SRVR_STATE_ENGAGING);

  return Qtrue;
}

VALUE moo_server_park(VALUE self)
{
  moo_server_stc *s;  
  Data_Get_Struct(self,moo_server_stc,s);

/*
 * Park only from engaged
 */

  if(s->state!=MOO_SRVR_STATE_ENGAGED)
  {
    lg("Server: ERROR! Cannot park from state %s",srvr_state_labels[s->state]);
    return Qfalse;
  }

  change_state(s,MOO_SRVR_STATE_PARKING_WAIT);

  return Qtrue;
}

VALUE moo_server_cur_rotcenter(VALUE self)
{
  moo_server_stc *s;  
  Data_Get_Struct(self,moo_server_stc,s);

  return rb_str_new((char *)s->p.to_rotcenter,sizeof(hv));
}
  
VALUE moo_server_modify_rotcenter(VALUE self,VALUE v_rotcenter)
{
  moo_server_stc *s;  
  Data_Get_Struct(self,moo_server_stc,s);

  hv rotcenter;

  memcpy(rotcenter,RSTRING_PTR(v_rotcenter),sizeof(hv));
  modify_platform_rotcenter(&s->p,rotcenter);

  return self;
}

VALUE moo_server_get_actu_l(VALUE self,VALUE v_dof)
{
  moo_server_stc *s;  
  Data_Get_Struct(self,moo_server_stc,s);
  
  platf_stc p;
  int i;
  hv lin,rot;
  float lengths[MOO_N_ACTU];
  actu_stc *acp;

  memcpy(&p,&s->p,sizeof(platf_stc));
  for(acp=p.actu,i=0;i<MOO_N_ACTU;i++,acp++)
  {
    acp->pf=p.joints[0]+i;
    acp->pt=p.joints[2]+((i+1)%MOO_N_ACTU);    
  }

  for(i=0;i<3;i++)
  {
    lin[i]=NUM2DBL(rb_ary_entry(v_dof,i));
    rot[i]=NUM2DBL(rb_ary_entry(v_dof,i+3));
  }
//  lg("lin %.2f, %.2f, %.2f rot %.2f, %.2f, %.2f",lin[0],lin[1],lin[2],rot[0],rot[1],rot[2]);
  
  get_actu_lengths(&p,rot,lin,lengths);
//  lg("len %.2f, %.2f, %.2f %.2f, %.2f, %.2f",lengths[0],lengths[1],lengths[2],lengths[3],lengths[4],lengths[5]);
  
  VALUE to_ret=rb_ary_new_capa(2),v=rb_ary_new_capa(MOO_N_ACTU);

  rb_ary_store(to_ret,0,Qtrue);
  rb_ary_store(to_ret,1,v);

  for(i=0;i<MOO_N_ACTU;i++)
  {
    rb_ary_store(v,i,DBL2NUM(lengths[i]));
    if(lengths[i]<MOO_ACTU_MIN_MM+MOO_ACTU_FIXED_MM || lengths[i]>MOO_ACTU_MAX_MM+MOO_ACTU_FIXED_MM)
      rb_ary_store(to_ret,0,Qfalse);
  }
  
  return to_ret;
}

static void change_state(moo_server_stc *s,moo_srvr_state_enum new_state)
{
  if(s->state==new_state)
  {
    lg("Server: Warning: state %s doubly set",srvr_state_labels[s->state]);
    return;
  }

  clock_gettime(CLOCK_MONOTONIC,&s->state_change);
  s->state=new_state;
  lg("Server: State now %s",srvr_state_labels[s->state]);

/*
 * Whenever entering ENGAGED, copy actual readout to current status
 */

  if(s->state==MOO_SRVR_STATE_ENGAGED)
  {
    pthread_mutex_lock(&s->mtx);
    moogidea2serveridea(s->dofs_from_moog,s->p.pos,s->p.ypr);    
    pthread_mutex_unlock(&s->mtx);
  }

  prepare_packet_body(s);
}

static int prepare_packet_state(moo_server_stc *s)
{
  int cmd=-1;
  struct timespec now;
  long long int td;

  switch(s->state)
  {
  case MOO_SRVR_STATE_OFF:
    return -1;
    break;
  case MOO_SRVR_STATE_FAULT:
    cmd=MOO_CMD_DISABLE;    
    break;
  case MOO_SRVR_STATE_IDLE:
  case MOO_SRVR_STATE_ENGAGED:
    cmd=MOO_CMD_NEW_POSITION;
    break;
  case MOO_SRVR_STATE_ENGAGING:

/*
 * 1/2 second MOO_CMD_RESET
 * 1/2 second MOO_CMD_LENGTH_MODE or MOO_CMD_DOF_MODE
 * then MOO_CMD_ENGAGE until state becomes MOO_MACH_STATE_ENGAGED
 */

    clock_gettime(CLOCK_MONOTONIC,&now);  
    td=timediff(&s->state_change,&now);

    if(td<500)
      cmd=MOO_CMD_RESET;
    else if(td<1000)
      cmd=(s->op_mode==MOO_MODE_LENGTH) ? MOO_CMD_LENGTH_MODE : MOO_CMD_DOF_MODE;
    else
    {
      if(s->response.d.encoded_machine_state==MOO_MACH_STATE_ENGAGED)
      {
	change_state(s,MOO_SRVR_STATE_ENGAGED);
	return -1;
      }
      cmd=MOO_CMD_ENGAGE;
    }
    break;
  case MOO_SRVR_STATE_PARKING_WAIT:    
    if(s->response.d.encoded_machine_state==MOO_MACH_STATE_PARKING)
    {
      change_state(s,MOO_SRVR_STATE_PARKING);
      return -1;
    }
    cmd=MOO_CMD_PARK;
    break;
  case MOO_SRVR_STATE_PARKING:
    if(s->response.d.encoded_machine_state==MOO_MACH_STATE_IDLE)
    {
      change_state(s,MOO_SRVR_STATE_RESET);
      return -1;
    }
    break;
    cmd=MOO_CMD_LENGTH_MODE;
  case MOO_SRVR_STATE_RESET:

/*
 * 1/2 second MOO_CMD_RESET
 * 1/2 second MOO_CMD_DOF_MODE
 */

    clock_gettime(CLOCK_MONOTONIC,&now);  
    td=timediff(&s->state_change,&now);
    
    if(td<500)
      cmd=MOO_CMD_RESET;
    else if(td<1000)
      cmd=MOO_CMD_DOF_MODE;
    else
    {
      change_state(s,MOO_SRVR_STATE_IDLE);
      return -1;
    }

    break;
  }
  pthread_mutex_lock(&s->mtx);
  s->datapacket[0]=htonl(cmd);
  pthread_mutex_unlock(&s->mtx);

  return cmd;
}

static void prepare_packet_body(moo_server_stc *s)
{
  uint32_t datapacket[8],*dpp;

  bzero(datapacket,sizeof(uint32_t)*8);
  datapacket[0]=ntohl(s->datapacket[0]);  

  float *fptr=(float *)(datapacket+1);
  moo_srv_actu_stc *actu;
  int i;

/*
 * For any other message apart from NEW_POSITION, send ACTU_MIN (= parking position)
 */

  moo_srv_tosendout_stc tosendout;
  
  memcpy(&tosendout,&s->tosendout,sizeof(moo_srv_tosendout_stc));
  
  if(s->op_mode==MOO_MODE_LENGTH)
    for(i=0;i<MOO_N_ACTU;i++,actu++)
      fptr[actu_order[i]]=datapacket[0]==MOO_CMD_NEW_POSITION ? tosendout.extensions[i]/1000.0-MOO_ACTU_FIXED : MOO_ACTU_MIN;
  else if(s->op_mode==MOO_MODE_DOF)
  {
    if(datapacket[0]==MOO_CMD_NEW_POSITION)
    /* { */
    /*   fptr[0]=-s->p.ypr[2]; // roll */
    /*   fptr[1]=-s->p.ypr[1]; // pitch */
    /*   fptr[2]=-(s->p.pos[1]-MOO_ACTU_IDLE_H+MOO_ACTU_MOOG_IDLE); // heave */
    /*   fptr[3]=s->p.pos[2]; // surge */
    /*   fptr[4]=s->p.ypr[0]; // yaw */
    /*   fptr[5]=-s->p.pos[0]; // lateral */
    /* } */
      serveridea2moogidea(tosendout.lin,tosendout.rot,fptr);
    else
    {
      /* fptr[0]=0.0; // roll */
      /* fptr[1]=0.0; // pitch */
      /* fptr[2]=-MOO_ACTU_MOOG_IDLE; // heave */
      /* fptr[3]=0.0; // surge */
      /* fptr[4]=0.0; // yaw */
      /* fptr[5]=0.0; // lateral */
      bzero(fptr,sizeof(float)*6);      
    }
  }

  for(dpp=datapacket,i=0;i<8;i++,dpp++)
    (*dpp)=htonl(*dpp);

  pthread_mutex_lock(&s->mtx);
  memcpy(s->datapacket,datapacket,sizeof(uint32_t)*8);
  pthread_mutex_unlock(&s->mtx);

//  dump_packet(s); /***********************************************/
}

static void *thr(void *arg)
{
  lg("THR HAS PID %d",syscall(SYS_gettid));
  
  moo_server_stc *s=(moo_server_stc *)arg;
  struct timespec start,now,latest_to_receive;
  char bfr[BFR_SIZE];
  double fpos;
  float *fbfr=(float *)(bfr+12),f,g;
  moo_srv_dof_stc *dpf,*dpt,*dpc;
  int i,res;
  uint8_t fail_flg,msg_recv_flg=0,complained_flg=0;
  uint32_t *dpp,td,timestep;
  uint64_t send_next=0;
  struct pollfd pfd={s->skt,POLLIN,0};
  moo_srv_action_stc a;
  hv v;
  uint32_t tmp_pkt[8];

  fill_default_idle_packet(s);

  clock_gettime(CLOCK_MONOTONIC,&start);
  memcpy(&latest_to_receive,&start,sizeof(struct timespec));
  timeadd(&latest_to_receive,MAXWAIT_USEC);
	 
  while(!s->leave_thread)
  {
    clock_gettime(CLOCK_MONOTONIC,&now);
    res=timepast(&latest_to_receive,&now);
    if(msg_recv_flg || res) // either I received a msg or I have received a timeout
    {
      timeadd(&latest_to_receive,MAXWAIT_USEC);
      
      if(res)
      {
	if(!complained_flg)
	{
	  lg("Warning: Not receiving moog msgs!");
	  complained_flg=1;
	}
      }
      else
      {
	complained_flg=0;
	msg_recv_flg=0; // reset
      }
      
/*
 * prepare next packet
 */
      
      prepare_packet_state(s);
      
      if(s->action)
      {
	pthread_mutex_lock(&s->mtx);
	memcpy(&a,s->action,sizeof(moo_srv_action_stc));
	s->action_changed=0;
	pthread_mutex_unlock(&s->mtx);

	td=timediff_usec(&a.start,&now);
	fpos=(td>=a.length) ? 1.0 : (double)td/a.length;

	if(a.algo==ALG_SINEACC)
	{	  
	  accsin_pos_at(a.u.accsin,fpos,s->p.pos,s->p.ypr);
	  for(i=0;i<3;i++)
	  {
	    a.dof_cur.lin[i]=s->p.pos[i];
	    a.dof_cur.rot[i]=s->p.ypr[i];
	  }	  
	}
	else if(a.algo==ALG_DAMPEDOSC)
	{
	  int pos=(int)(fpos*a.u.dho->n_steps);

	  if(pos<0)
	    pos=0;
	  else if(pos>=a.u.dho->n_steps)
	    pos=a.u.dho->n_steps-1;

	  memcpy(s->p.pos,a.u.dho->l.steps[pos],sizeof(hv));
	  memcpy(s->p.ypr,a.u.dho->a.steps[pos],sizeof(hv));

	  for(i=0;i<3;i++)
	  {
	    a.dof_cur.lin[i]=s->p.pos[i];
	    a.dof_cur.rot[i]=s->p.ypr[i];
	  }	  
	}	  
	else
	{	  
	  switch(a.algo)
	  {
	  case ALG_LIN:	    
	    break;
	  case ALG_SIN:
	    fpos=sin(fpos*M_PI-M_PI_2)*0.5+0.5;
	    break;
	  case ALG_CASTELJAU:
	    fpos=casteljau_calc(&s->interpolator,fpos);
	    break;
	  default:
	    lg("Unknown algo type %d",a.algo);
	    abort();
	  }
	  
	  for(i=0;i<3;i++)
	  {
	    a.dof_cur.lin[i]=a.dof_from.lin[i]+(a.dof_to.lin[i]-a.dof_from.lin[i])*fpos;
	    a.dof_cur.rot[i]=a.dof_from.rot[i]+(a.dof_to.rot[i]-a.dof_from.rot[i])*fpos;
	    
	    s->p.pos[i]=a.dof_cur.lin[i];
	    s->p.ypr[i]=a.dof_cur.rot[i];
	  }
	}
	
	apply_motion(&s->p); // here extension is set
	
	for(fail_flg=0,i=0;i<MOO_N_ACTU;i++)
	  if(s->p.actu[i].extension<=MOO_ACTU_MIN_MM+MOO_ACTU_FIXED_MM || s->p.actu[i].extension>=MOO_ACTU_MAX_MM+MOO_ACTU_FIXED_MM)
	  {
	    lg("Server: Fail at actuator %d (%f/%f/%f)",i,s->p.actu[i].extension,MOO_ACTU_MIN_MM+MOO_ACTU_FIXED_MM,MOO_ACTU_MAX_MM+MOO_ACTU_FIXED_MM);
	    s->actu[i].fail_flg=fail_flg=1;
	  }
	  else
	    s->actu[i].fail_flg=0;
	
	if(fail_flg)
	{
	  lg("FAIL: lin %.3f,%.3f,%.3f ang %.3f,%.3f,%.3f",
	     s->p.pos[0],s->p.pos[1],s->p.pos[2],
	     s->p.ypr[0]*180.0/M_PI,s->p.ypr[1]*180.0/M_PI,s->p.ypr[2]*180.0/M_PI);
	     
	  for(i=0;i<MOO_N_ACTU;i++)
	  {
	    s->p.actu[i].extension=s->actu[i].cur_length;
	    if(s->p.actu[i].extension<=MOO_ACTU_MIN_MM+MOO_ACTU_FIXED_MM)
	      s->p.actu[i].extension=MOO_ACTU_MIN_MM+MOO_ACTU_FIXED_MM+1.0;
	    else if(s->p.actu[i].extension>=MOO_ACTU_MAX_MM+MOO_ACTU_FIXED_MM)
	      s->p.actu[i].extension=MOO_ACTU_MAX_MM+MOO_ACTU_FIXED_MM-1.0;
	  }
	}
	else
	{
	  memcpy(&s->dof_last_good,&a.dof_cur,sizeof(moo_srv_dof_stc));

	  for(i=0;i<MOO_N_ACTU;i++)
	    s->tosendout.extensions[i]=s->p.actu[i].extension;	  
	  for(i=0;i<3;i++)
	  {
	    s->tosendout.lin[i]=a.dof_cur.lin[i]+s->p.movplat_rotcenter_offs[i];
	    s->tosendout.rot[i]=a.dof_cur.rot[i];
	  }

/*
 * Prepare datapacket (was done in low thread)
 */

	  prepare_packet_body(s);
	}
	
	pthread_mutex_lock(&s->mtx);
	if(s->action && !s->action_changed)
	{
	  if((double)td>a.length || fail_flg) // cancel action
	  {
	    free(s->action);
	    s->action=NULL;
	  }
	  else
	    memcpy(s->action,&a,sizeof(moo_srv_action_stc));
	}
	s->action_changed=0;
	pthread_mutex_unlock(&s->mtx);
      }
      else if(s->q_flg)
      {
	pthread_mutex_lock(&s->mtx);
	memcpy(&s->tosendout,&s->q,sizeof(moo_srv_tosendout_stc));
	pthread_mutex_unlock(&s->mtx);
	prepare_packet_body(s);
      }
      s->q_flg=0;	
      
/*
 * Eventually send
 */      
      
      if(send(s->skt,s->datapacket,sizeof(uint32_t)*8,0)<0)
	lg("Server: %s WARNING! Error sending msg (%s)",__func__,strerror(errno));
      else if(s->logunit_out>0)
      {
	struct timespec now;
	
	clock_gettime(CLOCK_MONOTONIC,&now);  
	write(s->logunit_out,&now,sizeof(struct timespec));
	write(s->logunit_out,s->tosendout.lin,sizeof(hv));
	write(s->logunit_out,s->tosendout.rot,sizeof(hv));
	write(s->logunit_out,s->tosendout.extensions,sizeof(float)*MOO_N_ACTU);
      }      
    }
//    else
//      usleep(1000);    
    
/*
 * See if replies are present on socket
 */
    
    pfd.events=POLLIN;
    res=poll(&pfd,1,POLL_TIMEOUT);
//      teto_checkpoint(ts,7);
    if(res>0)
    {
      res=recv(s->skt,bfr,BFR_SIZE,0);
      if(res!=40)
	lg("Server: %s Warning: bad pkt received (len %d // %s)",__func__,res,strerror(errno));
      else
      {
	msg_recv_flg=1;
	clock_gettime(CLOCK_MONOTONIC,&latest_to_receive);
	timeadd(&latest_to_receive,MAXWAIT_USEC);	
	
	for(dpp=(uint32_t *)bfr,i=0;i<10;i++,dpp++)
	  (*dpp)=ntohl(*dpp);
	
	pthread_mutex_lock(&s->mtx);
	memcpy(&s->response,bfr,sizeof(response_datapacket_stc));
	memcpy(s->dofs_from_moog,fbfr,sizeof(float)*6);
	
//	lg("   UA %f %f %f %f %f %f",res,fbfr[0],fbfr[1],fbfr[2],fbfr[3],fbfr[4],fbfr[5]);
	
	s->newly_rcvd_flg=1;	  
	pthread_mutex_unlock(&s->mtx);
	
	if(s->logunit_in>0)
	{
	  clock_gettime(CLOCK_MONOTONIC,&now);
	  write(s->logunit_in,&now,sizeof(struct timespec));
	  write(s->logunit_in,bfr,40);
	}
	
/*
 * If first loop ever, check if moog state is MOO_MACH_STATE_FAULT2, and in that case force status as
 * MOO_SRVR_STATE_RESET
 */
	
	if(s->first_loop_flg)
	{
	  s->first_loop_flg=0;
	  
	  if(s->response.d.encoded_machine_state==MOO_MACH_STATE_FAULT2)
	  {
	    lg("===> Moog was in FAULT2 - resetting");
	    change_state(s,MOO_SRVR_STATE_RESET);
	  }
	}
      }
    }
  }

  prepare_packet_state(s);
  prepare_packet_body(s);
  if(send(s->skt,s->datapacket,sizeof(uint32_t)*8,0)<0)
    lg("Server (before leaving): %s WARNING! Error sending msg (%s)",__func__,strerror(errno));
  
//  print_teto_results_and_dealloc(ts);
  
  usleep(50000);  
  lg("Server thr out.");
    
  return NULL;
}

static void srvr_shutdown(moo_server_stc *s)
{
  if(s->closed_flg)
    return;

  s->closed_flg=1;

  s->leave_thread=1;
  pthread_join(s->thr,NULL);

  close(s->skt);
  free(s->loc_addr);
  free(s->rem_addr);

  if(s->logpath)
  {
    close(s->logunit_in);
    close(s->logunit_out);
    free(s->logpath);
  }

  free(s->interpolator.steps);
}

static void get_actu_lengths(platf_stc *p,const hv ypr,const hv pos,float lengths[MOO_N_ACTU])
{
//  hv4 q;
  hv v,mv;
  hm4 m1,m2,rotm;
  int i;
  actu_stc *acp;

//  tait_bryan_to_quat(ypr,q);
//  quat_to_hm4(q,m1);
  mat4_tait_bryan(ypr,m1);

  mat4_unit(m2);

  m2[3][0]=pos[0];
  m2[3][1]=pos[1];
  m2[3][2]=pos[2];
  mat4_mult(m1,m2,rotm);

  memcpy(p->joints[2],p->joints[1],sizeof(hv)*MOO_N_ACTU);

  for(acp=p->actu,i=0;i<MOO_N_ACTU;i++,acp++)
  {
    mat4_vect_mult(rotm,*(acp->pt),v);
    vect_sub(v,*(acp->pf),mv);
    lengths[i]=vect_normalize(mv,v);
  }
}

static void fill_default_idle_packet(moo_server_stc *s)
{
  int i;
  uint32_t *dpp;

  bzero(s->datapacket,sizeof(uint32_t)*8);
  
  s->datapacket[0]=MOO_CMD_NEW_POSITION;

  if(s->op_mode==MOO_MODE_LENGTH)
    for(dpp=s->datapacket+1,i=0;i<6;i++)
      *dpp=MOO_ACTU_MIN;

  for(dpp=s->datapacket,i=0;i<8;i++,dpp++)
    (*dpp)=htonl(*dpp);  
}

#if 0
static void dump_packet(moo_server_stc *s)
{
  uint32_t v;
  float f;
  int i;
  
  fputs("*------------------------------------------------------------------*\n",stderr);
  fprintf(stderr,"Code: %x\n",htonl(s->datapacket[0]));
  for(i=0;i<6;i++)
  {
    v=htonl(s->datapacket[i+1]);
    memcpy(&f,&v,4);
    
    fprintf(stderr,"%.3f ",f);
  }
  fputs("\n*------------------------------------------------------------------*\n",stderr);
}
#endif
