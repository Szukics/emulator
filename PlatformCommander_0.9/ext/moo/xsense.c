/* xsense.c */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================
 *
 * Code to read data from xsens sensor (now official one is moved to hw)
 */

/*
 * Software to converse with the Xsense MTx motion sensor
 */

#include <carlobase.h>
#include <maths_include.h>
#include "moo.h"
#include <pthread.h>
#include <poll.h>
#include <termios.h>

#define XSENSE_SPEED B115200
#define POLL_TIMEOUT 20
#define BFR_SIZE 0x100

typedef struct xsense_event
{
  int msgtype,msglen;
  uint8_t *msg;
  struct timespec reftime;  
  struct xsense_event *next;
} xsense_event_stc;

typedef struct
{
  char *devname;
  struct termios orig_tios,tios;
  int unit;

  xsense_event_stc *events;

  pthread_t thr;
  pthread_mutex_t mtx;
  uint8_t leave_thread;
} moo_xsense_stc;

extern VALUE mod_moo,cls_moo_xsense;

static void free_moo_xsense(void *p);
static void close_thread(moo_xsense_stc *s);
static void *xsense_thr(void *arg);
static int parse_msg(const uint8_t *bfr,const int len,int *consumed,uint8_t *msg_id,uint8_t **retbfr,int *mlen);

VALUE new_moo_xsense(VALUE self,VALUE v_devname)
{
  moo_xsense_stc *s;
  VALUE sdata=Data_Make_Struct(cls_moo_xsense,moo_xsense_stc,NULL,free_moo_xsense,s),v;
  
  bzero(s,sizeof(moo_xsense_stc));
  
  s->devname=strdup(RSTRING_PTR(v_devname));
  s->unit=open(s->devname,O_RDWR|O_NOCTTY|O_NONBLOCK);  
  if(s->unit<0)
    rb_raise(rb_eArgError,"%s: Device %s could not be opened (%s)",__func__,s->devname,strerror(errno));
  fcntl(s->unit,F_SETFL,O_RDWR);
  if(tcgetattr(s->unit,&s->orig_tios)<0)
    rb_raise(rb_eArgError,"%s: Device %s: error in tcgetattr (%s)",__func__,s->devname,strerror(errno));
  memcpy(&s->orig_tios,&s->tios,sizeof(struct termios));  

  s->tios.c_cflag&=(~(CLOCAL|CREAD|CSIZE|CSTOPB|CRTSCTS));
  s->tios.c_cflag|=((CLOCAL|CREAD|CS8|CSTOPB)&(~PARENB));
  s->tios.c_iflag|=IGNPAR;
  s->tios.c_iflag&=~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL|IXON|IXOFF|IXANY|INPCK|ECHO|ECHONL|ICANON|ISIG|IEXTEN);
  s->tios.c_oflag&=~OPOST;
  s->tios.c_cc[VMIN]=1; // minimum number of chars to read in noncanonical (raw mode)
  s->tios.c_cc[VTIME]=5; // time in deciseconds to wait for data in noncanonical mode (raw mode)

  cfsetspeed(&s->tios,XSENSE_SPEED);

  if(tcsetattr(s->unit,TCSAFLUSH,&s->tios))
    rb_raise(rb_eArgError,"%s: Device %s: error in tcsetattr (%s)",__func__,s->devname,strerror(errno));

/*
 * The thread for managing the conversation
 */

  int sts;
  
  sts=pthread_mutex_init(&s->mtx,NULL);
  if(sts)
    rb_raise(rb_eArgError,"%s: error #%d creating mutex (%s)",__func__,sts,strerror(errno));
  
  s->leave_thread=0; 

  sts=pthread_create(&s->thr,NULL,xsense_thr,s);
  if(sts)
    rb_raise(rb_eArgError,"%s: error #%d starting thread (%s)",__func__,sts,strerror(errno));

  return sdata;  
}

static void free_moo_xsense(void *p)
{
  moo_xsense_stc *s=(moo_xsense_stc *)p;

  close_thread(s);
  free(s->devname);

  free(p);
}

VALUE moo_xsense_send_msg(VALUE self,VALUE v_msg_id,VALUE v_payload)
{
  moo_xsense_stc *s;  
  Data_Get_Struct(self,moo_xsense_stc,s);

  int msg_id=FIX2INT(v_msg_id),msg_len=(v_payload==Qnil) ? 0 : RSTRING_LEN(v_payload),sum,i;
  uint8_t bfr[msg_len+5];

  if(msg_len<0 || msg_len>0xff)
    rb_raise(rb_eArgError,"%s: Extended length still not supported",__func__);

  bfr[0]=0xfa;
  bfr[1]=0xff;
  bfr[2]=msg_id;
  bfr[3]=msg_len;
  if(msg_len>0)
    memcpy(bfr+4,RSTRING_PTR(v_payload),msg_len);

  for(sum=0,i=1;i<msg_len+4;i++)
    sum+=bfr[i];
  bfr[4+msg_len]=0x100-(sum&0xff);

  int res=write(s->unit,bfr,5+msg_len);
  if(res<0)
    rb_raise(rb_eArgError,"%s: Error writing! (%s)",__func__,strerror(errno));
  tcdrain(s->unit);

//  lg("=> sent %d bytes",5+msg_len);
//  hexprint((char *)bfr,5+msg_len);

  return self;
}

VALUE moo_xsense_poll(VALUE self)
{
  moo_xsense_stc *s;  
  Data_Get_Struct(self,moo_xsense_stc,s);

  if(!s->events)
    return Qfalse;

  xsense_event_stc *evq,*evp1,*evp2;

  pthread_mutex_lock(&s->mtx);
  evq=s->events;
  s->events=NULL;
  pthread_mutex_unlock(&s->mtx);

  VALUE to_ret=rb_ary_new(),v;

  for(evp1=evq;evp1;)
  {
    evp2=evp1;
    evp1=evp2->next;

    v=rb_ary_new_capa(3);
      
    rb_ary_store(v,0,INT2FIX(evp2->msgtype));
    rb_ary_store(v,1,rb_str_new((char *)evp2->msg,evp2->msglen));
    rb_ary_store(v,2,rb_str_new((char *)&evp2->reftime,sizeof(struct timespec)));    
    rb_ary_push(to_ret,v);
		 
    free(evp2->msg);
    free(evp2);
  }    

  return to_ret;
}

VALUE moo_xsense_close_thread(VALUE self)
{
  moo_xsense_stc *s;  
  Data_Get_Struct(self,moo_xsense_stc,s);

  close_thread(s);

  return self;
}

static void close_thread(moo_xsense_stc *s)
{
  if(s->leave_thread)
    return;

  s->leave_thread=1;
  pthread_join(s->thr,NULL);

  if(tcsetattr(s->unit,TCSAFLUSH,&s->orig_tios))
    rb_raise(rb_eArgError,"%s: Device %s: error in tcsetattr/close (%s)",__func__,s->devname,strerror(errno));
  close(s->unit);

  lg("Xsense thread closed.");
}

static void *xsense_thr(void *arg)
{
  moo_xsense_stc *s=(moo_xsense_stc *)arg;
  struct pollfd pfd={s->unit,POLLIN,0};
  int res,len,cur_bfr_len,stale_len=0,parsed_len=0,i,j,mlen=0,consumed;
  uint8_t cond,bfr[BFR_SIZE],msg_id=0xff,*msg_ptr=NULL;
  xsense_event_stc *ev=NULL,*evp;
  int16_t sp[11];
  
  while(!s->leave_thread)
  {
    pfd.events=POLLIN;
    res=poll(&pfd,1,POLL_TIMEOUT);

    if(res<0)
    {
      lg("%s: Error in polling (%s)",__func__,strerror(errno));
      break;
    }
    
    if(res!=0)
    {
      i=read(s->unit,bfr+stale_len,BFR_SIZE-stale_len);
      if(i<=0)
	break;
      
//      lg("=> Ho letto %x (stale %x, old parsed %x)",i,stale_len,parsed_len);
//      hexprint((char *)bfr,stale_len+i);
	
      parsed_len=0;
      cur_bfr_len=stale_len+i;

      while(1)
      {
	j=parse_msg(bfr+parsed_len,cur_bfr_len-parsed_len,&consumed,&msg_id,&msg_ptr,&mlen);
	if(j)
	{
//	  lg("-->> recv %x (%d bytes)",msg_id,mlen);
	  
	  ev=malloc(sizeof(xsense_event_stc));
	  ev->msgtype=msg_id;
	  ev->msglen=mlen;
	  ev->msg=msg_ptr;
	  clock_gettime(CLOCK_MONOTONIC,&ev->reftime);
	  ev->next=NULL;

	  pthread_mutex_lock(&s->mtx);
	  if(!s->events)
	    s->events=ev;
	  else
	  {
	    for(evp=s->events;evp->next;evp=evp->next)
	      ;
	    evp->next=ev;
	  }
	  pthread_mutex_unlock(&s->mtx);
	}
	
	parsed_len+=consumed;

	if(!j)
	  break;
      }
      stale_len=cur_bfr_len-parsed_len;
      memmove(bfr,bfr+parsed_len,stale_len);
//      lg("Ho mosso: parsed %x stale %x",parsed_len,stale_len);
//      hexprint((char *)bfr,BFR_SIZE);      
    }
  }

  lg("Xsense thr out.");
    
  return NULL;
}

static int parse_msg(const uint8_t *bfr,const int len,int *consumed,uint8_t *msg_id,uint8_t **retbfr,int *mlen)
{
  int i,j,begpos,sum;
  uint8_t *found=NULL;

  for(i=0;i<len-2;i++)
  {
    if(bfr[i]!=0xfa || bfr[i+1]!=0xff)
      continue;
    *(msg_id)=bfr[i+2];
    *(mlen)=bfr[i+3];
    
    if((*mlen)==0xff)
    {
      lg("%s: ERROR! Extended len not handled (msg %x)",__func__,*msg_id);
      hexprint((char *)bfr,len);
      abort();
    }

    if((i+4+(*mlen))>=len)
    {
      (*consumed)=i-1;      
      return 0;
    }    

    for(sum=0,j=1;j<(*mlen)+5;j++)
    {
      sum+=bfr[i+j];
//      lg("Aft %d (%x) -> %x",j,bfr[i+j],sum);
    }

    if((sum&0xff)!=0)
    {
      lg("%s: ERROR! Bad checksum %x (from %d)",__func__,sum,i);
      hexprint((char *)bfr,len);
      continue;
    }

    (*retbfr)=malloc(*mlen);
    memcpy(*retbfr,bfr+i+4,*mlen);
    (*consumed)=i+5+(*mlen);
    
    return 1;
  }

//  lg("VUOTA %d/%d",i,len);

  (*consumed)=len-2;  
  return 0;
}



  
	
