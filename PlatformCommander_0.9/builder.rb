#!/usr/bin/env -S ruby -I./libs/
# builder.rb

=begin

==========================================================================
Copyright (C) 2020 Carlo Prelz, University of Bern

carlo.prelz@humdek.unibe.ch

This file is part of Platform Commander.

Platform Commander is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Platform Commander is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
==========================================================================

Run this script to build Placo 

=end

require_relative('scripts/cx')

class Builder
  TAGFILE='TAG'
  EXTS=['carlobase','glengine','hw','maths','memblock','moo','moogskt_ruby']

  #
  # MUST MATCH BASE in moo.rb
  #
  
  BASEDIR='./moog_base/'
  
  def initialize
    raise "Tag file missing!" unless(File::file?(TAGFILE))
    @tag=Marshal::restore(File::read(TAGFILE))
  end

  def runme
    lg("Compiling platform commander v#{@tag[:v]} (produced #{Time::at(@tag[:time]).stamp})")
    mysystem("(cd special/moogskt/ &&  make V=1 && cp libmoogskt.so  ../../libs && cp libmoogskt.so ../../c)")
    EXTS.each do |ext|
      mysystem("(cd ext/#{ext}/ && ruby extconf.rb && make -j12)")
    end
    mysystem("(cd libs && find ../ext/ -name '*.so' -exec ln -s {} . \\; )",false)

    mysystem("mkdir -p #{BASEDIR}")
    
    ['css','shaders'].each do |d|
#      FileUtils::rm(BASEDIR+d) if(File::exist?(BASEDIR+d))
      FileUtils::rm_f(BASEDIR+d) 
      FileUtils::ln_s(Dir::pwd+'/'+d,BASEDIR)
    end
  end
end

Builder::new.runme
