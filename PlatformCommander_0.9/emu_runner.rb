#!/usr/bin/env -S ruby -I./libs/
# emu_runner.rb

=begin

==========================================================================
Copyright (C) 2020 Carlo Prelz, University of Bern

carlo.prelz@humdek.unibe.ch

This file is part of Platform Commander.

Platform Commander is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Platform Commander is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
==========================================================================

Run this script to start the emulator

=end


require('cx')

class Emu_runner
  def initialize
    ENV['RUBYLIB']='./libs/'
    ENV['SERVER_INPORT']='11332'
    ENV['SERVER_OUTPORT']='11331'
    ENV['SERVER_LOCAL_ADDRESS']='127.0.0.1'
    ENV['NORMAL_USER']=(`whoami`).chomp
    ENV['CAM_DISPLAY']=ENV['DISPLAY']
  end

  def runme
    require_relative('scripts/emu2.rb')
    Moo::Emu::new(Moo::SERVER_LOCAL_ADDRESS,Moo::SERVER_OUTPORT,Moo::SERVER_INPORT,11431,11432,['ve1','fr1'],true).runme
  end
end

Emu_runner::new.runme


    
