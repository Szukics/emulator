#!/usr/bin/env -S ruby -I./libs/
# emu_demo.rb

=begin

==========================================================================
Copyright (C) 2020 Carlo Prelz, University of Bern

carlo.prelz@humdek.unibe.ch

This file is part of Platform Commander.

Platform Commander is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Platform Commander is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
==========================================================================

Connects to the running platform emulator, and runs one of four
possible demos.

=end

($:).push('./scripts/')
($:).push('./libs/')

ENV['CAM_DISPLAY']=ENV['DISPLAY']
ENV['SERVER_LOCAL_ADDRESS']='127.0.0.1'
ENV['SERVER_INPORT']='11332'
ENV['SERVER_OUTPORT']='11331'

require('mr')

class Single_demo
  attr_reader(:name,:mr)

  RECVPORT_TAG='_-_RECVPORT-_-'

  LOGIN_WAIT=10.0

  STATE_IDLE=0
  STATE_ENGAGING=1  
  STATE_ENGAGED=2
  STATE_PARKING=3
  STATE_LOGOUT=4
  STATE_OFF=5

  STATES={STATE_IDLE=>'Idle',STATE_ENGAGING=>'Engaging',STATE_ENGAGED=>'Engaged',
          STATE_PARKING=>'Parking',STATE_LOGOUT=>'Log out',STATE_OFF=>'Off'}

  REPORT_INTERVAL=100
  
  def initialize(name)
    @name=name
    @state=STATE_IDLE
    @start=Time::now.to_f
  end

  def exec_lowlevel(remote,login_msg)
    @remote=remote
    @mr=Moogskt_ruby::new(SERVER_LOCAL_ADDRESS,[@remote,'','',''],SERVER_OUTPORT,SERVER_INPORT)
    
    @rcv_thr=Thread::new do
      loop do
        pkts=@mr.recv_pkts
        if(pkts)
          pkts.each do |p|
            packet_parse(p)
          end
        else
          sleep(0.1)
        end
      end
    end
    @rcv_thr.priority=-2

    @login_msg=login_msg.gsub(RECVPORT_TAG,@mr.recvport.to_s)
    @mr.send_pkt(MSG_LOGIN,@login_msg)

    loop do
      break if(!step())
      @rcv_thr.wakeup
    end

    @mr.close
  end

  def packet_parse(p)
    a=p.split(',')

    time=a.shift.to_i
    msg_id=a.shift.to_i
    case msg_id
    when MSG_LOGIN_ACK
      lg("Login confirmed. Session tag: #{a[0]}")
      after_login
    when MSG_LOGIN_NAK
      lg("Error logging in: #{a[0]}")
      exit(0)
    when MSG_ENGAGE_ACK
      lg("Engage request confirmed.")
      after_engage
    when MSG_ENGAGE_NAK
      lg("Engage request not acknowledged (#{a[0]}).")
    when MSG_ENGAGE_DONE
      lg("Engage completed.")
      new_state(STATE_ENGAGED)
    when MSG_STATE
      @sts=a[0,3].map do |v|
        v.to_i
      end
      @dof=a[3,6].map do |v|
        v.to_f
      end
    when MSG_PARK_ACK
      lg("Park request confirmed.")
    when MSG_PARK_DONE
      after_park
    when MSG_LOGOUT_ACK
      lg("Logout confirmed.")
      new_state(STATE_OFF)
    when MSG_DPIN_CHANGE
      lg("Button #{a[0]} #{a[1]=='1' ? 'pres' : 'relea'}sed.")      
    when MSG_BRANCH
      lg("Branch: #{a}")
    when MSG_IDLE_TIMEOUT
      lg("Idle timeout!")
      new_state(STATE_OFF)
    else
      lg("Rcvd Msg #{msg_id} not processed (#{a})") unless(packet_parse_local(time,msg_id,a))
    end
  end

  def step
    t=Time::now.to_f
    if(@state==STATE_IDLE)
      raise "Login: waited too much!" if((t-@start)>LOGIN_WAIT)
    elsif(!STATES[@state] || @state==STATE_ENGAGED)
      return subclass_step(t)
    end

    true
  end

  def new_state(ns)
    if(ns==@state)
      lg("Warning: state unchanged to #{ns}")
      return
    end

    @state_time=Time::now.to_f
    @state=ns

    return lower_new_state unless(STATES[@state])

    lg("Change state: #{STATES[@state]}")    
    
    case @state
    when STATE_ENGAGING
      lg("Engaging.")
      @mr.send_pkt(MSG_ENGAGE,nil)
    when STATE_PARKING
      lg("Parking.")
      @parking=Time::now.to_f
      @mr.send_pkt(MSG_PARK,nil)
    when STATE_LOGOUT
      lg("Logging out.")
      @mr.send_pkt(MSG_LOGOUT,REPORT_INTERVAL.to_s)      
    when STATE_OFF
      @mr.close
      exit(0)
    end
  end

  def packet_parse_local(time,msg_id,a)
    false
  end

  def after_login
    new_state(STATE_ENGAGING)
  end
  def after_engage
  end
  def after_park
    new_state(STATE_LOGOUT)
  end

  def lower_new_state
    raise "Unknown state #{@state}"
  end                                
  
  def self::all
    [Demo_immediate::new,Demo_shortseq::new,Demo_longseq::new,Demo_joystick::new]
  end
end

class Demo_immediate < Single_demo
  LINSTEPS=700
  ANGSTEPS=1400

  MAX_LIN=0.05
  MAX_ANG=1.0*Math::PI/180.0
  HGHT_B=-0.1
  HGHT_B_MM=(-HGHT_B)*1000.0
  HGHT=250.0

  RAD=90.0
  INCL=-6.0

  STEP_LEN=0.0045
  REPORT_INTERVAL=100

  def initialize
    super('Immediate')

    @step_arr=build_steps().map do |a|
      [
        -a[5]*DEG2RAD,
        -a[4]*DEG2RAD,    
        -a[1]/1000.0,
        a[2]/1000.0,
        a[3]*DEG2RAD,    
        -a[0]/1000.0,
      ]
    end
  end

  def exec(remote)
    @cur_step=nil
    @state=STATE_IDLE
    
    exec_lowlevel(remote,sprintf("%d,#{RECVPORT_TAG},%f,%f,%f,zlo,zli,1 zla",SESSION_IMMEDIATE,MAX_LIN,MAX_ANG,HGHT_B))
  end

  def subclass_step(t)
    return true unless(@state==STATE_ENGAGED)
    
    tidiff=Time::now.to_f-@state_time
    new_step=(tidiff/STEP_LEN).to_i
    if(new_step!=@cur_step)
      lg("At step #{new_step} of #{@step_arr.length}") if(new_step%250==0)
      if(new_step>=@step_arr.length)
        new_state(STATE_PARKING)
        return true
      end
      
      @cur_step=new_step
      @mr.send_pkt(MSG_IMMEDIATE_REQ,sprintf('%d,%f,%f,%f,%f,%f,%f',@cur_step,*@step_arr[@cur_step]))
    end
    true
  end

  def build_steps
    a=[]
    LINSTEPS.times do |i|
      v=(1.0/LINSTEPS)*i
      a.push([0.0,HGHT_B_MM+(HGHT-HGHT_B_MM)*v,RAD*v,0.0,INCL*v,0.0])
    end
    b=a.reverse
    ANGSTEPS.times do |i|
      v=(1.0/ANGSTEPS)*i
      av=2.0*Math::PI*v
      
      sv=Math::sin(av)
      cv=Math::cos(av)
      a.push([sv*RAD,HGHT,cv*RAD,0.0,INCL*cv,INCL*sv])
    end
    a+b
  end

  def packet_parse_local(time,msg_id,a)
    case msg_id
    when MSG_IMMEDIATE_AT_HEIGHT
      lg("At height.")
      @at_height=Time::now.to_f
      return true
    when MSG_IMMEDIATE_ACK
      return true
    end      
    false
  end
end

class Demo_shortseq < Single_demo
  START_HEIGHT=-0.2
  REPORT_INTERVAL=100
  MAX_SPEED=0.1
  ROT_CENTER=[0.9,0.0,-0.3] # y,z,x in meters

  H=Moogskt_ruby::MOOG_H_TO_SRV_H*1000+200

  STEP_ARR=[
    [[0.0,H-50,0.0,0.0,0.0,0.0],5.0],
    [[0.0,H,0.0,0.0,0.0,0.0],3.0],
    [[0.0,H,80.0,6.0,0.0,-5.0],6.0],
    [[0.0,H,0.0,0.0,0.0,0.0],6.0],
    [[0.0,H,-80.0,-6.0,0.0,5.0],6.0],
    [[0.0,H,0.0,0.0,0.0,0.0],6.0],
    [[0.0,H-50,0.0,0.0,0.0,0.0],5.0],
  ].map do |a|
    aa=[a[0][0],a[0][1],a[0][2],a[0][3]*DEG2RAD,a[0][4]*DEG2RAD,a[0][5]*DEG2RAD]
    [Moogskt_ruby::server_idea_to_moog_idea(aa),a[1],'S']
  end
  
  def initialize
    super('Short sequence')
    @fired=false
  end

  def exec(remote)
    @state=STATE_IDLE
    exec_lowlevel(remote,sprintf("%d,#{RECVPORT_TAG},D,%f,,red,yellow",SESSION_SHORT_SEQ,START_HEIGHT))
  end

  def subclass_step(t)
    return true unless(@state==STATE_ENGAGED)
    unless(@fired)
      feed_short_seq
      @fired=Time::now.to_f
    end

    true
  end

  def packet_parse_local(time,msg_id,a)
    case msg_id
    when MSG_SHORT_SEQ_AT_HEIGHT
      lg("At height.")
      @at_height=Time::now.to_f
      return true
    when MSG_SHORT_SEQ_ACK
      lg("Short seq request acknowledged.")
      return true
    when MSG_SHORT_SEQ_DONE
      lg("Short seq completed.")
      new_state(STATE_PARKING)
      return true
    end      
    false
  end

  def feed_short_seq
    @mr.send_pkt(MSG_BEGIN_SHORT_SEQ,sprintf("%f,%f,%f,%f",MAX_SPEED,*ROT_CENTER))
    STEP_ARR.each do |s|
      @mr.send_pkt(MSG_SHORT_SEQ_STEP,sprintf("%f,%f,%f,%f,%f,%f,%f,S",*s[0],s[1]))
    end
    @mr.send_pkt(MSG_COMPLETE_SHORT_SEQ,nil)
  end
end

class Lseq
  attr_reader(:name,:a,:st)
  attr_accessor(:ready_flg,:done_flg)

  ST_IDLE=1
  ST_POSITION=2
  ST_PLAY=3
  
  def initialize(name,a)
    @name,@a=name,massage_a(a)
    @st=ST_IDLE
  end

  #
  # Must prepare Moog manual order:
  # roll,pitch,heave,surge,yaw,lateral
  # (lengths in meters)
  #
  
  def massage_a(aa)
    aa.map do |a|      
      aa=[a[0][0],a[0][1],a[0][2],a[0][3]*DEG2RAD,a[0][4]*DEG2RAD,a[0][5]*DEG2RAD]
      [Moogskt_ruby::server_idea_to_moog_idea(aa),a[1],'C']
    end
  end

  def prepare_for_play(main)
    unless(@st==ST_IDLE)
      lg("#{@name}: CANNOT PREPARE! (#{@st})")
      return
    end
    @st=ST_POSITION
    @ready_flg=false
    @done_flg=false

    main.mr.send_pkt(MSG_LONG_SEQ_AT_START,@name.to_s)
  end

  def loop(main)
    case(@st)
    when(ST_POSITION)      
      if(@ready_flg)
        main.mr.send_pkt(MSG_LONG_SEQ_EXECUTE,nil)
        @st=ST_PLAY
      end
    when(ST_PLAY)
      if(@done_flg)      
        @st=ST_IDLE
        return false 
      end
    end
    true
  end
end

class Demo_longseq < Single_demo
  STATE_SEQ_LOADING=1001
  LS_STATES={STATE_SEQ_LOADING=>'Sequence loading'}
  
  START_HEIGHT=-0.1
  
  STT=1.5
  ANGLE=6.0
  HEIGHT=100.0
  RUNTYPE='D'

  MAX_SPEED=0.14
  ROT_CENTER=[-1.3,0.3,0.0] # y,z,x in meters
  #  ROT_CENTER=[0.0,0.0,0.0] # y,z,x in meters
  
  H=Moogskt_ruby::MOOG_H_TO_SRV_H*1000+200
  STEP_ARR=[
    [:t_roll,[
       [[0.0,H,0.0,0.0,0.0,0.0],STT],
       [[0.0,H,0.0,0.0,0.0,-ANGLE],STT],
       [[0.0,H,0.0,0.0,0.0,ANGLE],STT*2],
       [[0.0,H,0.0,0.0,0.0,0.0],STT],
     ]],
    [:t_pitch,[
       [[0.0,H,0.0,0.0,0.0,0.0],STT],
       [[0.0,H,0.0,0.0,-ANGLE,0.0],STT],
       [[0.0,H,0.0,0.0,ANGLE,0.0],STT*2],
       [[0.0,H,0.0,0.0,0.0,0.0],STT],
     ]],
    [:t_heave,[
       [[0.0,H,0.0,0.0,0.0,0.0],STT],
       [[0.0,-HEIGHT+H,0.0,0.0,0.0,0.0],STT],
       [[0.0,HEIGHT+H,0.0,0.0,0.0,0.0],STT*2],
       [[0.0,H,0.0,0.0,0.0,0.0],STT],
     ]],
    [:t_surge,[
       [[0.0,H,0.0,0.0,0.0,0.0],STT],
       [[0.0,H,-HEIGHT,0.0,0.0,0.0],STT],
       [[0.0,H,HEIGHT,0.0,0.0,0.0],STT*2],
       [[0.0,H,0.0,0.0,0.0,0.0],STT],
     ]],
    [:t_yaw,[
       [[0.0,H,0.0,0.0,0.0,0.0],STT],
       [[0.0,H,0.0,-ANGLE,0.0,0.0],STT],
       [[0.0,H,0.0,ANGLE,0.0,0.0],STT*2],
       [[0.0,H,0.0,0.0,0.0,0.0],STT],
     ]],
    [:t_lateral,[
       [[0.0,H,0.0,0.0,0.0,0.0],STT],
       [[-HEIGHT,H,0.0,0.0,0.0,0.0],STT],
       [[HEIGHT,H,0.0,0.0,0.0,0.0],STT*2],
       [[0.0,H,0.0,0.0,0.0,0.0],STT],
     ]],    
  ].map do |n,a|
    [n,Lseq::new(n,a)]
  end.to_h
  
  SEQ2PLAY=[:t_roll,:t_pitch,:t_heave,:t_surge,:t_yaw,:t_lateral]
    
  def initialize
    super('Long sequence')
  end

  def exec(remote)
    @state=STATE_IDLE
    exec_lowlevel(remote,sprintf("%d,#{RECVPORT_TAG},%s,%f,,red,yellow",SESSION_LONG_SEQ,RUNTYPE,START_HEIGHT))
  end

  def after_login
    new_state(STATE_SEQ_LOADING)
  end
  
  def subclass_step(t)
    if(@state==STATE_SEQ_LOADING)
      if(@seqstoload && @ack)
        if(@seqstoload.length==0)
          lg("Sequence loading complete.")
          @seqstoload=nil
          @seqstoplay=SEQ2PLAY.clone
          @playedseq=nil
          new_state(STATE_ENGAGING)
        else
          lg("#{@seqstoload.length} sequences to load.")
          @ack=false
          feed_next_long_seq
        end
      end
      return true
    end
      
    return true unless(@state==STATE_ENGAGED)

    if(!@playedseq)
      if(@seqstoplay.length<=0)
        lg("Seqs completed")
        new_state(STATE_PARKING)
      else        
        sy=@seqstoplay.shift
        @playedseq=STEP_ARR[sy]
        @playedseq.prepare_for_play(self)
      end
    else
      @playedseq=nil unless(@playedseq.loop(self))
    end
    
    true
  end

  def packet_parse_local(time,msg_id,a)
    case msg_id
    when MSG_LONG_SEQ_AT_HEIGHT
      lg("At height.")
      @at_height=Time::now.to_f
      return true
    when MSG_LONG_SEQ_ACK
      lg("Long seq: Acked (#{a[0]})")
      @ack=true
    when MSG_LONG_SEQ_NAK
      lg("ERROR! long seq refused (#{a})")
      exit
    when MSG_LONG_SEQ_READY
      if(@playedseq)
        lg("Seq #{@playedseq.name} ready")
        @playedseq.ready_flg=true
      end
    when MSG_LONG_SEQ_DONE
      if(@playedseq)
        lg("Seq #{@playedseq.name} completed")
        @playedseq.done_flg=true
      end
    end
  end

  def feed_next_long_seq
    k=@seqstoload.keys[0]
    v=@seqstoload.delete(k)

    @mr.send_pkt(MSG_BEGIN_LONG_SEQ,sprintf("%s,%f,%f,%f,%f",k,MAX_SPEED,*ROT_CENTER))
    v.a.each do |s|
      @mr.send_pkt(MSG_LONG_SEQ_STEP,sprintf("%f,%f,%f,%f,%f,%f,%f,S",*s[0],s[1]))
    end
    @mr.send_pkt(MSG_COMPLETE_LONG_SEQ,nil)
  end

  def lower_new_state
    raise "Unknown long seq new state #{@state} (#{LS_STATES})" unless(LS_STATES[@state])        
    lg("Long seq: change state: #{LS_STATES[@state]}")

    case @state
    when STATE_SEQ_LOADING
      @seqstoload=STEP_ARR.clone
      @seqstoplay=SEQ2PLAY.clone
      @ack=true
    end
  end                                  
end

class Demo_joystick < Single_demo
  VELOCITY=0.15
  ROT_CENTER=[-0.5,0.0,0.0] # y,z,x in meters
  BTN_LBL='STOP'

  STATE_JS=2001
  JS_STATES={STATE_JS=>'Joystick mode'}
  
  def initialize
    super('Joystick')
    @sts=[nil,nil,nil]
    @eng=false
  end

  def exec(remote)
    @state=STATE_IDLE
    
    exec_lowlevel(remote,sprintf("%d,#{RECVPORT_TAG},%f,%f,%f,%f,%s",SESSION_JOYSTICK,VELOCITY,*ROT_CENTER,BTN_LBL))
  end

  def subclass_step(t)
    true
  end

  def lower_new_state
    raise "Unknown long seq new state #{@state} (#{JS_STATES})" unless(JS_STATES[@state])        
    lg("Long seq: change state: #{JS_STATES[@state]}")
  end

  def after_login
    new_state(STATE_ENGAGED)
  end
  def after_engage
    new_state(STATE_JS)
  end
  def after_park
    new_state(STATE_OFF)
  end
end

class Emu_demo
  def initialize
    @all_demos=Single_demo::all
    @remote=Moogskt_ruby::alive?
    raise "No platform commander server is alive!" unless(@remote)   
  end

  def runme
    demo=nil
    loop do
      print <<EOF

***********************************************************
*                  University of Bern                     *
*                  Technology Platform                    *
***********************************************************

Platform commander demo


EOF
      @all_demos.each_with_index  do |d,i|
        printf("%2d: %s\n",i+1,d.name)
      end
      printf("\n\nSelect (return=quit): ")
      resp=gets.chomp
      exit(0) if(resp.length==0)
      respn=resp.to_i-1
      if(respn>=0 &&  respn<@all_demos.length)
        demo=@all_demos[respn]
        break
      end     
    end

    demo.exec(@remote)
  end
end

Emu_demo::new.runme
