# Procedure to prepare a MicroSD card with a *Devuan* distribution to host a `PlatformCommander` demo installation

* Get a new MicroSD card - should better be at least 16 GB.

* Clone the official distribution of Raspberry PI firmware:
```
https://github.com/raspberrypi/firmware
```
  Warning: it is HUGE.


* Download the currently only available *Devuan* bootable image for
  Raspberry PI:  
```
http://files.devuan.org/devuan_ascii/embedded/devuan_ascii_2.0.0_armhf_raspi2.img.xz
```

* Uncompress it:
  
```sh
unxz devuan_ascii_2.0.0_armhf_raspi2.img.xz
```


* Insert the MicroSD card onto your Linux machine. Use `lsblk` to find
out which device it is assigned. Let'say it gets `/dev/sdz`. 

* From your linux machine, Use `dd` to copy the *Devuan* image to the
  card. For example:

```sh
dd if=devuan_ascii_2.0.0_armhf_raspi2.img of=/dev/sdz
```

* Now you will extend the copied partition so that it uses more space
  on the card. Use `fdisk.` At beginning the content will look similar
  to this:
  
```
Dispositivo Avvio  Start    Fine Settori  Size Id Tipo
/dev/sdz1           8192  270335  262144  128M  c W95 FAT32 (LBA)
/dev/sdz2         270336 3872767 3602432  1.7G 83 Linux
```

* Delete partition #2.
* Create a new partition #2, requesting the same start sector (in the
  example, 270336) and a larger size. I decided to use 12G, so as to
  leave some space for swap. At the end of the treatment I had:
  
```
Dispositivo Avvio  Start     Fine  Settori  Size Id Tipo
/dev/sdz1          8192   270335   262144  128M  c W95 FAT32 (LBA)
/dev/sdz2        270336 25436159 25165824   12G 83 Linux
/dev/sdz3      25436160 31116287  5680128  2.7G 82 Linux swap / Solaris
```

* At this point, execute this command:

```
resize2fs /dev/sdz2
```

  which extends the already existing ext4 partition so that it makes use
  of the available space.

* Mount the first parition. E.g.

   ```
   mount /dev/sdz1 /mnt
   ```
  and remove all that is there. Then copy inside there everything that
  is included under `/firmware/boot` of the official Raspberry PI
  firmware distribution you previously cloned.
  
* Inside that partition, create two files. one, called `cmdline.txt`
  should contain this **single** line:
  
  ```
  dwc_otg.lpm_enable=0 console=serial0,115200 console=tty1 root=/dev/mmcblk0p2 rootfstype=ext4 elevator=deadline  fsck.repair=yes rootwait mem=4G
  ```
  
* The second file must be called `config.txt`, and should contain this
  text block:
  
  ```
## always audio
dtparam=audio=on

## maximum amps on usb ports
max_usb_current=1

#
# Disable bluetooth & wifi
#

dtoverlay=disable-bt
dtoverlay=disable-wifi

[pi4]
## Enable DRM VC4 V3D driver on top of the dispmanx display stack
arm_64bit=1
total_mem=4096
enable_gic=1
# memory shared with the GPU
gpu_mem=128
```

* Unmount the first partition, and mount the second one

   ```
   umount /mnt
   mount /dev/sdz2 /mnt
   ```

* Copy the `v8` module directory you find under `firmware/modules` -
  in my case the directory is called `5.4.81-v8+` - to the appropriate
  place:
  
  ```
  cp -aRv firmware/modules/5.4.81-v8+ /mnt/lib/modules/
  ```
  
* if you have a cooler on your PI, add this line in `/mnt/etc/rc.local`:

```
echo performance >  /sys/devices/system/cpu/cpufreq/policy0/scaling_governor
```

which will permit your CPU to run as fast as possible. 
  
* Unmount the second partition:

   ```
   umount /mnt
   ```

## At this point, the SD card should be bootable. 

* Insert it into the Raspberry pi, make sure to connect to it a
  keyboard, a mouse, a monitor and an ethernet cable, and boot it.
  
  
### The PI must be able to obtain an address from a DHCP server!!

* I prefer to operate from my own computer, connecting to the PI via
  `ssh`. Thus, from the PI keyboard I only do two things:
  * Log in (as `root`, with password `toor`) and change root password.
  * Install package `openssh-server`:
  ```
  apt-get install openssh-server
  ```
  
* At this point you should be able to reach the machine via SSH. 

  You will want to update the installation from `ascii` to `beowulf`
  (the most recent *Devuan* version). To obtain this:
* you must substitute the contents of file `/etc/apt/sources.list` with these lines:
```
deb http://deb.devuan.org/merged beowulf main
deb http://deb.devuan.org/merged beowulf-updates main
deb http://deb.devuan.org/merged beowulf-security main
#deb http://deb.devuan.org/merged beowulf-backports main
```	

* You will then run these commands
```sh
apt-get update
apt-get dist-upgrade
```
  
* Reboot.

* At this point, install X11 on your PI. It isn sufficient to install
  two packages, that pull in all that's needed:
  
```sh
apt-get install xfce4 xfce4-goodies
```
  
* Install these packages:

```sh
apt-get install gcc make ruby ruby-dev git libgtk-3-dev libglib2.0-dev libglew-dev libxml2-dev libhidapi-dev libxcb-ewmh-dev mesa-utils libgl1-mesa-glx
```

* Install two *Ruby* `gems`:

```sh
gem install -V rake gtk3
```

* Create a normal user on the machine.

* Copy the `PlatformCommander_0.X` archive to the home directory of your new user.

* Extract it somewhere, and change directory to the root of the
  extracted material (`PlatformCommander_0.X`). 

* Copy *Ruby* script `cx.rb` to a place where it will be found by
  default:
  
  ```sh
  sudo cp scripts/cx.rb /usr/local/lib/site_ruby/
  ```

* Execute script `builder.rb`, which will compile the `PlatformCommander` material:

```sh
./builder.rb
```

**The script will once ask you for your password** in order to create
the default directories at the root of the disk space. 

If you receive no errors, the install procedure is completed. From
your login on the PI, first start X:

```sh
startx
```

then open two terminal windows. On both, change to where you have
extracted the `PlatformCommander` material (`PlatformCommander_0.X`).

* On the first one, execute

```sh
./emu_runner.rb
```

The master program and the emulator will show their screens.

* On the second one, execute

```sh
./emu_scripter.rb shortseq.rb
```

that will execute one of my test scripts. 

Of course, you will most probably be able to run your Julia scripts
too. You'll have to install julia and the scripts, and make sure that
you configure the address and port values appropriately.

